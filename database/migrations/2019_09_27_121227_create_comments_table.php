<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            
            $table->increments('id');
            $table->text('description');

            $table->integer('parent_id')->nullable();

            $table->unsignedInteger('bachelor_id')->nullable();
            $table->foreign('bachelor_id')->references('id')->on('bechelors')->onDelete('cascade');

            $table->unsignedInteger('sublet_id')->nullable();
            $table->foreign('sublet_id')->references('id')->on('sublets')->onDelete('cascade');

            $table->unsignedInteger('family_id')->nullable();
            $table->foreign('family_id')->references('id')->on('families')->onDelete('cascade');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('admin_id')->nullable();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
