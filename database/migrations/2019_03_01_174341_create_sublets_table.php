<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubLetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sublets', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sublet_religion');
            $table->string('sublet_gender');


            $table->string('month');
            $table->string('year');
            
            $table->String('room_status');
            $table->integer('total_room');
            $table->float('room_rent');

            $table->integer('bed_room');
            $table->integer('bath_room');
            $table->integer('phone');
            $table->text('short_description');
            $table->string('dinning_room');
            $table->string('drawing_room');
            $table->text('facilities');
            $table->text('conditions');

            
            $table->boolean('confirmed')->default(0)->comment('0=pending|1=confirm|2=Ban');
            $table->boolean('is_seen')->default(0)->comment('0=unseen|1=seen');
            $table->boolean('is_done')->default(0)->comment('0=not ok|1=ok');

            

            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->unsignedInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');

            $table->unsignedInteger('thana_id');
            $table->foreign('thana_id')->references('id')->on('thanas')->onDelete('cascade');

            $table->unsignedInteger('ward_id');
            $table->foreign('ward_id')->references('id')->on('wards')->onDelete('cascade');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->text('address');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub-lets');
    }
}
