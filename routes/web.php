<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






// use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
// use Illuminate\Support\Facades\Route;
// Route::get('/map',function(){
// 	Mapper::map(53.381128999999990000, -1.470085000000040000);	
// 	print '<div style="height:400px;width:400px;">';
// 	print Mapper::render();

// 	print '</div>';

// });







Auth::routes();

Route::get('/', [
	'uses' => 'Frontend\IndexPageController@index',
	'as' => '/'
]);

Route::get('/about-us', [
    'uses' => 'Frontend\IndexPageController@aboutUs',
    'as' => 'about-us'
]);

Route::get('/services', [
    'uses' => 'Frontend\IndexPageController@services',
    'as' => 'services'
]);

Route::get('/contact', [
    'uses' => 'Frontend\IndexPageController@contact',
    'as' => 'contact'
]);

Route::post('/contact/mail_sent', [
    'uses' => 'Frontend\IndexPageController@contactMailSent',
    'as' => 'contact-mail-sent'
]);



// for bachelor
Route::group(['prefix'=>'bachelors'],function(){

		Route::get('/', [
			'uses' => 'Frontend\BachelorController@index',
			'as' => 'bachelors'
		]);

		Route::get('/show/{id}',[
			'uses'=>'Frontend\BachelorController@show',
			'as'=>'bachelors-show'
		]);

});

//for sublet 

Route::group(['prefix'=>'sublets'],function(){

		Route::get('/', [
			'uses' => 'Frontend\SubletController@index',
			'as' => 'sublets'
		]);

		Route::get('/show/{id}', [
			'uses' => 'Frontend\SubletController@show',
			'as' => 'sublets-show'
		]);
});

// for family
Route::group(['prefix'=>'families'],function(){

		Route::get('/', [
			'uses' => 'Frontend\FamilyController@index',
			'as' => 'families'
		]);

		Route::get('/show/{id}', [
			'uses' => 'Frontend\FamilyController@show',
			'as' => 'families-show'
		]);

	});







Route::get('/dashboard', [
	'uses' => 'HomeController@index',
	'as' => '/dashboard'
]);

//user verification route
Route::get('/token/{token}',[
		'uses'=>'Backend\Frontend\UserVerificationController@verify',
		'as'=>'user.verification'
    ]);

//end user varifation route

//only user routes 
Route::group(['prefix'=>'user'],function()
{
	    //user dashboard route
		Route::get('/', [
			'uses' => 'Backend\User\UserController@dashboard',
			'as' => 'user-dashboard'
		]);


		Route::group(['prefix'=>'orders'],function(){

					Route::post('/store', [
						'uses' => 'Backend\User\OrderController@store',
						'as' => 'user-order'
					]);

					Route::get('/families', [
						'uses' => 'Backend\User\OrderController@family_index',
						'as' => 'families-user-order-index'
				    ]);

				    Route::get('/sublets', [
						'uses' => 'Backend\User\OrderController@sublet_index',
						'as' => 'sublets-user-order-index'
				    ]);

				    Route::get('/bachelors', [
						'uses' => 'Backend\User\OrderController@bachelor_index',
						'as' => 'bachelors-user-order-index'
				    ]);

			    });

		Route::group(['prefix'=>'comments'],function(){ 

				Route::post('/store', [
					'uses' => 'Backend\User\CommentController@store',
					'as' => 'user-comment-store'
				]);

				Route::post('/edit/{id}', [
					'uses' => 'Backend\User\CommentController@edit',
					'as' => 'user-comment-edit'
				]);

				Route::post('/update/{id}',[
					'uses'=>'Backend\User\CommentController@update',
					'as'=>'comment-user-update'
				]);


			 });






		Route::group(['prefix'=>'families'],function(){
				Route::get('/', [
					'uses' => 'Backend\User\FamilyController@index',
					'as' => 'families-user-index'
				]);

				Route::get('is-confirmed', [
					'uses' => 'Backend\User\FamilyController@confirmed',
					'as' => 'families-user-confirmed'
				]);

				Route::get('/create', [
					'uses' => 'Backend\User\FamilyController@create',
					'as' => 'families-user-create'
				]);

				Route::get('/booking/{id}',[
					'uses'=>'Backend\User\FamilyController@booking',
					'as'=>'user-families-booking'
				]);

				


				Route::post('/store', [
					'uses' => 'Backend\User\FamilyController@store',
					'as' => 'families-user-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Backend\User\FamilyController@edit',
					'as'=>'families-user-edit'
				]);

				Route::post('/update/{id}',[
					'uses'=>'Backend\User\FamilyController@update',
					'as'=>'families-user-update'
				]);

				Route::get('/show/{id}',[
					'uses'=>'Backend\User\FamilyController@show',
					'as'=>'families-user-show'
				]);


		    });
		Route::group(['prefix'=>'bechelors'],function(){
			
				Route::get('/', [
					'uses' => 'Backend\User\BechelorController@index',
					'as' => 'bechelors-user-index'
				]);

				Route::get('is-confirmed', [
					'uses' => 'Backend\User\BechelorController@confirmed',
					'as' => 'bechelors-user-confirmed'
				]);

				Route::get('/create', [
					'uses' => 'Backend\User\BechelorController@create',
					'as' => 'bechelors-user-create'
				]);

				Route::post('/store', [
					'uses' => 'Backend\User\BechelorController@store',
					'as' => 'bechelors-user-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Backend\User\BechelorController@edit',
					'as'=>'bechelors-user-edit'
				]);

				

				Route::post('/update/{id}',[
					'uses'=>'Backend\User\BechelorController@update',
					'as'=>'bechelors-user-update'
				]);

				Route::get('/show/{id}',[
					'uses'=>'Backend\User\BechelorController@show',
					'as'=>'bechelors-user-show'
				]);


	        });
		Route::group(['prefix'=>'cards'],function(){

		        Route::get('/',"Backend\User\BachelorCardController@index")->name('user-cards-index');
				Route::get('/create',"Backend\User\BachelorCardController@create")->name('user-cards-create');
				Route::post('/store',"Backend\User\BachelorCardController@store")->name('user-cards-store');

				Route::get('/booking/{id}',[
					'uses'=>'Backend\User\BachelorCardController@booking',
					'as'=>'card-user-booking'
				]);

				Route::get('/show',"Backend\User\BachelorCardController@show")->name('user-cards-show');
				Route::get('/edit/{id}',"Backend\User\BachelorCardController@edit")->name('user-cards-edit');
				Route::post('/update/{id}',"Backend\User\BachelorCardController@update")->name('user-cards-update');
				Route::post('/delete/{id}',"Backend\User\BachelorCardController@destroy")->name('user-cards-destroy'); 
			});




		Route::group(['prefix'=>'sub-lets'],function(){
				Route::get('/', [
					'uses' => 'Backend\User\SubletController@index',
					'as' => 'sub-lets-user-index'
				]);

				Route::get('is-confirmed', [
					'uses' => 'Backend\User\SubletController@confirmed',
					'as' => 'sub-lets-user-confirmed'
				]);

				Route::get('/create', [
					'uses' => 'Backend\User\SubletController@create',
					'as' => 'sub-lets-user-create'
				]);

				Route::post('/store', [
					'uses' => 'Backend\User\SubletController@store',
					'as' => 'sub-lets-user-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Backend\User\SubletController@edit',
					'as'=>'sub-lets-user-edit'
				]);

				Route::post('/update/{id}',[
					'uses'=>'Backend\User\SubletController@update',
					'as'=>'sub-lets-user-update'
				]);

				Route::get('/show/{id}',[
					'uses'=>'Backend\User\SubletController@show',
					'as'=>'sub-lets-user-show'
				]);

				Route::get('/booking/{id}',[
					'uses'=>'Backend\User\SubletController@booking',
					'as'=>'sublet-user-booking'
				]);

	        });
});

// user route end


//only admin route start
Route::group(['prefix'=>'admin'],function(){

		Route::get('/', [
			'uses' => 'Backend\Admin\AdminController@dashboard',
			'as' => 'admin.dashboard'
		]);
		//admin registration and verification
	    Route::get('/register/submit',[
	    	'uses'=>'Auth\Admin\RegisterController@showRegistrationForm',
	    	'as'=>'admin.register.submit'
	    ]);
		Route::post('/register',[
			'uses'=>'Auth\Admin\RegisterController@register',
			'as'=>'admin.register'
		]);
		Route::get('/token/{token}',[
			'uses'=>'Backend\AdminVerificationController@verify',
			'as'=>'admin.verification'
		]);
	    //admin login and logout
		Route::get('/login',[
			'uses'=>'Auth\Admin\LoginController@showLoginForm',
			'as'=>'admin.login'
		]);

		Route::post('/login/submit',[
			'uses'=>'Auth\Admin\LoginController@login',
			'as'=>'admin.login.submit'
		]);

		Route::post('/logout',[
			'uses'=>'Auth\Admin\LoginController@logout',
			'as'=>'admin.logout'
	    ]);

	    //admin Forgotpassword and reset password
		Route::post('/password/email',[
			'uses'=>'Auth\Admin\ForgotPasswordController@sendResetLinkEmail',
			'as'=>'admin.password.email'
		]);
		Route::get('/password/reset',[
			'uses'=>'Auth\Admin\ForgotPasswordController@showLinkRequestForm',
			'as'=>'admin.password.request'
		]);
		Route::get('/password/reset/{token}',[
			'uses'=>'Auth\Admin\ResetPasswordController@showResetForm',
			'as'=>'admin.password.reset'
		]);
		Route::post('/password/reset',[
			'uses'=>'Auth\Admin\ResetPasswordController@reset',
			'as'=>'admin.password.reset.post'
		]);

		Route::resource('cities','Backend\Admin\CityController');
		Route::resource('thanas','Backend\Admin\ThanaController');
		Route::resource('wards','Backend\Admin\WardController');

		//for categories
	    Route::group(['prefix'=>'categories'],function(){

		    Route::get('/',"Backend\Admin\CategoryController@index")->name('categories.index');
		    Route::get('/create',"Backend\Admin\CategoryController@create")->name('categories.create');
		    Route::post('/store',"Backend\Admin\CategoryController@store")->name('categories.store');
		    Route::get('/show/{id}',"Backend\Admin\CategoryController@show")->name('categories.show');
		    Route::get('/edit/{id}',"Backend\Admin\CategoryController@edit")->name('categories.edit');
		    Route::post('/update/{id}',"Backend\Admin\CategoryController@update")->name('categories.update');
		    Route::post('/delete/{id}',"Backend\Admin\CategoryController@delete")->name('categories.delete');
		    Route::get('/categories-show/{id}',"Backend\Admin\FamilyController@categories_show")->name('categories-show');
		});




	    Route::group(['prefix'=>'cards'],function(){
	            Route::get('/',"Backend\Admin\BachelorCardController@index")->name('admin-cards-index');
				Route::get('/create',"Backend\Admin\BachelorCardController@create")->name('admin-cards-create');
				Route::post('/store',"Backend\Admin\BachelorCardController@store")->name('admin-cards-store');

				Route::get('/show',"Backend\Admin\BachelorCardController@show")->name('admin-cards-show');
				Route::get('/edit/{id}',"Backend\Admin\BachelorCardController@edit")->name('admin-cards-edit');
				Route::post('/update/{id}',"Backend\Admin\BachelorCardController@update")->name('admin-cards-update');
				Route::post('/delete/{id}',"Backend\Admin\BachelorCardController@destroy")->name('admin-cards-destroy');
		});

		Route::group(['prefix'=>'comments'],function(){ 

				Route::post('/comment/store', [
					'uses' => 'Backend\Admin\CommentController@store',
					'as' => 'admin-comment-store'
				]);


	    });




	    Route::group(['prefix'=>'families'],function(){
				Route::get('/pending', [
					'uses' => 'Backend\Admin\FamilyController@pending',
					'as' => 'families-admin-pending'
				]);

				Route::get('confirm', [
					'uses' => 'Backend\Admin\FamilyController@confirm',
					'as' => 'families-admin-confirm'
				]);

				Route::get('/complete', [
					'uses' => 'Backend\Admin\FamilyController@complete',
					'as' => 'families-admin-complete'
				]);

				Route::get('/create', [
					'uses' => 'Backend\Admin\FamilyController@create',
					'as' => 'families-admin-create'
				]);

				Route::post('/store', [
					'uses' => 'Backend\Admin\FamilyController@store',
					'as' => 'families-admin-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Backend\Admin\FamilyController@edit',
					'as'=>'families-admin-edit'
				]);

				Route::post('/update/{id}',[
					'uses'=>'Backend\Admin\FamilyController@update',
					'as'=>'families-admin-update'
				]);

				Route::get('/show/{id}',[
					'uses'=>'Backend\Admin\FamilyController@show',
					'as'=>'families-admin-show'
				]);

				Route::post('/comment/store', [
					'uses' => 'Backend\Admin\FamilyCommentController@comment_store',
					'as' => 'families-admin-comment-store'
				]);

				Route::post('/families/confirmed/{id}', [
					'uses' => 'Backend\Admin\FamilyController@is_Confirmed',
					'as' => 'admin-family-confirmed'
				]);



	});
	    Route::group(['prefix'=>'bechelors'],function(){
				Route::get('/pending', [
					'uses' => 'Backend\Admin\BechelorController@pending',
					'as' => 'bechelors-admin-pending'
				]);

				Route::get('confirm', [
					'uses' => 'Backend\Admin\BechelorController@confirm',
					'as' => 'bechelors-admin-confirm'
				]);

				Route::get('complete', [
					'uses' => 'Backend\Admin\BechelorController@complete',
					'as' => 'bechelors-admin-complete'
				]);

				Route::get('/create', [
					'uses' => 'Backend\Admin\BechelorController@create',
					'as' => 'bechelors-admin-create'
				]);

				Route::post('/store', [
					'uses' => 'Backend\Admin\BechelorController@store',
					'as' => 'bechelors-admin-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Backend\Admin\BechelorController@edit',
					'as'=>'bechelors-admin-edit'
				]);

				Route::post('/update/{id}',[
					'uses'=>'Backend\Admin\BechelorController@update',
					'as'=>'bechelors-admin-update'
				]);

				Route::get('/show/{id}',[
					'uses'=>'Backend\Admin\BechelorController@show',
					'as'=>'bechelors-admin-show'
				]);

				Route::post('/comment/store', [
					'uses' => 'Backend\Admin\BechelorCommentController@comment_store',
					'as' => 'bechelors-admin-comment-store'
				]);

				Route::post('/bachelors/confirmed/{id}', [
					'uses' => 'Backend\Admin\BechelorController@is_Confirmed',
					'as' => 'admin-bachelor-confirmed'
				]);

	        });


	    Route::group(['prefix'=>'sublets'],function(){
				Route::get('/pending', [
					'uses'=>'Backend\Admin\SubletController@pending',
					'as' => 'sublets-admin-pending'
				]);

				Route::get('confirm', [
					'uses'=>'Backend\Admin\SubletController@confirm',
					'as' => 'sublets-admin-confirm'
				]);

				Route::get('/complete', [
					'uses'=>'Backend\Admin\SubletController@complete',
					'as' => 'sublets-admin-complete'
				]);


				Route::get('/create', [
					'uses' => 'Backend\Admin\SubletController@create',
					'as' => 'sublets-admin-create'
				]);

				Route::post('/store', [
					'uses' => 'Backend\Admin\SubletController@store',
					'as' => 'sublets-admin-store'
				]);
				Route::get('/edit/{id}',[
					'uses'=>'Backend\Admin\SubletController@edit',
					'as'=>'sublets-admin-edit'
				]);

				Route::post('/update/{id}',[
					'uses'=>'Backend\Admin\SubletController@update',
					'as'=>'sublets-admin-update'
				]);

				Route::get('/show/{id}',[
					'uses'=>'Backend\Admin\SubletController@show',
					'as'=>'sublets-admin-show'
				]);

				Route::post('/comment/store', [
					'uses' => 'Backend\Admin\SubletCommentController@comment_store',
					'as' => 'sublets-admin-comment-store'
				]);

				Route::post('/sublets/confirmed/{id}', [
					'uses' => 'Backend\Admin\SubletController@is_Confirmed',
					'as' => 'admin-sublets-confirmed'
				]);
	        });

	        Route::group(['prefix'=>'comments'],function(){

				Route::post('/comment/store', [
					'uses' => 'Backend\Admin\CommentController@store',
					'as' => 'admin-comment-store'
				]);


			});


	    Route::group(['prefix'=>'orders'],function(){


					Route::get('/families', [
						'uses' => 'Backend\Admin\OrderController@family_index',
						'as' => 'families-order-index'
				    ]);

				    Route::get('/sublets', [
						'uses' => 'Backend\Admin\OrderController@sublet_index',
						'as' => 'sublets-order-index'
				    ]);

				    Route::get('/bachelors', [
						'uses' => 'Backend\Admin\OrderController@bachelor_index',
						'as' => 'bachelors-order-index'
				    ]);

				    //pending post
				    Route::get('/families/pending', [
						'uses' => 'Backend\Admin\OrderController@family_pending',
						'as' => 'families-order-pending'
				    ]);

				    Route::get('/sublets/pending', [
						'uses' => 'Backend\Admin\OrderController@sublet_pending',
						'as' => 'sublets-order-pending'
				    ]);

				    Route::get('/bachelors/pending', [
						'uses' => 'Backend\Admin\OrderController@bachelor_pending',
						'as' => 'bachelors-order-pending'
				    ]);
				    //complete
				    Route::get('/families/complete', [
						'uses' => 'Backend\Admin\OrderController@family_complete',
						'as' => 'families-order-complete'
				    ]);

				    Route::get('/sublets/complete', [
						'uses' => 'Backend\Admin\OrderController@sublet_complete',
						'as' => 'sublets-order-complete'
				    ]);

				    Route::get('/bachelors/complete', [
						'uses' => 'Backend\Admin\OrderController@bachelor_complete',
						'as' => 'bachelors-order-complete'
				    ]);


				    Route::get('/bachelors/show/{id}', [
						'uses' => 'Backend\Admin\OrderController@bachelor_show',
						'as' => 'bachelors-order-show'
				    ]);

				    Route::get('/families/show/{id}', [
						'uses' => 'Backend\Admin\OrderController@family_show',
						'as' => 'families-order-show'
				    ]);

				    Route::get('/sublets/show/{id}', [
						'uses' => 'Backend\Admin\OrderController@sublet_show',
						'as' => 'sublets-order-show'
				    ]);


				    Route::get('/sublets/edit/{id}', [
						'uses' => 'Backend\Admin\OrderController@sublet_edit',
						'as' => 'sublets-order-edit'
				    ]);

				    Route::get('/bachelors/edit/{id}', [
						'uses' => 'Backend\Admin\OrderController@bachelor_edit',
						'as' => 'bachelors-order-edit'
				    ]);

				    Route::get('/families/edit/{id}', [
						'uses' => 'Backend\Admin\OrderController@family_edit',
						'as' => 'families-order-edit'
				    ]);

				    Route::get('/sublets/delete/{id}', [
						'uses' => 'Backend\Admin\OrderController@sublet_delete',
						'as' => 'sublets-order-delete'
				    ]);


                    Route::get('/bachelors/delete/{id}', [
						'uses' => 'Backend\Admin\OrderController@bachelor_delete',
						'as' => 'bachelors-order-delete'
				    ]);

				    Route::get('/families/delete/{id}', [
						'uses' => 'Backend\Admin\OrderController@family_delete',
						'as' => 'families-order-delete'
				    ]);

				    Route::get('/sublets/show/{id}', [
						'uses' => 'Backend\Admin\OrderController@sublet_show',
						'as' => 'sublets-order-show'
				    ]);


				    Route::post('/complete/{id}', [
						'uses' => 'Backend\Admin\OrderController@order_complete',
						'as' => 'order-complete'
				    ]);

				    Route::post('/paid/{id}', [
						'uses' => 'Backend\Admin\OrderController@order_paid',
						'as' => 'order-paid'
				    ]);

				   


			    });

});

    Route::get('categories-show/{id}',[
    	'uses'=>'Frontend\CategoryController@categories_show',
    	'as'=>'categories-show'
    ]);


	//for cities thanas and wards
	Route::get('/cities/ajax/{id}', [
		'uses' => 'HomeController@cities_ajax',
		'as' => 'cities'
	]);

	Route::get('/thanas/ajax/{id}', [
		'uses' => 'HomeController@thanas_ajax',
		'as' => 'thanas'
	]);


//both user and admin route end 


