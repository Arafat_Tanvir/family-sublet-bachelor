@extends('frontend.layouts.master')

@section('title')
    About-Us
@endsection

@section('content')
<section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container" >
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-6 ftco-animate pb-5 text-center"> 
            <div class="box p-5" style="background-color: #172E7E">

        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
          @csrf    

            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="text-right">
                       
                            <input class="form-check-input"  style="color: white" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" style="color: white" for="remember">
                                {{ __('Remember Me') }}
                            </label>

                    <button type="submit" class="btn btn-info btn-block btn-flat">
                        {{ __('Login') }}
                    </button>
            </div>
        </form>
        <!-- /.social-auth-links -->

        <a class="btn float-right" style="color: white" href="{{ route('password.request') }}">
            {{ __('Forgot Your Password?') }}
        </a>

        <a href="{{ route('register') }}" style="color: white" class="float-left">Register</a>
          </div>
          </div>
        </div>
      </div>
    </section>
@endsection