@extends('frontend.layouts.master')

@section('title')
    User-Registration
@endsection

@section('content')


<section class="hero-wrap hero-wrap-2  js-fullheight" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');margin-top: 80px;" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container" >
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-8 ftco-animate"> 
            <div class="p-5" style="background-color: #172E7E;color: white">
           <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}"  enctype="multipart/form-data">
              @csrf
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-4">
                          <label for="name">{{ __('Name') }}</label>
                        </div>
                        <div class="col-sm-8">
                          
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                       
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-4">
                          <label for="gender">{{ __('Gender') }}</label>
                        </div>
                        <div class="col-sm-8">
                          <div class="row text-center"> 
                            <label class="col-sm-6">
                                    <input type="radio" class="flat-red" name="gender" value="Male">Male
                            </label>
                            <label class="col-sm-6">
                                  <input type="radio" class="flat-red" name="gender" value="Female">Female
                            </label>
                          </div>
                            

                            @if ($errors->has('gender'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('gender') }}</strong>
                                </span>
                            @endif
                        </div>
                       
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-4">
                          <label for="name">{{ __('Contact Number') }}</label>
                        </div>
                        <div class="col-sm-8">
                          
                            <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>

                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                       
                      </div>
                    </div>
                         
                            <div class="form-group ">
                              <div class="row">
                                <div class="col-sm-4">
                                     <label for="email">{{ __('E-Mail Address') }}</label>
                                </div>
                                <div class="col-sm-8">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                          </div>
                          

                        <div class="form-group">
                          <div class="row">
                        <div class="col-sm-4">
                            <label for="password">{{ __('Password') }}</label>
                          </div>

                            <div class="col-sm-8">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div>

                        <div class="form-group">
                          <div class="row">
                        <div class="col-sm-4">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                          </div>

                            <div class="col-sm-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                      </div>

                        <div class="form-group">
                          <div class="row">
                            <div class="col-sm-4">
                                <label for="images">{{ __('Picture') }}</label>
                          </div>

                            <div class="col-sm-8">
                                <input id="images" type="file" class="form-control{{ $errors->has('images') ? ' is-invalid' : '' }}" name="images" value="{{ old('images') }}" required autofocus>

                                @if ($errors->has('images'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-sm-8 offset-sm-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
              </div>
          </div>
        </div>
      </div>
    </section>
  @endsection

@section('scripts')
<script>
  $(function () {
    

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
  })
</script>

@endsection



