      <div class="container">
        <a class="navbar-brand" href="{{ route('/')}}">Easy Finder</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active"><a href="{{ route('/')}}" class="nav-link">Home</a></li>
                    <li class="nav-item"><a href="{{ route('about-us')}}" class="nav-link">About</a></li>
                    <li class="nav-item"><a href="{{ route('families')}}" class="nav-link">Family</a></li>
                    <li class="nav-item"><a href="{{ route('sublets')}}" class="nav-link">Sublet</a></li>
                    <li class="nav-item"><a href="{{ route('bachelors')}}" class="nav-link">Bachelor</a></li>
                    <li class="nav-item"><a href="{{ route('services')}}" class="nav-link">Services</a></li>
                    <li class="nav-item"><a href="{{ route('contact')}}" class="nav-link">Contact</a></li>
                   @guest

                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                          {{ __('Login') }}
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">
                          <i class="fa fa-registered"></i>
                          {{ __('Register') }}
                        </a>
                      </li>
                    @else
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('user-dashboard') }}">
                          {{ __('User DashBoard') }}
                        </a>

                      </li>
                    @endguest
                </ul>
            </div>
      </div>