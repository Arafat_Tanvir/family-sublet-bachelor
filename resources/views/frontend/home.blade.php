@extends('frontend.layouts.master')

@section('title')
    Home
@endsection

@section('content')

<div class="hero-wrap ftco-degree-bg" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text justify-content-center align-items-center">
          <div class="col-lg-8 col-md-6 ftco-animate d-flex align-items-end">
            <div class="text text-center">
                <h1 class="mb-4"> Easiest Way to <br> Find Your Room </h1>
                <p style="font-size: 18px;">You can easily find your room as a rent, sublet or just only a seat. You can list your property for rent.People can easily find your post and booked. </p>
                <form action="#" class="search-location mt-md-5">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 align-items-end">
                                <div class="form-group">
                                <div class="form-field">
                                <input type="text" class="form-control" placeholder="Search location">
                                <button><span class="ion-ios-search"></span></button>
                              </div>
                          </div>
                            </div>
                        </div>
                    </form>
                  </div>
                </div>
               
            </div>
        </div>
        <div class="mouse">
                <a href="#" class="mouse-icon">
                    <div class="mouse-wheel"><span class="ion-ios-arrow-round-down"></span></div>
                </a>
        </div>
    </div>

<section class="ftco-section goto-here">
        <div class="container">
            <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <span class="subheading">What You Looking For</span>
            <h2 class="mb-2">Explore Easy Finder</h2>
          </div>
        </div>
        <div class="row">
            <div class="col-md-4" onclick="location.href='{{route('families')}}'">
                <div class="property-wrap ftco-animate">
                    <a  class="img" style="background-image: url({{ asset('/') }}assets/frontend/images/work-1.jpg);"></a>
                    <div class="text shadow">
                        <h3> <b>Family</b></h3>
                         
                        <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; {{ $family_count->count()}}  Rent Room</span></p>
                        <a class="d-flex align-items-center justify-content-center btn-custom">
                        <span class="ion-ios-link"></span>
                      </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4" onclick="location.href='{{route('sublets')}}'">
                <div class="property-wrap ftco-animate">
                    <a href="#" class="img" style="background-image: url({{ asset('/') }}assets/frontend/images/work-2.jpg);"></a>
                    <div class="text shadow">
                        <h3><b>Sublet</b></h3>
                        
                        <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; {{ $sublet_count->count()}}  Sublet</span></p>
                        <a class="d-flex align-items-center justify-content-center btn-custom">
                        <span class="ion-ios-link"></span>
                      </a>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-4" onclick="location.href='{{route('bachelors')}}'">
                <div class="property-wrap ftco-animate">
                    <a href="#" class="img" style="background-image: url({{ asset('/') }}assets/frontend/images/work-3.jpg);"></a>
                    <div class="text shadow">
                        <h3><b>Bachelor Seat</b></h3>
                        <p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; {{ $bachelor_count->count()}}
                         Bachelor Seat</span></p>
                         
                        <a class="d-flex align-items-center justify-content-center btn-custom">
                        <span class="ion-ios-link"></span>
                      </a>
                        
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    @if($families->count()>0)
    <section class="ftco-section ftco-no-pt">
          <div class="container">
              <div class="row justify-content-center mb-5">
                  <div class="col-md-7 heading-section text-center ftco-animate">
                      <span class="subheading">Looking for Family</span>
                      <h2>Explore Family Room</h2>
                  </div>
              </div>
              
              <div class="row">
                @foreach($families as $family)
                <div class="col-md-4">
                  <div class="property-wrap ftco-animate">
                    <div id="carouselExample{{$family->id}}Indicators" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                          @foreach($family->images as $image)
                          <li data-target="#carouselExample{{$family->id}}Indicators" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                          @endforeach

                      </ol>
                      <div class="carousel-inner">
                        @php
                        $i=1
                        @endphp

                        @foreach($family->images as $image)
                        <div class="carousel-item {{$i==1 ? 'active' : ''}}">
                          <p class="img" style="background-image: url({{asset('images/family_room/'.$image->images)}});"></p>
                        </div>
                        @php

                        $i++
                        @endphp
                        @endforeach
                        
                      </div>
                      <a class="carousel-control-prev" href="#carouselExample{{$family->id}}Indicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExample{{$family->id}}Indicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>


                    <div class="text shadow-lg" onclick="location.href='{{route('families-show',$family->id)}}'">

                      <div class="text-center">
                          <img src="{{asset('images/categories/'.$family->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">
                      </div>
                      <hr>

                      <h4 class="badge badge-pill badge-light"> {!! $family->room_status!!}</h4> <br> 
                      <h4 class="badge badge-warning text-center"> BDT : {{ $family->room_rent}}/Tk </h4> <br>  
                      <h4 class="fas fa-clock badge badge badge-pill badge-light"> 1<sup>st</sup> {{ $family->month }},{{ $family->year}}</h4>
                      <h4 class="fas fa-map-marker-alt badge badge-pill badge-light"> {!! $family->ward->name!!},{!! $family->thana->name!!},{!! $family->city->name!!}</h4>
                      <span class="far fa-address-card"> {!! $family->address !!}</span>
                      <a class="d-flex align-items-center justify-content-center btn-custom">
                        <span class="ion-ios-link"></span>
                      </a>
                    </div>
                  </div>
                </div>
                @endforeach
             </div>
            
          </div>
      </section>

       @else

      @endif

      @if($sublets->count()>0)

    <section class="ftco-section ftco-no-pt">
          <div class="container">
              <div class="row justify-content-center mb-5">
                  <div class="col-md-7 heading-section text-center ftco-animate">
                      <span class="subheading">Looking for Sublet</span>
                      <h2>Explore Sublet Room</h2>
                  </div>
              </div>
              <div class="row">
          @foreach($sublets as $sublet)
          <div class="col-md-4">
            <div class="property-wrap ftco-animate">
              <div id="carousel{{$sublet->id}}ExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($sublet->images as $image)
                    <li data-target="#carousel{{$sublet->id}}ExampleIndicators" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                    @endforeach

                </ol>
                <div class="carousel-inner">
                  @php
                  $i=1
                  @endphp

                  @foreach($sublet->images as $image)
                  <div class="carousel-item {{$i==1 ? 'active' : ''}}">
                    <p class="img" style="background-image: url({{asset('images/sublet_room/'.$image->images)}});"></p>
                  </div>
                  @php

                  $i++
                  @endphp
                  @endforeach
                  
                </div>
                <a class="carousel-control-prev" href="#carousel{{$sublet->id}}ExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel{{$sublet->id}}ExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>


              <div class="text shadow-lg" onclick="location.href='{{route('sublets-show',$sublet->id)}}'">

                <div class="text-center">
                    <img src="{{asset('images/categories/'.$sublet->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">
                </div>
                <hr>
                
                <h4 class="badge badge-pill badge-light"> {!! $sublet->room_status!!}</h4> <br> 
                <h4 class="badge badge-warning text-center"> BDT : {{ $sublet->room_rent}}/Tk </h4> <br>  
                <h4 class="fas fa-clock badge badge badge-pill badge-light"> 1<sup>st</sup> {{ $sublet->month }},{{ $sublet->year}}</h4>
                <h4 class="fas fa-map-marker-alt badge badge-pill badge-light"> {!! $sublet->ward->name!!},{!! $sublet->thana->name!!},{!! $sublet->city->name!!}</h4>
                <span class="far fa-address-card"> {!! $sublet->address !!}</span>
                <a class="d-flex align-items-center justify-content-center btn-custom">
                  <span class="ion-ios-link"></span>
                </a>
              </div>
            </div>
          </div>
          @endforeach
    </div>
          </div>
      </section>
      @else

      @endif

      @if($bachelors->count()>0)

    <section class="ftco-section ftco-no-pt">
          <div class="container">
              <div class="row justify-content-center mb-5">
                  <div class="col-md-7 heading-section text-center ftco-animate">
                      <span class="subheading">Looking for Bachelor Seat</span>
                      <h2>Explore Bachelor Seat</h2>
                  </div>
              </div>
            <div class="row">
              @foreach($bachelors as $bechelor)
              <div class="col-md-4">
                <div class="property-wrap ftco-animate">
                  <div id="carousel{{$bechelor->id}}ExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($bechelor->images as $image)
                        <li data-target="#carousel{{$bechelor->id}}ExampleIndicators" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                        @endforeach

                    </ol>
                    <div class="carousel-inner">
                      @php
                      $i=1
                      @endphp

                      @foreach($bechelor->images as $image)
                      <div class="carousel-item {{$i==1 ? 'active' : ''}}">
                        <p class="img" style="background-image: url({{asset('images/bechelor_room/'.$image->images)}});"></p>
                      </div>
                      @php

                      $i++
                      @endphp
                      @endforeach
                      
                    </div>
                    <a class="carousel-control-prev" href="#carousel{{$bechelor->id}}ExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel{{$bechelor->id}}ExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>


                   <div class="text shadow-lg" onclick="location.href='{{route('bachelors-show',$bechelor->id)}}'">
                      <div class="text-center">
                          <img src="{{asset('images/categories/'.$bechelor->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">
                      </div>
                      <hr>
                      <ul class="property_list">
                        <li><span class="flaticon-bed"></span>{{ $bechelor->seat}}</li>
                        <li><span class="flaticon-bathtub"></span>{{ $bechelor->room  }}</li>
                      </ul>
                      <h4 class="badge badge-pill badge-light">{!! $bechelor->room_status!!}</h4> <br>  
                      <h4 class="badge badge-warning text-center">BDT : {{ $bechelor->room_rent}}/Tk </h4> <br> 
                      <h4 class="fas fa-clock badge badge badge-pill badge-light">1<sup>st</sup> {{ $bechelor->month }},{{ $bechelor->year}}</h4>
                      <h4 class="fas fa-map-marker-alt badge badge-pill badge-light"> {!! $bechelor->ward->name!!},{!! $bechelor->thana->name!!},{!! $bechelor->city->name!!}</h4>
                      <span class="far fa-address-card"> {!! $bechelor->address !!}</span>
                      <a class="d-flex align-items-center justify-content-center btn-custom">
                        <span class="ion-ios-link"></span>
                      </a>
                    </div>
                </div>
              </div>
              @endforeach
        </div>
          </div>
      </section>

      @else

      @endif

      @if($users->count()>0)

    <section class="ftco-section testimony-section">
      <div class="container">
       <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <span class="subheading">Testimonial</span>
            <h2 class="mb-3">Happy Clients</h2>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel ftco-owl">
              @foreach($users as $user)
              <div class="item">
                <div class="testimony-wrap py-4">
                  <div class="text">
                    <div class="d-flex align-items-center">
                        <div class="user-img" style="background-image: url({{asset('images/users/'.$user->images)}})"></div>
                        <div class="pl-3">
                            <p class="name">{{ $user->name}}</p>
                            <span class="position">{{ $user->email}}</span>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>

    @else

    @endif

    @endsection