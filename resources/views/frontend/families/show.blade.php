@extends('frontend.layouts.master')

@section('title')
    Bachelors seat Details
@endsection

@section('content')

    <section class="ftco-section ftco-property-details">
      <div class="container">
      	<div class="row justify-content-center">
      		<div class="col-md-12">
      			<div class="property-details">
      				<div class="text-center">
					       <div class="card-header">
					            <img src="{{asset('images/categories/'.$families->category->images)}}" class="img-circle" height="80px" width="80px" alt="">
					             <br>
					           
					            <h3> <b>{!! $families->category->name !!} </b></h4>
					               From : 1st {{ $families->month}},{{$families->year}} <br>
					               <h6>{{ $families->room_status}}, Seat : {{$families->seat}}({!! $families->category->name!!}), in {{ $families->room}} Room</h6>
					            <h6 class="badge badge-warning" style="font-size: 20px">BDT : {{ $families->room_rent}}</h6><br>
					       </div>
					    </div>
      				<div id="carousel{{$families->id}}ExampleIndicators" class="carousel slide" data-ride="carousel">
		                <ol class="carousel-indicators">
		                 
		                  @foreach($families->images as $image)
		                  <li data-target="#carousel{{$families->id}}ExampleIndicators" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
		                  @endforeach
		                </ol>
		                <div class="carousel-inner">
		                  @php
		                  $i=1
		                  @endphp

		                  @foreach($families->images as $image)
		                  <div class="carousel-item {{$i==1 ? 'active' : ''}}">
		                    <img  src="{{asset('images/family_room/'.$image->images)}}" class="img">
		                    <div class="carousel-caption">
		                        <h4 style="color: white">{{$i}}</h4>
		                        <p>Family Sublet Bachelor Rental System</p>
		                        
		                    </div>
		                  </div>
		                  @php

		                  $i++
		                  @endphp
		                  @endforeach
		                  
		                </div>
		                <a class="carousel-control-prev" href="#carousel{{$families->id}}ExampleIndicators" role="button" data-slide="prev">
		                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		                  <span class="sr-only">Previous</span>
		                </a>
		                <a class="carousel-control-next" href="#carousel{{$families->id}}ExampleIndicators" role="button" data-slide="next">
		                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
		                  <span class="sr-only">Next</span>
		                </a>
		              </div>
      				<div class="text text-center">
      					<span class="subheading">Chattogram</span>
      					<h2>The Blue Sky Home</h2>
      					<a href="{{route('user-families-booking', $families->id)}}" ><i class="label label-warning pull-left">Booking</i></a>
      				</div>
      			</div>
      			<div class="col-md-12 pills">
						<div class="bd-example bd-example-tabs">
							<div class="d-flex justify-content-center">
							  <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">

							    <li class="nav-item">
							      <a class="nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="pills-description" aria-expanded="true">Features</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" id="pills-manufacturer-tab" data-toggle="pill" href="#pills-manufacturer" role="tab" aria-controls="pills-manufacturer" aria-expanded="true">Description</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" id="pills-review-tab" data-toggle="pill" href="#pills-review" role="tab" aria-controls="pills-review" aria-expanded="true">Review</a>
							    </li>
							  </ul>
							</div>

						  <div class="tab-content" id="pills-tabContent">
						    <div class="tab-pane fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">
						    	<div class="row">
						    		<div class="col-md-4">
						    			<ul class="features">
						    				<li class="check"><span class="ion-ios-checkmark"></span>Lot Area: 1,250 SQ FT</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Bed Rooms: 4</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Bath Rooms: 4</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Luggage</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Garage: 2</li>
						    			</ul>
						    		</div>
						    		<div class="col-md-4">
						    			<ul class="features">
						    				<li class="check"><span class="ion-ios-checkmark"></span>Floor Area: 1,300 SQ FT</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Year Build:: 2019</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Water</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Stories: 2</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Roofing: New</li>
						    			</ul>
						    		</div>
						    		<div class="col-md-4">
						    			<ul class="features">
						    				<li class="check"><span class="ion-ios-checkmark"></span>Floor Area: 1,300 SQ FT</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Year Build:: 2019</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Water</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Stories: 2</li>
						    				<li class="check"><span class="ion-ios-checkmark"></span>Roofing: New</li>
						    			</ul>
						    		</div>
						    	</div>
						    </div>

						    <div class="tab-pane fade" id="pills-manufacturer" role="tabpanel" aria-labelledby="pills-manufacturer-tab">
						      <div class="row">
						      	<div class="col-sm-6">
			                    	<div class="table-responsive p-3">
					                    <table class="table table-bordered">
					                     	<tr>
					                     	 	<th>City</th>
					                     	 	<td>{{ $families->city->name}}</td>
					                     	</tr>
					                     	<tr>
					                     	 	<th>Thana</th>
					                     	 	<td>{{ $families->thana->name}}</td>
					                     	</tr>
					                     	<tr>
					                     	 	<th>Ward</th>
					                     	 	<td>{{ $families->Ward->name}}</td>
					                     	</tr>

					                     	<tr>
					                     	 	<th>Address </th>
					                     	 	<td>{!! $families->address !!}</td>
					                     	</tr>
					                    </table>
					                </div>
					            </div>
					               
			                       
									<div class="col-sm-6">
										 <div style="height: 500px;">
									         {!! Mapper::render() !!}
									     </div>
									</div>
						      	</div>
						      </div>

						    <div class="tab-pane fade" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">
						      <div class="row">
							   		<div class="col-md-7">
							   			<h3 class="head">23 Reviews</h3>
							   			@foreach(App\Models\Admin\Comment::orderBy('id','asc')->where('family_id',$families->id)->where('parent_id',NULL)->get(); as $parent)

							   			<div class="review d-flex" >
							   				<img src="{{asset('images/users/'.$parent->user->images)}}" class="user-img" alt="">
									   		<div class="desc">
									   			<h4>
									   				<span class="text-left">{{$parent->user->name}}</span>
									   				<span class="text-right">14 March 2018</span>
									   			</h4>

									   			<p class="star text-right">
								   					<span class="text-right">
								   						<a href="#comment-{{$parent->id}}" data-toggle="collapse" class="reply"><i class="icon-reply"></i></a></span>
									   			</p>
									   			<span>
									   			<p class="text-left">{{ $parent->description}}</p>
									   			
									   			@if(Auth::user()->email==$parent->user->email)
									   			<p class="star text-right">
								   					<span class="text-right">

								   						<a data-toggle="modal" data-target="#exampleModalCenter{{ $parent->id}}parent"  class="reply">Edit</a>
								   					</span>

													<!-- Modal -->
													<div class="modal fade" id="exampleModalCenter{{ $parent->id}}parent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
													  <div class="modal-dialog modal-dialog-centered" role="document">
													    <div class="modal-content">
													      <div class="modal-header">
													        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
													        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													          <span aria-hidden="true">&times;</span>
													        </button>
													      </div>



													      <div class="modal-body">




													        {{ Form::open([

												                  'route' => ['comment-user-update',$parent->id],
												                  'method' => 'POST',
												                  'class'=>'form-horizontal form-horizontal row-fluid',
												                  'enctype'=>'multipart/form-data'

												                  ])
												            }}


													          <div class="form-group">
													            <label for="description" class="col-form-label">description:</label>
													            <input type="text" class="form-control" name="description" value="{{ $parent->description }}" required>

											                            @if ($errors->has('description'))
													                        <span class="btn-danger" role="alert">
													                            <strong>{{ $errors->first('description') }}</strong>
													                        </span>
											                            @endif
											                    
										                        </div>
													          

													          <div class="modal-footer">
														        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														        <button type="submit" class="btn btn-primary">Update</button>
														      </div>

													        {{ Form::close() }}






													      </div>
													      
													    </div>
													  </div>
													</div>

									   			</p>
                                                @endif
                                                </span>


									   		</div>
									   	</div>
									   		<div class="collapse" id="comment-{{$parent->id}}">
							                	@foreach(App\Models\Admin\Comment::orderBy('id','asc')->where('parent_id',$parent->id)->get(); as $child)
							                	<div class="review d-flex" >
							                	<img src="{{asset('images/users/'.$child->user->images)}}" class="user-img" alt="">
							                	
							                    <div class="desc">
							                      
							                      <h4>
									   				<span class="text-left">{{$child->user->name}}</span>
									   				<span class="text-right">14 March 2018</span>
									   			</h4>
									   			<span>
									   			<p class="text-left">{{ $child->description}}</p>
									   			@if(Auth::user()->id==$child->user_id)
									   			<p class="star text-right">
								   					<span class="text-right">

								   						<a data-toggle="modal" data-target="#exampleModalCenter{{$child->id}}child"  class="reply">Edit</a>
								   					</span>
													<!-- Modal -->
													<div class="modal fade" id="exampleModalCenter{{$child->id}}child" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
													  <div class="modal-dialog modal-dialog-centered" role="document">
													    <div class="modal-content">
													      <div class="modal-header">
													        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
													        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													          <span aria-hidden="true">&times;</span>
													        </button>
													      </div>



													      <div class="modal-body">




													        {{ Form::open([

												                  'route' => ['comment-user-update',$child->id],
												                  'method' => 'POST',
												                  'class'=>'form-horizontal form-horizontal row-fluid',
												                  'enctype'=>'multipart/form-data'

												                  ])
												            }}


													          <div class="form-group">
													            <label for="description" class="col-form-label">description:</label>
													            <input type="text" class="form-control" name="description" value="{{ $child->description }}" required>

											                            @if ($errors->has('description'))
													                        <span class="btn-danger" role="alert">
													                            <strong>{{ $errors->first('description') }}</strong>
													                        </span>
											                            @endif
											                    
										                        </div>
													          <div class="modal-footer">
														        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														        <button type="submit" class="btn btn-primary">Update</button>
														      </div>

													        {{ Form::close() }}
													      </div>
													    </div>
													  </div>
													</div>
									   			</p>
                                                @endif
                                                <span>
							                  </div>
							              </div>
							                    @endforeach

								                {{ Form::open([
										                'route' => 'user-comment-store',
										                'method' => 'POST',
										                'class'=>'form-horizontal form-horizontal row-fluid',
										                'enctype'=>'multipart/form-data'
										            ])
								                }}
							                    <input type="hidden" name="parent_id" value="{{ $parent->id }}" />
								                <input type="hidden" name="family_id" value="{{ $families->id }}" />
									              <div class="input-group row p-3">
									              	<div class="col-sm-8" >
							            			 <input type="text" class="form-control" name="description" value="{{ old('description') }}" required>
								                            @if ($errors->has('description'))
										                        <span class="btn-danger" role="alert">
										                            <strong>{{ $errors->first('description') }}</strong>
										                        </span>
								                            @endif
								                    
							                        </div>
							                        <button type="submit" class="btn btn-info">Submit</i></button>
							            	     </div>
											    {{ Form::close() }}
							                </div>
								@endforeach
							   		{{ Form::open([
					                    'route' => 'user-comment-store',
					                    'method' => 'POST',
					                    'class'=>'form-horizontal form-horizontal row-fluid',
					                    'enctype'=>'multipart/form-data'
					                 ])
			                    }}
				                <input type="hidden" name="family_id" value="{{ $families->id }}" />
						              <div class="input-group row">
						              	<div class="col-sm-8" >
				            			 <input type="text" class="form-control" name="description" value="{{ old('description') }}" required>
					                            @if ($errors->has('description'))
							                        <span class="btn-danger" role="alert">
							                            <strong>{{ $errors->first('description') }}</strong>
							                        </span>
					                            @endif
					                    
				                        </div>
				                        <button type="submit" class="btn btn-info">Submit</i></button>      
				            		
				            	</div>
			                    {{ Form::close() }}
							   	</div>
						    </div>
						  </div>
						</div>
		      </div>
      		</div>
      	</div>
      	<div class="row" style="margin-top: 300px;">
		                    @foreach($family_other as $family)
          <div class="col-md-4">
            <div class="property-wrap ftco-animate">
              <div id="carousel{{$family->id}}ExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($family->images as $image)
                    <li data-target="#carousel{{$family->id}}ExampleIndicators" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                    @endforeach

                </ol>
                <div class="carousel-inner">
                  @php
                  $i=1
                  @endphp

                  @foreach($family->images as $image)
                  <div class="carousel-item {{$i==1 ? 'active' : ''}}">
                    <p class="img" style="background-image: url({{asset('images/family_room/'.$image->images)}});"></p>
                    <div class="carousel-caption">
		                        <h4 style="color: white">{{$i}}</h4>
		                        <p>Family Sublet Bachelor Rental System</p>
		                        
		                    </div>
                  </div>
                  @php

                  $i++
                  @endphp
                  @endforeach
                  
                </div>
                <a class="carousel-control-prev" href="#carousel{{$family->id}}ExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel{{$family->id}}ExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>


              <div class="text shadow-lg" onclick="location.href='{{route('families-show',$family->id)}}'">

                <div class="text-center">
                    <img src="{{asset('images/categories/'.$family->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">{!! $family->category->name !!}
                </div>
                <hr>
                <ul class="property_list">
                  <li><span class="flaticon-bed"></span>{{ $family->seat}}</li>
                  <li><span class="flaticon-bathtub"></span>{{ $family->room  }}</li>
                </ul>
                <h4 class="badge badge-pill badge-light">{!! $family->room_status!!}</h4>
                <h4 class="badge badge-warning text-center">BDT : {{ $family->room_rent}}/Tk </h4>
                <h4 class="fas fa-clock badge badge badge-pill badge-light">1<sup>st</sup> {{ $family->month }},{{ $family->year}}</h4>
                <h4 class="fas fa-map-marker-alt badge badge-pill badge-light"> {!! $family->ward->name!!},{!! $family->thana->name!!},{!! $family->city->name!!}</h4>
                <span class="far fa-address-card"> {!! $family->address !!}</span>
                <a class="d-flex align-items-center justify-content-center btn-custom">
                  <span class="ion-ios-link"></span>
                </a>
              </div>
            </div>
          </div>
          @endforeach
    </div> 
    </div>
    </section>
@endsection
