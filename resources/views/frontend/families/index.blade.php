@extends('frontend.layouts.master')

@section('title')
    Family Room
@endsection

@section('content')

<section class="hero-wrap hero-wrap-2 ftco-degree-bg js-fullheight" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate pb-5 text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Family Room <i class="ion-ios-arrow-forward"></i></a></span> <span>Properties <i class="ion-ios-arrow-forward"></i></span></p>
            <h1 class="mb-3 bread">Choose <br>Your Desired Family Room</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row">

           @foreach($families as $family)
          <div class="col-md-4">
            <div class="property-wrap ftco-animate">
               <div id="carouselE{{$family->id}}xampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($family->images as $image)
                    <li data-target="#carouselE{{$family->id}}xampleIndicators" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                    @endforeach

                </ol>
                <div class="carousel-inner">
                  @php
                  $i=1
                  @endphp

                  @foreach($family->images as $image)
                  <div class="carousel-item {{$i==1 ? 'active' : ''}}">
                    <p class="img" style="background-image: url({{asset('images/family_room/'.$image->images)}});"></p>
                    <div class="carousel-caption">
                            <h4 style="color: white">{{$i}}</h4>
                            <p>Family Sublet Bachelor Rental System</p>
                            
                        </div>
                  </div>
                  @php

                  $i++
                  @endphp
                  @endforeach
                  
                </div>
                <a class="carousel-control-prev" href="#carouselE{{$family->id}}xampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselE{{$family->id}}xampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

              <div class="text shadow-lg" onclick="location.href='{{route('families-show',$family->id)}}'">

                <div class="text-center">
                    <img src="{{asset('images/categories/'.$family->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">{!! $family->category->name !!}
                </div>
                <hr>

                <h4 class="badge badge-pill badge-light"> {!! $family->room_status!!}</h4> <br> 
                <h4 class="badge badge-warning text-center"> BDT : {{ $family->room_rent}}/Tk </h4> <br>  
                <h4 class="fas fa-clock badge badge badge-pill badge-light"> 1<sup>st</sup> {{ $family->month }},{{ $family->year}}</h4>
                <h4 class="fas fa-map-marker-alt badge badge-pill badge-light"> {!! $family->ward->name!!},{!! $family->thana->name!!},{!! $family->city->name!!}</h4>
                <span class="far fa-address-card"> {!! $family->address !!}</span>
                <a class="d-flex align-items-center justify-content-center btn-custom">
                  <span class="ion-ios-link"></span>
                </a>
              </div>
            </div>
          </div>
          @endforeach



        </div>
        <div class="row mt-5">
          <div class="col text-center">
            <ul class="pagination text-center">
                {{ $families->links()}}
              </ul>
          </div>
        </div>
      </div>
    </section>
@endsection