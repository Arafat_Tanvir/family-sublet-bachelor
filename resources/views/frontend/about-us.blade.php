@extends('frontend.layouts.master')

@section('title')
    About-Us
@endsection

@section('content')

 <section class="hero-wrap hero-wrap-2 ftco-degree-bg js-fullheight" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate pb-5 text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About us <i class="ion-ios-arrow-forward"></i></span></p>
            <h1 class="mb-3 bread">About Us</h1>
          </div>
        </div>
      </div>
    </section>

        <section class="ftco-section ftco-no-pb">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-6 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ asset('/') }}assets/frontend/images/about.jpg);">
                    </div>
                    <div class="col-md-6 wrap-about py-md-5 ftco-animate">
              <div class="heading-section p-md-5">
                <h2 class="mb-4">We Help People For Rent Home.</h2>

                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.</p>
              </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="ftco-counter img" id="section-counter">
        <div class="container">
            <div class="row">
          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
            <div class="block-18 py-4 mb-4">
              <div class="text text-border d-flex align-items-center">
                <strong class="number" data-number="305">0</strong>
                <span>Regular <br>Visitors</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
            <div class="block-18 py-4 mb-4">
              <div class="text text-border d-flex align-items-center">
                <strong class="number" data-number="1090">0</strong>
                <span>Total <br>Listing</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
            <div class="block-18 py-4 mb-4">
              <div class="text text-border d-flex align-items-center">
                <strong class="number" data-number="209">0</strong>
                <span>Confirm <br>Rent</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
            <div class="block-18 py-4 mb-4">
              <div class="text d-flex align-items-center">
                <strong class="number" data-number="67">0</strong>
                <span>Total <br>Client</span>
              </div>
            </div>
          </div>
        </div>
        </div>
    </section>

      <section class="ftco-section ftco-agent ftco-no-pt">
          <div class="container">
              <div class="row justify-content-center pb-5">
                  <div class="col-md-12 heading-section text-center ftco-animate">
                      <span class="subheading">Team</span>
                      <h2 class="mb-4">Our Team</h2>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4 ftco-animate">
                      <div class="agent">
                          <div class="img" >
                              <img src="{{ asset('/') }}assets/frontend/images/tanvir.jpg" style="height: 400px;width: 100%" class="img-fluid" alt="Kamrul Hassan">
                          </div>
                          <div class="desc">
                              <h3><a href="properties.html">MD.Kamrul Hassan</a></h3>
                              <p class="h-info">
                              <span class="location">
                                  ID : C151128 <br>
                                  Dept : CSE <br>
                                  Section : A <br>
                                  International Islamic University Chittagong
                              </span>

                              <span class="details">&mdash; Easy Finder</span></p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 ftco-animate">
                      <div class="agent">
                          <div class="img" >
                              <img src="{{ asset('/') }}assets/frontend/images/mahiuddin2.jpg" style="height: 400px;width: 100%" class="img-fluid" alt="Nayeem Ibrahim">
                          </div>
                          <div class="desc">
                              <h3><a href="properties.html">Mr. Md. Mahiuddin </a></h3>
                              <p class="h-info">
                                <span class="location">
                                  
                                  Assistant Professor <br>
                                  Department of Computer Science and Engineering <br>
                                  International Islamic University Chittagong

                              </span>
                              <span class="details">&mdash; Easy Finder</span></p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 ftco-animate">
                      <div class="agent">
                          <div class="img">
                              <img src="{{ asset('/') }}assets/frontend/images/nayeem.jpg" style="height: 400px;width: 100%" class="img-fluid" alt="Kamrul Hassan">
                          </div>
                          <div class="desc">
                              <h3><a href="properties.html">Nayeem Bin Ibrahim</a></h3>
                              <p class="h-info">
                                <span class="location">
                                  ID : C151028 <br>
                                  Dept : CSE <br>
                                  Section : A <br>
                                  International Islamic University Chittagong
                                </span>
                                <span class="details">&mdash; Easy Finder</span></p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>

@endsection