<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/animate.css">
    
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/magnific-popup.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/aos.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/flaticon.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/icomoon.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/style.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/frontend/css/css/design.css">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  </head>

  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    @include('frontend.partials.nav-bar')
	  </nav>
    <!-- END nav -->

	  @yield('content')

    <footer class="ftco-footer ftco-section">
       @include('frontend.partials.footer')
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
  @include('frontend.partials.scripts')

  @yield('scripts')
    
  </body>
</html>
























