@extends('frontend.layouts.master')

@section('title')
    Bachelor Seat
@endsection

@section('content')


    <section class="hero-wrap hero-wrap-2 ftco-degree-bg js-fullheight" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate pb-5 text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="#">Booking Seat <i class="ion-ios-arrow-forward"></i></a></span> <span>Properties <i class="ion-ios-arrow-forward"></i></span></p>
            <h1 class="mb-3 bread">Choose <br>Your Desired Bachelors</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section" >
      <div class="container">
        <div class="row">
          @foreach($bechelors as $bechelor)
          <div class="col-md-4">
            <div class="property-wrap ftco-animate">
              <div id="carousel{{$bechelor->id}}ExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($bechelor->images as $image)
                    <li data-target="#carousel{{$bechelor->id}}ExampleIndicators" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                    @endforeach

                </ol>
                <div class="carousel-inner">
                  @php
                  $i=1
                  @endphp

                  @foreach($bechelor->images as $image)
                  <div class="carousel-item {{$i==1 ? 'active' : ''}}">
                    
                    <p class="img" style="background-image: url({{asset('images/bechelor_room/'.$image->images)}});"></p>
                    <div class="carousel-caption">
                            <h4 style="color: white">{{$i}}</h4>
                            <p>Family Sublet Bachelor Rental System</p>
                            
                        </div>
                  </div>
                  @php

                  $i++
                  @endphp
                  @endforeach
                  
                </div>
                <a class="carousel-control-prev" href="#carousel{{$bechelor->id}}ExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel{{$bechelor->id}}ExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>


              <div class="text shadow-lg " onclick="location.href='{{route('bachelors-show',$bechelor->id)}}'">

                <div class="text-center">
                    <img src="{{asset('images/categories/'.$bechelor->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">{!! $bechelor->category->name !!}
                </div>
                <hr>
                <ul class="property_list">
                  <li><span class="flaticon-bed"></span>{{ $bechelor->seat}}</li>
                  <li><span class="flaticon-bathtub"></span>{{ $bechelor->room  }}</li>
                </ul>
                <h4 class="badge badge-pill badge-light">{!! $bechelor->room_status!!}</h4>
                <h4 class="badge badge-warning text-center">BDT : {{ $bechelor->room_rent}}/Tk </h4>
                <h4 class="fas fa-clock badge badge badge-pill badge-light">1<sup>st</sup> {{ $bechelor->month }},{{ $bechelor->year}}</h4>
                <h4 class="fas fa-map-marker-alt badge badge-pill badge-light"> {!! $bechelor->ward->name!!},{!! $bechelor->thana->name!!},{!! $bechelor->city->name!!}</h4>
                <span class="far fa-address-card"> {!! $bechelor->address !!}</span>
                <a class="d-flex align-items-center justify-content-center btn-custom">
                  <span class="ion-ios-link"></span>
                </a>
              </div>
            </div>
          </div>
          @endforeach
    </div>
        <div class="row mt-5">
          <div class="col text-center">
              <ul class="pagination text-center">
                {{ $bechelors->links()}}
              </ul>
          </div>
        </div>
      </div>
    </section>