@extends('frontend.layouts.master')
@section('title')
    Sublet-Room
@endsection


@section('content')
<section class="hero-wrap hero-wrap-2 ftco-degree-bg js-fullheight" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate pb-5 text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Sublet <i class="ion-ios-arrow-forward"></i></a></span> <span>Properties <i class="ion-ios-arrow-forward"></i></span></p>
            <h1 class="mb-3 bread">Choose <br>Your Desired Sublet Room</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row">
          @foreach($sublets as $sublet)
          <div class="col-md-4">
            <div class="property-wrap ftco-animate">
              <div id="carousel{{$sublet->id}}ExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($sublet->images as $image)
                    <li data-target="#carousel{{$sublet->id}}ExampleIndicators" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                    @endforeach

                </ol>
                <div class="carousel-inner">
                  @php
                  $i=1
                  @endphp

                  @foreach($sublet->images as $image)
                  <div class="carousel-item {{$i==1 ? 'active' : ''}}">
                    <p class="img" style="background-image: url({{asset('images/sublet_room/'.$image->images)}});"></p>
                  </div>
                  @php

                  $i++
                  @endphp
                  @endforeach
                  
                </div>
                <a class="carousel-control-prev" href="#carousel{{$sublet->id}}ExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel{{$sublet->id}}ExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
              <div class="text shadow-lg " onclick="location.href='{{route('sublets-show',$sublet->id)}}'">

                <div class="text-center">
                    <img src="{{asset('images/categories/'.$sublet->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">{!! $sublet->category->name !!}
                </div>
                <hr>
                
                <h4 class="badge badge-pill badge-light"> {!! $sublet->room_status!!}</h4> <br> 
                <h4 class="badge badge-warning text-center"> BDT : {{ $sublet->room_rent}}/Tk </h4> <br>  
                <h4 class="fas fa-clock badge badge badge-pill badge-light"> 1<sup>st</sup> {{ $sublet->month }},{{ $sublet->year}}</h4>
                <h4 class="fas fa-map-marker-alt badge badge-pill badge-light"> {!! $sublet->ward->name!!},{!! $sublet->thana->name!!},{!! $sublet->city->name!!}</h4>
                <span class="far fa-address-card"> {!! $sublet->address !!}</span>
                <a class="d-flex align-items-center justify-content-center btn-custom">
                  <span class="ion-ios-link"></span>
                </a>
              </div>
            </div>
          </div>
          @endforeach


    </div>
        <div class="row mt-5">
          <div class="col text-center">
            <ul class="pagination text-center">
                {{ $sublets->links()}}
              </ul>
          </div>
        </div>
      </div>
    </section>
    @endsection