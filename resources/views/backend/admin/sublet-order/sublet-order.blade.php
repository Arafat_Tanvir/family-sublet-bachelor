@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            @if((Session::get('succss')))
			<div class="alert alert-success alert-dismissable">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					{{ Session::get('message')}}
				</div>
			</div>
			@endif
            
			@if($sublet_orders->count() >0)
			<div class="card-body">
              <div class="mt-2">
                 <div class="table-responsive mt-2">

			<table id="orders" class="table table-bordered">
				<thead>
					<tr>
						<th>SL</th>
            <th>Name</th>
						<th>Name</th>
            <th>email</th>
            <th>Order Status</th>
            <th>phone</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<div style="display: none;">{{$a=1}}</div>
						@foreach($sublet_orders as $order)
						<td>{{ $a++ }}</td>
						<td>TM-{{ $order->id }}</td>
            <td>{{ $order->name }}</td>
            <td>{{ $order->email }}</td>
            <td>
              <p>
              @if($order->is_paid)
              <button type="button" class="badge badge-success">Paid</button>
              @else
              <button type="button" class="badge badge-warning">UnPaid</button>
              @endif
              @if($order->is_complete)
              <button type="button" class="badge badge-success mt-2">Complete</button>
              @else
              <button type="button" class="badge badge-dengar mt-2">Not Complete</button>
              @endif
              @if($order->is_seen_by_admin)
              <button type="button" class="badge badge-success mt-2">Seen</button>
              @else
              <button type="button" class="badge badge-info mt-2">Unseen</button>
              @endif
              </p>
            </td>
            <td>{{ $order->phone }}</td>
						<td>
              <a href="{{route('sublets-order-show',$order->id )}}" class="badge badge-primary">Show</a>
							<a href="{{route('sublets-order-edit', $order->id)}}" class="badge badge-warning mt-2">Edit</a>
							<a href="#DeleteModal{{ $order->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('sublets-order-delete', $order->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="btn btn-primary btn-sm">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>

							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
			<div class="text-center">NO Order</div>
			@endif
		</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$(document).ready(function() {
    $('#orders').DataTable();
} );
</script>
@endsection
