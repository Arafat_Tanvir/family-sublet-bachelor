@extends('Backend.admin.layouts.master')
@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>City Edit</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('cities.index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
      <form method="POST" action="{{ route('cities.update',$cities->id) }}" >
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="name">City Name</label>
          <div class="form-input">
            <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" value="{{ $cities->name}}">
            <div class="invalid-feedback">
              {{ ($errors->has('name')) ? $errors->first('name') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="bangla_name">City Bangla Name</label>
          <div class="form-input">
            <input type="text" class="form-control is-valid form-control-sm" name="bangla_name" id="bangla_name" value="{{ $cities->bangla_name? $cities->bangla_name:'N\A'}}">
            <div class="valid-feedback">
              {{ ($errors->has('bangla_name')) ? $errors->first('bangla_name') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="latitude">City Latitude</label>
          <div class="form-input">
            <input type="double" class="form-control is-valid form-control-sm input-md" step="0.01" name="latitude" id="latitude" value="{{ $cities->latitude? $cities->latitude:'0.00'}}">
            <div class="valid-feedback">
              {{ ($errors->has('latitude')) ? $errors->first('latitude') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="longitude">City Longitude</label>
          <div class="form-input">
            <input type="double" class="form-control is-valid form-control-sm input-md"step="0.01" name="longitude" id="longitude" value="{{ $cities->longitude? $cities->longitude:'0.00'}}">
            <div class="valid-feedback">
              {{ ($errors->has('longitude')) ? $errors->first('longitude') : ''}}
            </div>
          </div>
        </div>
        <button class="btn btn-primary" type="submit">Update</button>
      </form>
    </div>
    </div>
<div class="col-sm-4">
  <div id="map">
  </div>
</div>
@endsection