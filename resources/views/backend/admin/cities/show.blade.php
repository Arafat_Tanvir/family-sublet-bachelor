@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('cities.index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
      <div class="row">
        
        <div class="col-sm-6">
          <table class="table table-bordered"> 
          <thead> 
            <tr>
            <th>name</th> 
            <td> {{$cities->name}} </td> 
            </tr>
            <tr>
            <th>Bangla Name</th> 
            <td> {{$cities->bangla_name?$cities->bangla_name:'N\A'}} </td> 
            </tr>
            <tr>
            <th>Latitude</th> 
            <td> {{$cities->latitude?$cities->latitude:'N\A'}}</td> 
            </tr>
            <tr>
            <th>Longitude</th> 
            <td> {{$cities->longitude?$cities->longitude:'N\A'}} </td> 
            </tr>

          </thead>
        </table>
        </div>
        <div class="col-sm-6">
               <div style="height: 300px;">
                  {!! Mapper::render() !!}
               </div>
        </div>

      </div>
    </div>
  </div>
@endsection
