@extends('Backend.admin.layouts.master')

@section('content')

          <div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>City List</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('cities.create')}}"><i class="fas fa-plus-circle"></i></a></h1>
              <div class="table-responsive mt-2">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <caption>List of cities</caption>
                        <thead>
                          <tr>
                            <th>SL</th>
                            <th>Name</th>
                            <th>Bangla name</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Show Details</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <div style="display: none;">{{$a=1}}</div>
                            @foreach($cities as $city)
                            <td>{{ $a++ }}</td>
                            <td>{{ $city->name }}</td>
                            <td>
                                @if($city->bangla_name)
                                <p>{{ $city->bangla_name}}</p>
                                @else
                                  <p>N/A</p>
                                @endif
                            </td>
                            <td>
                                @if($city->latitude)
                                <p>{{ $city->latitude}}</p>
                                @else
                                  <p>N/A</p>
                                @endif
                            </td>
                            <td>
                                @if($city->longitude)
                                <p>{{ $city->longitude}}</p>
                                @else
                                  <p>N/A</p>
                                @endif
                            </td>
                            <td>
                              <a href="{{route('cities.show', $city->id)}}" class="badge badge-primary">Show</a>
                            </td>
                            <td>
                              <a href="{{route('cities.edit', $city->id)}}" class="badge badge-warning">Edit</a>
                              <a href="#" onclick="return confirm('are you sure')">
                                <form class="pull-right form-inline" action="{{route('cities.destroy', $city->id)}}" method="POST">
                                  {{csrf_field()}}
                                  {{method_field('DELETE')}}
                                  <button class="badge badge-danger" type="submit">Delete</button>
                                </form>
                              </a>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
            </div>
          </div>

@endsection
