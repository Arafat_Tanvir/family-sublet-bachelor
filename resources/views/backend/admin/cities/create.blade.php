@extends('Backend.admin.layouts.master')
@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h1 class="text-center">City Create</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('cities.index')}}"><i class="fas fa-backward"></i></a></h1>
      <form method="POST" action="{{ route('cities.store') }}">
        @csrf
        <div class="form-group">
          <label for="name">City Name</label>
          <div class="form-input">
            <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" placeholder="Enter City name" value="" required>
            <div class="invalid-feedback">
              {{ ($errors->has('name')) ? $errors->first('name') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="bangla_name">City Bangla Name</label>
          <div class="form-input">
            <input type="text" class="form-control is-valid form-control-sm" name="bangla_name" id="bangla_name" placeholder="Enter City Bangla Name" value="">
            <div class="valid-feedback">
              {{ ($errors->has('bangla_name')) ? $errors->first('bangla_name') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="latitude">City Latitude</label>
          <div class="form-input">
            <input type="double" class="form-control" name="latitude" id="latitude" placeholder="Enter City latitude" value="">
            <div class="valid-feedback">
              {{ ($errors->has('latitude')) ? $errors->first('latitude') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="longitude">City Longitude</label>
          <div class="form-input">
            <input type="double" class="form-control"name="longitude" id="longitude" placeholder="Enter City longitude" value="">
            <div class="valid-feedback">
              {{ ($errors->has('longitude')) ? $errors->first('longitude') : ''}}
            </div>
          </div>
        </div>
        <button class="btn btn-primary" type="submit">Create</button>
      </form>
    </div>
  </div>
@endsection