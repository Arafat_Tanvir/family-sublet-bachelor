@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3 text-center">
         <h1>Category Show</h1>
     </div>
     <div class="card-body">
        <h1 class="text-right"><a href="{{route('categories.index')}}"><i class="fas fa-backward"></i></a></h1>
           <div class="row">
               <div class="col-sm-8">
                   <form method="post" action="{{ route('categories.update',$categories->id) }}" enctype="multipart/form-data" >
                       @csrf
                       <div class="form-group">
                           <label for="name">Category Name</label>
                           <div class="form-input">
                               <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" value="{{ $categories->name}}">
                               <div class="invalid-feedback">
                                 {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                               </div>
                           </div>
                       </div>

                       <div class="form-group">
                           <label for="parent_id">Primary Name Off Category</label>
                           <div class="form-input">
                               <select name="parent_id" id="parent_id" class="form-control is-valid form-control-sm input-md">
                                       <option value="0" disabled="true" selected="true">===Select Parent Category===</option>
                                       @foreach($main_categories as $category)
                                       <option value="{{$category->id}}" {{ $category->id == $categories->parent_id ? 'selected' : ''}}>{{$category->name}}</option>
                                       @endforeach
                               </select>
                               <div class="valid-feedback">
                                 {{ ($errors->has('parent_id')) ? $errors->first('parent_id') : ''}}
                               </div>
                           </div>
                       </div>


                        <div class="form-group">
                            <label for="description">Description</label>
                            <div class="form-input">
                                <textarea name="description" cols="4" rows="5" class="form-control is-valid form-control-sm input-md" id="description">{{ $categories->description }}</textarea>
                                <div class="valid-feedback">
                                  {{ ($errors->has('description')) ? $errors->first('description') : ''}}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="images">Category Image</label>
                            <div class="form-input">
                                <input type="file" class="form-control is-valid form-control-sm input-md" name="images" id="images" value="{{ old('images')}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('images')) ? $errors->first('images') : ''}}
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                </div>
             
               <div class="col-sm-4">
                 <img class="card-img-top-image" src="{{asset('images/categories/'.$categories->images)}}" height="400px" width="300px">
               </div>
        </div>
    </div>
</div>
@endsection
