@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Category List</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('categories.create')}}"><i class="fas fa-plus-circle"></i></a></h1>
              <div class="mt-2">
      <div class="table-responsive mt-2">
        <table id="categories" class="table table-bordered table-striped">
          <caption>List of categories</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Description</th>
  						<th>Image</th>
              <th>Parent Of the Category</th>
              <th>Show</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($categories as $category)
  						<td>{{ $a++ }}</td>
  						<td>{{ $category->name }}</td>
  						<td>
  						    @if($category->description)
                  <p>{{ $category->description}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($category->images)
                  <p>
                    <img class="card-img-top-image" src="{{asset('images/categories/'.$category->images)}}" height="100px" width="200px">
                  </p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
							@if($category->parent_id==null)
              <b style="color: red">
							Primary Category
              </b>
							@else
              <b style="color: green">
							{{ $category->parent->name }}
              </b>
							@endif
						</td>
              <td>
                <a href="{{route('categories.show', $category->id)}}" class="badge badge-primary">Show</a>
              </td>
  						<td>
  							<a href="{{route('categories.edit', $category->id)}}" class="badge badge-warning">Edit</a>
                <a href="#DeleteModal{{ $category->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('categories.delete', $category->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="badge badge-success">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
	$(document).ready(function() {
    $('#categories').DataTable();
} );
</script>
@endsection
