@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Category Create</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('categories.index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="row">
                <div class="col-sm-8">
          
             
                   <table class="table table-bordered table-striped">
                     <thead>
                       <tr>
                         <th>Parent Category</th>
                         <th>Category Name</th>
                         <th>Description</th>
                         
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td>{{$categories->parent->name}}</td>
                         <td> {{$categories->name}} </td>
                         <td>{{$categories->description }}</td>
                       </tr>
                     </tbody>

            </table>
          </div>
        <div class="col-sm-4">
                <p>
                    <img class="card-img-top-image" src="{{asset('images/categories/'.$categories->images)}}" height="200px" width="200px">
                  </p>

  </div>
        </div>

      </div>
 
</div>
@endsection
