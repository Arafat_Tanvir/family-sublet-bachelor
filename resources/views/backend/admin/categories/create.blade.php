@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Category Create</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('categories.index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
      <form method="POST" class="user" action="{{ route('categories.store') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group">
              <label for="name">Name</label>
              <div class="form-input">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="name" id="name" placeholder="Enter Category name" value="{{old('name')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group">
              <label for="parent_id">Select Parent Category</label>
              <div class="form-input">
                  <select name="parent_id" id="parent_id" class="form-control form-control-user is-valid form-control-sm input-md" value="">
                          <option value="0" disabled="true" selected="true">===Select Parent Category===</option>
                          @foreach($categories as $category)
                          <option value="{{$category->id}}">{{$category->name}}</option>
                          @endforeach
                  </select>
                  <div class="valid-feedback">
                    {{ ($errors->has('parent_id')) ? $errors->first('parent_id') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group">
              <label for="description">Description</label>
              <div class="form-input">
                  <textarea name="description" cols="4" rows="5" value="{{old('description')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="description"required>{{ old('description')}}</textarea>
                  <div class="valid-feedback">
                    {{ ($errors->has('description')) ? $errors->first('description') : ''}}
                  </div>
              </div>
          </div>


          <div class="form-group">
              <label for="images">Category Image</label>
              <div class="form-input">
                  <input type="file" class="form-control form-control-user is-valid form-control-sm" name="images" id="images" placeholder="Enter Ward Bangla Name" value="">
                  <div class="valid-feedback">
                    {{ ($errors->has('images')) ? $errors->first('images') : ''}}
                  </div>
              </div>
          </div>


          <button class="btn btn-primary" type="submit">Create</button>
      </form>
    </div>
  </div>
</div>

</div>
</div>
@endsection
