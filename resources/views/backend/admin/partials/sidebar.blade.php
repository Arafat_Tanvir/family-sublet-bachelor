<ul class="navbar-nav bg-info sidebar sidebar-dark accordion" id="accordionSidebar">
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('/')}}">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Easy Finder</div>
  </a>
  

  

  <li class="nav-item active">
    <a class="nav-link" href="{{ route('admin.dashboard')}}"><i class="fas fa-fw fa-tachometer-alt" style="font-size:20px;color:white"></i><span>Admin Dashboard</span></a>
  </li>

  

  <div class="sidebar-heading">
    Family-Sublet-Bachelor
  </div>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwofamily" aria-expanded="true" aria-controls="collapseTwo"><i class="far fa-object-ungroup" style="font-size:20px;color:white"></i><span>Family Control</span>
      </a>
      <div id="collapseTwofamily" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="{{ route('families-admin-pending')}}">Advertisement Pending</a>
          <a class="collapse-item" href="{{ route('families-admin-confirm')}}">Advertisement Confirm</a>
          <a class="collapse-item" href="{{ route('families-admin-complete')}}">Advertisement Complete</a>
          <a class="collapse-item" href="{{ route('families-order-index')}}">All Order </a>
          <a class="collapse-item" href="{{ route('families-order-pending')}}">Pending Order </a>
          <a class="collapse-item" href="{{ route('families-order-complete')}}">Complete Order </a>
        </div>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwosublet" aria-expanded="true" aria-controls="collapseTwo"><i class="fab fa-app-store" style="font-size:20px;color:white"></i><span>Sub-Let Control</span>
      </a>
      <div id="collapseTwosublet" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          
          <a class="collapse-item" href="{{ route('sublets-admin-pending')}}">Advertisement Pending</a>
          <a class="collapse-item" href="{{ route('sublets-admin-confirm')}}">Advertisement Confirm</a>
          <a class="collapse-item" href="{{ route('sublets-admin-complete')}}">Advertisement Complete</a>
          <a class="collapse-item" href="{{ route('sublets-order-index')}}"> All Order</a>
          <a class="collapse-item" href="{{ route('sublets-order-pending')}}">Pending Order </a>
          <a class="collapse-item" href="{{ route('sublets-order-complete')}}">Complete Order </a>
          
        </div>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwobachelor" aria-expanded="true" aria-controls="collapseTwo"><i class="fas fa-bed" style="font-size:20px;color:white"></i><span>Bechelor Control</span>
      </a>
      <div id="collapseTwobachelor" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
         
          
          <a class="collapse-item" href="{{ route('bechelors-admin-pending')}}"> Advertisement Pending </a>
          <a class="collapse-item" href="{{ route('bechelors-admin-confirm')}}"> Advertisement Confirm</a>
          <a class="collapse-item" href="{{ route('bechelors-admin-complete')}}"> Advertisement Complete</a>
          <a class="collapse-item" href="{{ route('bachelors-order-index')}}">All Order </a>
           <a class="collapse-item" href="{{ route('bachelors-order-pending')}}">Pending Order </a>
          <a class="collapse-item" href="{{ route('bachelors-order-complete')}}">Complete Order </a>
          
        </div>
      </div>
    </li>

  

    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwoCategory" aria-expanded="true" aria-controls="collapseTwo"><i class=" fa fa-barcode" style="font-size:20px;color:white"></i><span>Categoires Control</span>
      </a>
      <div id="collapseTwoCategory" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="{{ route('categories.index')}}">Categories show</a>
          <a class="collapse-item" href="{{ route('categories.create')}}">Categories Create</a>
        </div>
      </div>
    </li>

   

    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilitiespostlocation" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-crosshairs" style="font-size:20px;color:white"></i>
        <span>Location</span>
      </a>
      <div id="collapseUtilitiespostlocation" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="{{ route('cities.index')}}">Cities</a>
          <a class="collapse-item" href="{{ route('thanas.index')}}">Thanas</a>
          <a class="collapse-item" href="{{ route('wards.index')}}">Wards</a>
        </div>
      </div>
    </li>
    
    
    <hr class="sidebar-divider d-none d-md-block">
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    </ul>