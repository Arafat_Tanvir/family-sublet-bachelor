<script src="{{ asset('Backend/Admin/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('Backend/Admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('Backend/Admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('Backend/Admin/js/sb-admin-2.min.js')}}"></script>

<script src="{{ asset('Backend/Admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('Backend/Admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('Backend/Admin/js/demo/datatables-demo.js')}}"></script>

<!-- Page level plugins -->

 <!-- Page level plugins -->
  <script src="{{ asset('Backend/Admin/vendor/chart.js/Chart.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('Backend/Admin/js/demo/chart-area-demo.js')}}"></script>
  <script src="{{ asset('Backend/Admin/js/demo/chart-pie-demo.js')}}"></script>

<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>