@extends('backend.admin.layouts.master')

@section('content')
 <div class="container">
   <div class="row">
          <div class="col-sm-12">
            <div class="o-hidden border-0 shadow-lg my-3 card">
            <div class="card-body">
				<div class="text-center">
                    <div class="card-header" style="background-color: #36B9CC;color: white">
                       <img src="{{asset('images/categories/'.$bechelors->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">
                    
                      <h6 class="mb-0">
                        {{ $bechelors->room_status}},{{ $bechelors->seat}} Seat
                      </h6>
                      <h6 class="d-none d-sm-inline-block">1st {{$bechelors->month.','.$bechelors->year}}</h6>
                      <h6 class="d-none d-sm-inline-block">BDT : {{ $bechelors->room_rent}}</h6>
                    </div>
                </div>
		            <div id="demo" class="carousel slide" data-ride="carousel">
	                    <div class="carousel-inner">
	                      <div class="carousel-inner">
	                        @php
	                        $i=1
	                        @endphp
	                        @foreach($bechelors->images as $image)
	                        <div style="text-align: center;" class="carousel-item {{$i==1 ? 'active' : ''}}">
	                          <img class="card-img-top-image" src="{{asset('images/bechelor_room/'.$image->images)}}" alt="" width="100%"  height="420px" ">
	                        </div>
	                        @php
	                        $i++
	                        @endphp
	                        @endforeach
	                      </div>
	                    </div>
	                    <!-- Left and right controls -->
	                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
	                      <span class="carousel-control-prev-icon"></span>
	                    </a>
	                    <a class="carousel-control-next" href="#demo" data-slide="next">
	                      <span class="carousel-control-next-icon"></span>
	                    </a>
	                  </div>

	                  <div id="accordion" style="margin-top: 30px;">
  <div class="card">
    <div class="card-header" id="headingOne" style="background-color: #36B9CC; color: white;text-align: center;">
     
        <h4 class="text-uppercase" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
           Necessary Information
        </h4>
    
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <div class="table-responsive">
	            <table class="table" style="background: pink">
	             	<tr>
	             	 	<th>City</th>
	             	 	<td>{{ $bechelors->city->name}}</td>
	             	</tr>
	             	<tr>
	             	 	<th>Thana</th>
	             	 	<td>{{ $bechelors->thana->name}}</td>
	             	</tr>
	             	<tr>
	             	 	<th>Ward</th>
	             	 	<td>{{ $bechelors->Ward->name}}</td>
	             	</tr>
	                <tr>
	             	 	<th>Religion : </th>
	             	 	<td>{{ $bechelors->religion}}</td>
	             	</tr>
	             	<tr>
	             	 	<th>Mobile</th>
	             	 	<td>{{ $bechelors->phone}}</td>
	             	</tr>
	            </table>
	        </div>


	        <div class="text-left py-1">
				<h4>Facilities</h4>
				<ul style="list-style-type:square;">
				    <li>{{ $bechelors->facilities}}</li>
				</ul>
			</div>
			<div class="text-left py-1">
			    <h4>Conditions</h4>
				<ul style="list-style-type:square;">
				    <li>{{ $bechelors->conditions}}</li>
				</ul>
			</div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo" style="background-color: #36B9CC;color: white;text-align: center;">
     
        <h4 class="collapsed text-uppercase" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Discription
        </h4>
     
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
       <ul>
        <span>{!! $bechelors->short_description !!}</span>
    </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree" style="background-color: #36B9CC;color: white;text-align: center;">
      
        <h4 class="collapsed text-uppercase" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Address & location & Map
        </h4>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <div class="text-left py-1">					    			   
			    <ul>
			        <span>{!! $bechelors->address !!}</span>
			    </ul>
			</div>
        	<div class="table-responsive">
                <table class="table" style="background: pink">
                 	<tr>
                 	 	<th>City</th>
                 	 	<td>{{ $bechelors->city->name}}</td>
                 	</tr>
                 	<tr>
                 	 	<th>Thana</th>
                 	 	<td>{{ $bechelors->thana->name}}</td>
                 	</tr>
                 	<tr>
                 	 	<th>Ward</th>
                 	 	<td>{{ $bechelors->Ward->name}}</td>
                 	</tr>
                </table>
            </div>
           
            <div style="height: 300px;">
			    {!! Mapper::render() !!}
			</div>
      </div>
    </div>
  </div>
</div> 
						        <div class="p-5">
						            
						              <div class="d-sm-flex align-items-center justify-content-between mb-0">
						              	<h4>Comments</h4>
						                {{ str_plural('comment', $bechelors->comments->count()) }} {{$bechelors->comments->count()}}
						             
						            </div>
						            <div class="card-body chat" id="chat-box">
						            	 @foreach(App\Models\Admin\Comment::orderBy('id','asc')->where('bachelor_id',$bechelors->id)->where('parent_id',NULL)->get(); as $parent)
						              
						              <div class="align-items-center justify-content-between mb-0">
						                <img src="{{asset('images/users/'.$parent->user->images)}}" alt="user image" height="40px;" width="40px;" class="img-profile rounded-circle">

						                 {{$parent->user->name}}
						               </div>
						               
						               <div class="d-sm-flex align-items-center justify-content-between mb-0">
						                   
						                   <p class="p-3">{{$parent->description}}</p>
						                   <span href="#comment-{{$parent->id}}" data-toggle="collapse" class="btn btn-info float-right">Reply</span>
						               </div>

						                
						                <div class="attachment">
						                	<div class="child-rows collapse" id="comment-{{$parent->id}}">
							                	@foreach(App\Models\Admin\Comment::orderBy('id','asc')->where('parent_id',$parent->id)->get(); as $child)
							                   <div class="align-items-center justify-content-between mb-0">
							                      <img src="{{asset('images/users/'.$child->user->images)}}" alt="user image" height="40px;" width="40px;" class="img-profile rounded-circle">

									                  
									                    {{$child->user->name}}
							                  </div>
							                  <p class="p-3">
							                   {{$child->description}}
							                   

							                  @endforeach
							                  <div class="child-rows">
								                {{ Form::open([
										                'route' => 'admin-comment-store',
										                'method' => 'POST',
										                'class'=>'form-horizontal form-horizontal row-fluid',
										                'enctype'=>'multipart/form-data'
										            ])
								                }}
							                    <input type="hidden" name="parent_id" value="{{ $parent->id }}" />
								                <input type="hidden" name="bachelor_id" value="{{ $bechelors->id }}" />
									              <div class="input-group">
									               <input type="text" class="form-control" name="description" value="{{ old('description') }}" required>
									                @if ($errors->has('description'))
								                        <span class="btn-danger" role="alert">
								                            <strong>{{ $errors->first('description') }}</strong>
								                        </span>
								                    @endif

									                <div class="input-group-btn">
									                  <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
									                </div>
									              </div>
											    {{ Form::close() }}
								            </div>
							                </div>
							           
						               
						              </div>
						              @endforeach
						           
						            </div>

						        {{ Form::open([
					                    'route' => 'admin-comment-store',
					                    'method' => 'POST',
					                    'class'=>'form-horizontal form-horizontal row-fluid',
					                    'enctype'=>'multipart/form-data'
					                 ])
			                    }}
				                <input type="hidden" name="bachelor_id" value="{{ $bechelors->id }}" />
				            	 <div class="box-footer">
						              <div class="input-group">
				            			 <input type="text" class="form-control" name="description" value="{{ old('description') }}" required>
				                            @if ($errors->has('description'))
				                        <span class="btn-danger" role="alert">
				                            <strong>{{ $errors->first('description') }}</strong>
				                        </span>
				                            @endif
				                            <div class="input-group-btn">
						                  <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
						                </div>
				            		</div>
				            	</div>
			                    {{ Form::close() }}
						            <!-- /.chat -->
						           
						                
						              </div>
						            
						          </div>
					    	</div>
					    </div>
					</div>
				</div>
@endsection
