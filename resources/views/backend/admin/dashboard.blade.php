@extends('Backend.admin.layouts.master')


@section('content')
          <h2 class="font-weight-bold text-uppercase p-3 bg-info text-center text-white ">POST DETAILS</h2>
          <div class="row p-5">

            <!-- Earnings (Monthly) Card Example -->
             <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('families-admin-pending')}}'">
              <div class="card border-left-warning shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-warning text-uppercase my-4">Family Pending Post</h4>
                      <hr>
                      <div class="bg-warning font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $families_pending->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
             <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('sublets-admin-pending')}}'">
              <div class="card border-left-warning shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-warning text-uppercase my-4">Sublet Pending Post</h4>
                      <hr>
                      <div class="bg-warning font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $sublets_pending->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('bechelors-admin-pending')}}'">
              <div class="card border-left-warning shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-warning text-uppercase my-4">Bachelor Pending Post</h4>
                      <hr>
                      <div class="bg-warning font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $bechelors_pending->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>











            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('families-admin-confirm')}}'">
              <div class="card border-left-info shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-info text-uppercase my-4">Family Pending Post</h4>
                      <hr>
                      <div class="bg-info font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $families_confirm->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
             <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('sublets-admin-confirm')}}'">
              <div class="card border-left-info shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-info text-uppercase my-4">Sublet Confirm Post</h4>
                      <hr>
                      <div class="bg-info font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $sublets_confirm->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('bechelors-admin-confirm')}}'">
              <div class="card border-left-info shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-info text-uppercase my-4">Bachelor Confirm Post</h4>
                      <hr>
                      <div class="bg-info font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $bechelors_confirm->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>







            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('families-admin-complete')}}'">
              <div class="card border-left-success shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-success text-uppercase my-4">Family Complete Post</h4>
                      <hr>
                      <div class="bg-success font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $families_complete->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
             <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('sublets-admin-complete')}}'">
              <div class="card border-left-success shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-success text-uppercase my-4">Sublet Complete Post</h4>
                      <hr>
                      <div class="bg-success font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $sublets_complete->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('bechelors-admin-complete')}}'">
              <div class="card border-left-success shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-success text-uppercase my-4">Bachelor Complete Post</h4>
                      <hr>
                      <div class="bg-success font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $bechelors_complete->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>
          </div>


          
          <h2 class="font-weight-bold text-uppercase p-3 bg-info text-center text-white ">ORDER DETAILS</h2>
         


       <div class="row p-5">


            <!-- Pending Requests Card Example -->
            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('families-order-index')}}'">
              <div class="card border-left-info shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-info text-uppercase my-4">Family Order Request</h4>
                      <hr>
                      <div class="bg-info font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $family_orders->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>


            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('sublets-order-index')}}'">
              <div class="card border-left-info shadow h-100 py-2">
                
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-info text-uppercase my-4">Sublet Order Request</h4>
                      <hr>
                      <div class="bg-info font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $sublet_orders->count() }}</div>
                    </div>
                  </div>
               
              </div>
            </div>

            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('bachelors-order-index')}}'">
              <div class="card border-left-info shadow h-100 py-2">
                
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                       <h4 class="font-weight-bold text-info text-uppercase my-4">Bachelor Order Request</h4>
                      <hr>
                      <div class="bg-info font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total {{ $bachelor_orders->count() }}</div>
                    </div>
                  </div>
                </div>
             
            </div>

            




            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('families-order-pending')}}'">
              <div class="card border-left-warning shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-warning text-uppercase my-4">Family Order Pending</h4>
                      <hr>
                      <div class="bg-warning font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total Pending {{ $family_pending->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
             <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('sublets-order-pending')}}'">
              <div class="card border-left-warning shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-warning text-uppercase my-4">Sublet order Pending</h4>
                      <hr>
                      <div class="bg-warning font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total Pending {{ $sublet_pending->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('bachelors-order-pending')}}'">
              <div class="card border-left-warning shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-warning text-uppercase my-4">Bachelor Order Pending</h4>
                      <hr>
                      <div class="bg-warning font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total Pending : {{ $bachelor_pending->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>










            <!-- Pending Requests Card Example -->
            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('families-order-complete')}}'">
              <div class="card border-left-success shadow h-100 py-2">
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-success text-uppercase my-4">Family Order Complete</h4>
                      <hr>
                      <div class="bg-success font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total complete {{ $family_complete->count() }}</div>
                    </div>
                  </div>
              </div>
            </div>


            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('sublets-order-complete')}}'">
              <div class="card border-left-success shadow h-100 py-2">
                
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                      <h4 class="font-weight-bold text-success text-uppercase my-4">Sublet Order Complete</h4>
                      <hr>
                      <div class="bg-success font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total complete {{ $sublet_complete->count() }}</div>
                    </div>
                  </div>
               
              </div>
            </div>

            <div class="col-xl-4 col-md-6 mb-4" onclick="location.href='{{route('bachelors-order-complete')}}'">
              <div class="card border-left-success shadow h-100 py-2">
                
                  <div class="row no-gutters align-items-center">
                    <div class="card-body text-center">
                       <h4 class="font-weight-bold text-success text-uppercase my-4">Bachelor Order Complete</h4>
                      <hr>
                      <div class="bg-success font-weight-bold text-white p-3 rotate-n-15 d-inline-block my-4">Total Complete : {{ $bachelor_complete->count() }}</div>
                    </div>
                  </div>
                </div>
             
            </div>

          </div>

@endsection