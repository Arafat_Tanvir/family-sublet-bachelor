@extends('Backend.admin.layouts.master')
@section('content')
     <div class="o-hidden border-0 shadow-lg my-3 p-5 card">
     	<div class="row">
        <div class="col-sm-12">
          
        <div class="row badge-pill p-5 shadow-lg" style="color: black">
	         <div class="col-sm-4">
            <h4 class="float-center font-weight-bold text-info text-uppercase">Advertiser Information</h4><hr>
            @foreach($bachelor_order->cart as $card)
	                   <h6>Name:  {{ $card->bachelor->user->name }} </h6>
	                   <h6>Email:  {{ $card->bachelor->user->email}} </h6>
	                   <h6>Phone: {{ $card->bachelor->phone }} </h6>
                      @php
                        $id=$card->bachelor->id;
                      @endphp
	        @endforeach
	          </div>

	           <div class="col-sm-4">
              <h4 class="float-center font-weight-bold text-info text-uppercase">Payment Information</h4><hr>
              <table class="table table-bordered">
                <thead >
                  <tr>
                    <th>Mathod</th>
                    <td>{{ $bachelor_order->payment->name }}</td>
                  </tr>
                  <tr>
                    <th>Id</th>
                    <td>{{ $bachelor_order->transaction_id }}</td>
                  </tr>
                </thead>
              </table>

            </div>

             <div class="col-sm-4" >
              <h4 class="float-center font-weight-bold text-info text-uppercase">Booker Information</h4><hr>
                     <h6>Name:  {{ $bachelor_order->name }} </h6>
                     <h6>Email:  {{ $bachelor_order->email}} </h6>
                     <h6>Phone {{ $bachelor_order->phone }} </h6>
             </div>
      </div>
            <div class="o-hidden border-0 shadow-lg my-3 card">
            <div class="card-body">
              
              <table class="table table-bordered">
				<thead>
					<tr>
						<th rowspan="2">SL</th>
						<th rowspan="2">Category</th>
						<th rowspan="2">Image</th>
						<th rowspan="2">Seat Quantity</th>
						<th rowspan="2">Per Seat Price</th>
						<th rowspan="2">Sub Total Price</th>
						<th rowspan="2">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<div style="display: none;">{{$a=1}}</div>

						@foreach($bachelor_order->cart as $card)
          
						<td>{{ $a++ }}</td>
						<td>{{ $card->bachelor->category->name }}</td>
						<td style="width: 220px">

							<div id="demo" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <div class="carousel-inner">
                        @php
                        $i=1
                        @endphp
                        @foreach($card->bachelor->images as $image)
                        <div style="text-align: center;" class="carousel-item {{$i==1 ? 'active' : ''}}">
                          <img class="img-fluid img-thumbnail" src="{{asset('images/bechelor_room/'.$image->images)}}" alt="">
                        </div>
                        @php
                        $i++
                        @endphp
                        @endforeach
                      </div>
                    </div>
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                      <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                      <span class="carousel-control-next-icon"></span>
                    </a>
                  </div>
						</td>
	            	
						<td>
							<form style="margin-left: 40px" class="form-inline" action="{{ route('admin-cards-update', $card->id)}}" method="POST">
								{{csrf_field()}}
									<input type="number" value="{{$card->seat_quantity}}" name="seat_quantity" class="col-sm-6 form-control">
							        <button type="submit" class="btn btn-primary btn-sm">Update</button>
							</form>
						</td>
						<td>{{ $card->bachelor->room_rent}}</td>
						@php

						$total_taka=$card->bachelor->room_rent * $card->seat_quantity;

						@endphp

						<td>{{ $total_taka  }}</td>
						
						<td>
							<a href="#DeleteModal{{ $card->id}}" data-toggle="modal" class="btn btn-outline-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$card->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('admin-cards-destroy', $card->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="btn btn-outline-primary btn-lg">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-outline-success btn-lg" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								
							</a>
						</td>
					</tr>
					@endforeach
					<tr>
						<th style="margin-left: 120px" colspan="5">Total Amount</th>
						<th colspan="2">{{ $total_taka}}</th>
					</tr>
				</tbody>
			</table>
            </div>
          </div>
          </div>
      </div>
     	<div class="d-sm-flex align-items-center justify-content-between mb-0">

      <form class="form-inline" action="{{ route('order-paid',$bachelor_order->id)}}" method="POST">
        {{csrf_field()}}
        @if($bachelor_order->is_paid)
              <button type="submit" class="btn btn-success btn-sm">Paid Order</button>
        @else
              <button type="submit" class="btn btn-warning btn-sm">Cencel Paid</button>
        @endif
      </form>

      <a href="{{route('bechelors-admin-show',$id)}}" class="btn btn-info btn-sm">View Post</a>
      
      <form class="form-inline" action="{{ route('order-complete',$bachelor_order->id)}}" method="POST">
        {{csrf_field()}}
        @if($bachelor_order->is_complete)
              <button type="submit" class="btn btn-success btn-sm">Complete Order</button>
        @else
              <button type="submit" class="btn btn-warning btn-sm">Cencel Order</button>
        @endif
      </form>
    </div>
      </div>
      </div>
      </div>
        @endsection