@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Thana List</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('thanas.create')}}"><i class="fas fa-plus-circle"></i></a></h1>
              <div class="table-responsive mt-2">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <caption>List of thanas</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Bangla name</th>
  						<th>Latitude</th>
              <th>Longitude</th>
              <th>Parent Thana Name</th>
              <th>Show Details</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($thanas as $thana)
  						<td>{{ $a++ }}</td>
  						<td>{{ $thana->name }}</td>
  						<td>
  						    @if($thana->bangla_name)
                  <p>{{ $thana->bangla_name}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($thana->latitude)
                  <p>{{ $thana->latitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td>
  						    @if($thana->longitude)
                  <p>{{ $thana->longitude}}</p>
                  @else
                    <p>N/A</p>
                  @endif
  						</td>
              <td style="color:green">{{$thana->city->name}}</td>
              <td>
                <a href="{{route('thanas.show', $thana->id)}}" class="badge badge-primary">Show</a>
              </td>
  						<td>
  							<a href="{{route('thanas.edit', $thana->id)}}" class="badge badge-warning">Edit</a>
                <a href="#" onclick="return confirm('are you sure')">
									<form class="pull-right form-inline" action="{{route('thanas.destroy', $thana->id)}}" method="POST">
										{{csrf_field()}}
										{{method_field('DELETE')}}
										<button class="badge badge-danger" type="submit">Delete</button>
									</form>
								</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
