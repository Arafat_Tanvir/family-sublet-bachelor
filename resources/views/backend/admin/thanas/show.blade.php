@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Thana Show</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('thanas.index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-6">
                   <p>Thana name</p>
                   <p>Thana Bangla Name</p>
                   <p>Thana Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-6">
                   <p>City: {{$thanas->city->name}}, Thana: {{$thanas->name}}</p>
                   <p>{{$thanas->bangla_name?$thanas->bangla_name:'N\A'}} </p>
                   <p>{{$thanas->latitude?$thanas->latitude:'N\A'}} </p>
                   <p>{{$thanas->longitude?$thanas->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div id="map">

            </div>
        </div>

      </div>
    </div>
</div>
@endsection
