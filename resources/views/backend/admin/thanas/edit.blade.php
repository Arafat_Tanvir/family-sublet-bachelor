@extends('Backend.admin.layouts.master')
@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Thana Edit</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('thanas.index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
      <form method="POST" action="{{ route('thanas.update',$thanas->id) }}" >
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="latitude">City Name</label>
          <div class="form-input">
            <select name="city_id" class="form-control is-valid form-control-sm input-md" value="{{old('city_id')}}">
              <option value="0" disabled="true" selected="true">===Select Under City===</option>
              @foreach($cities as $city)
              <option value="{{$city->id}}" {{ $city->id == $thanas->city_id ? 'selected' : ''}}>{{$city->name}}</option>
              @endforeach
            </select>
            <div class="valid-feedback">
              {{ ($errors->has('city_id')) ? $errors->first('city_id') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="name">Thana Name</label>
          <div class="form-input">
            <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" value="{{ $thanas->name}}">
            <div class="invalid-feedback">
              {{ ($errors->has('name')) ? $errors->first('name') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="bangla_name">Thana Bangla Name</label>
          <div class="form-input">
            <input type="text" class="form-control is-valid form-control-sm" name="bangla_name" id="bangla_name" value="{{ $thanas->bangla_name? $thanas->bangla_name:'N\A'}}">
            <div class="valid-feedback">
              {{ ($errors->has('bangla_name')) ? $errors->first('bangla_name') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="latitude">Thana Latitude</label>
          <div class="form-input">
            <input type="double" class="form-control is-valid form-control-sm input-md" step="0.01" name="latitude" id="latitude" value="{{ $thanas->latitude? $thanas->latitude:'0.00'}}">
            <div class="valid-feedback">
              {{ ($errors->has('latitude')) ? $errors->first('latitude') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="longitude">Thana Longitude</label>
          <div class="form-input">
            <input type="double" class="form-control is-valid form-control-sm input-md"step="0.01" name="longitude" id="longitude" value="{{ $thanas->longitude? $thanas->longitude:'0.00'}}">
            <div class="valid-feedback">
              {{ ($errors->has('longitude')) ? $errors->first('longitude') : ''}}
            </div>
          </div>
        </div>
        <button class="btn btn-primary" type="submit">Update</button>
      </form>
    </div>
  </div>
@endsection