@extends('backend.admin.layouts.master')
@section('content')
@foreach($sublets as $sublet)
 <div class="container">
   <div class="row">
          <div class="col-sm-12">
            <div class="o-hidden border-0 shadow-lg my-3 card">
            <div class="card-body row">
              <div class="col-sm-6">
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <div class="carousel-inner">
                        @php
                        $i=1
                        @endphp
                        @foreach($sublet->images as $image)
                        <div style="text-align: center;" class="carousel-item {{$i==1 ? 'active' : ''}}">
                          <img class="card-img-top-image" src="{{asset('images/sublet_room/'.$image->images)}}" alt="" width="100%"  height="320px" ">
                        </div>
                        @php
                        $i++
                        @endphp
                        @endforeach
                      </div>
                    </div>
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                      <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                      <span class="carousel-control-next-icon"></span>
                    </a>
                  </div>
              </div>
              <div class="col-sm-6" onclick="location.href='{{route('sublets-admin-show',$sublet->id)}}'">
                  <div class="text-center">
                    <div class="card-header" style="background-color: #36B9CC;color: white">
                       <img src="{{asset('images/categories/'.$sublet->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">
                    
                      <h6 class="mb-0">
                        {{ $sublet->room_status}}
                      </h6>
                      <h6 class="d-none d-sm-inline-block">1st {{$sublet->month.','.$sublet->year}}</h6>
                    

                      <h6 class="d-none d-sm-inline-block">BDT : {{ $sublet->room_rent}}</h6>
                    </div>
                  </div>
                      <div class="col-sm-12 p-1">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>City</th>
                              <th>Thana</th>
                              <th>Ward</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>{{  $sublet->city->name}}</td>
                              <td>{{  $sublet->thana->name}}</td>
                              <td>{{  $sublet->ward->name }}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                     
                        <div class="text-left p-1">
                          Address :  {!! $sublet->address   !!}
                        </div>
                     
                    
                  
                  <div class="d-sm-flex align-items-center justify-content-between mb-0">
                    
                      <form class="form-inline" action="{{ route('admin-sublets-confirmed', $sublet->id)}}" method="POST">
                            {{csrf_field()}}
                            @if($sublet->confirmed)
                                  <button type="submit" class="btn btn-success btn-sm">Confirmed</button>
                            @else
                                  <button type="submit" class="btn btn-warning btn-sm">Panding</button>
                            @endif
                          </form>
                    
                    <a href="{{route('sublets-admin-edit', $sublet->id)}}"><i class="fas fa-edit"></i></i></a>
                  </div>
                 
              </div>
            </div>
          </div>
        </div>
      </div>

  </div>
  @endforeach

  @endsection