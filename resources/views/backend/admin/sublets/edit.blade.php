@extends('backend.admin.layouts.master')
@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">City List</h6>
            </div>
            <div class="card-body">
            	<div class="row">
            		<div class="col-sm-7">
            			{{ Form::open([
                  'route' => ['sublets-admin-update',$sublets->id],
                  'method' => 'POST',
                  'class'=>'form-horizontal form-horizontal row-fluid',
                  'enctype'=>'multipart/form-data'
                  ])
                  }}
              <div class="form-group" style="margin-top: 20px;">
                  <label for="category_id">{{ __('Room Category') }}</label>
                  <div class="form-input">
                      <select name="category_id" type="category_id" id="category_id" class="form-control {{ $errors->has('category_id') ? ' is-valid' : '' }}" value="{{ old('category_id') }}">
	                        <option value="0" disabled="true" selected="true">===Select Category===</option>
	                       @foreach(App\Models\Admin\Category::orderBy('name','desc')->where('parent_id',NULL)->get(); as $parent)
                        <option value="{{$parent->id}}" {{ $parent->id ==$sublets->category_id ? 'selected' : ''}}>{{$parent->name}}</option>
                        @foreach(App\Models\Admin\Category::orderBy('name','desc')->where('parent_id',$parent->id)->get(); as $child)
                            <option value="{{$child->id}}" {{ $child->id ==$sublets->category_id ? 'selected' : ''}}>--->{{$child->name}}</option>
                        @endforeach
                        @endforeach

                      </select>
                      @if ($errors->has('category_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('category_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label for="title">{{ __('Title') }}</label>
                  <div class="form-input">
                      <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-valid' : '' }}" name="title" value="{{ $sublets->title }}">
                      @if ($errors->has('title'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('title') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <label for="month">{{ __('Month') }}</label>
                  <div class="form-input">
                      <select name="month" type="month" id="month" class="form-control{{ $errors->has('month') ? ' is-valid' : '' }}" value="{{ old('month') }}">
                            <option value="0" disabled="true" selected="true">===whem Seat OR Room Available===</option>
                        <option value="January"@if ($sublets->month == "January")selected="selected" @endif >January</option>
                        <option value="February" @if ($sublets->month == "February")selected="selected" @endif >February </option>
                        <option value="March" @if ($sublets->month == "March")selected="selected" @endif > March </option>
                        <option value="April" @if ($sublets->month == "April")selected="selected" @endif > April </option>
                        <option value="May" @if ($sublets->month == "May")selected="selected" @endif > May </option>
                        <option value="June" @if ($sublets->month == "June")selected="selected" @endif >June </option>
                        <option value="July" @if ($sublets->month == "July")selected="selected" @endif >July </option>
                        <option value="August" @if ($sublets->month == "August")selected="selected" @endif >August </option>
                        <option value="September" @if ($sublets->month == "September")selected="selected" @endif >September </option>
                        <option value="October" @if ($sublets->month == "October")selected="selected" @endif >October </option>
                        <option value="November" @if ($sublets->month == "November")selected="selected" @endif >November </option>
                        <option value="December" @if ($sublets->month == "December")selected="selected" @endif >December </option>
                          <option value="No Condition" @if ($sublets->month == "No Condition")selected="selected" @endif >No Condition</option>
                      </select>
                      @if ($errors->has('month'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('month') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <label for="status">{{ __('Status') }}</label>
                  <div class="form-input">
                      <select name="status" type="status" id="status" class="form-control{{ $errors->has('status') ? ' is-valid' : '' }}" value="{{ old('status') }}">
                          <option value="0" disabled="true" selected="true">===which Type Roommate?===</option>
                          <option value="Student" @if ($sublets->status == "Student")selected="selected" @endif >Student</option>
                          <option value="Job-Holder" @if ($sublets->status == "Job-Holder")selected="selected" @endif >Job-Holder</option>
                          <option value="No Conditions" @if ($sublets->status == "No Conditions")selected="selected" @endif >No Conditions</option>
                      </select>
                      @if ($errors->has('status'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('status') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              

              <div class="form-group">
                  <label for="gender">{{ __('Gender Select') }}</label>
                  <div class="form-input">

                      <div class="row">
                          <label class="col-sm-6 col-sm-offset-2">
                              <input type="radio" name="gender" value="Male"@if($sublets->gender=='Male') checked @endif>Male
                          </label>
                          <label class="col-sm-6 col-sm-offset-2">
                               <input type="radio" name="gender" value="Female"@if($sublets->gender=='Female') checked @endif>Female
                          </label>
                      </div>


                      @if ($errors->has('gender'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('gender') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <label for="religion">{{ __('Religion') }}</label>
                  <div class="form-input">
                      <select name="religion" type="religion" id="religion" class="form-control{{ $errors->has('religion') ? ' is-valid' : '' }}" value="{{ old('religion') }}">
                          <option value="0" disabled="true" selected="true">===Choose Religion===</option>
                        <option value="Islam" @if ($sublets->religion == "Islam")selected="selected" @endif >Islam</option>
                        <option value="Hinduism" @if ($sublets->religion == "Hinduism")selected="selected" @endif >Hinduism</option>
                        <option value="Buddhism" @if ($sublets->religion == "Buddhism")selected="selected" @endif >Buddhism</option>
                        <option value="Christianity" @if ($sublets->religion == "Christianity")selected="selected" @endif >Christianity</option>
                      </select>
                      @if ($errors->has('religion'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('religion') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>


              <div class="form-group">
                  <label for="married_status">{{ __('married_status Select') }}</label>
                  <div class="form-input">

                      <div class="row">
                          <label class="col-sm-4 col-sm-offset-2">
                              <input type="radio" name="married_status" value="Married"@if($sublets->married_status=='Married') checked @endif>Married
                          </label>
                          <label class="col-sm-4 col-sm-offset-2">
                               <input type="radio" name="married_status" value="Unmarried"@if($sublets->married_status=='Unmarried') checked @endif>Unmarried
                          </label>
                          <label class="col-sm-4 col-sm-offset-2">
                               <input type="radio" name="married_status" value="No Conditions"@if($sublets->married_status=='No Conditions') checked @endif>No Conditions
                          </label>
                      </div>


                      @if ($errors->has('married_status'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('married_status') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <label for="seat">{{ __('Seat') }}</label>
                  <div class="form-input">
                      <select name="seat" type="seat" id="seat" class="form-control{{ $errors->has('seat') ? ' is-valid' : '' }}" value="{{ old('seat') }}">
                          <option value="0" disabled="true" selected="true">===Family Total Room===</option>
                          <option value="1"@if ($sublets->seat == "1")selected="selected" @endif >1</option>
                          <option value="2"@if ($sublets->seat == "2")selected="selected" @endif >2</option>
                          <option value="3"@if ($sublets->seat == "3")selected="selected" @endif >3</option>
                          <option value="4"@if ($sublets->seat == "4")selected="selected" @endif >4</option>
                          <option value="5"@if ($sublets->seat == "5")selected="selected" @endif>5</option>
                          <option value="6"@if ($sublets->seat == "6")selected="selected" @endif >6</option>
                          <option value="7"@if ($sublets->seat == "7")selected="selected" @endif >7</option>
                          <option value="8"@if ($sublets->seat == "8")selected="selected" @endif >8</option>
                      </select>
                      @if ($errors->has('seat'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('seat') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <label for="room_type">{{ __('Room Type') }}</label>
                  <div class="form-input">
                      <select name="room_type" type="room_type" id="room_type" class="form-control{{ $errors->has('room_type') ? ' is-valid' : '' }}" value="{{ old('room_type') }}">
                          <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                          <option value="Flat" @if ($sublets->room_type == "Flat")selected="selected" @endif >Flat</option>
                          <option value="Semi-ripe house" @if ($sublets->room_type == "Semi-ripe house")selected="selected" @endif >Semi-ripe house</option>
                      </select>
                      @if ($errors->has('room_type'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('room_type') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              
              <div class="form-group">
                  <label for="room_rent">{{ __('Room Rent') }}</label>
                  <div class="form-input">
                      <input id="room_rent" type="number" class="form-control{{ $errors->has('room_rent') ? ' is-valid' : '' }}" name="room_rent" value="{{ $sublets->room_rent }}">
                      @if ($errors->has('room_rent'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('room_rent') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label for="description">{{ __('Short Description') }}</label>
                  <div class="form-input">
                      <textarea id="editor" class="form-control{{ $errors->has('description') ? ' is-valid' : '' }}" name="description" value="{{ old('description')}}" cols="40" rows="5">{!! $sublets->description !!}</textarea>
                      @if ($errors->has('description'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <hr>
              <div class="form-group">
                  <label for="facilities">{{ __('Room facilities') }}</label>
                  <div class="form-input">
                      <label class="checkbox-inline" >
                          <input type="checkbox" name="facilities[]" value="Fully Decorated"@if(in_array('Fully Decorated', $facilities)) checked @endif/>Fully Decorated
                      </label>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="facilities[]" value="Attach Bathroom"@if(in_array('Attach Bathroom', $facilities)) checked @endif />Attach Bathroom
                      </label>
                      <br>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="facilities[]" value="A Balcony along with the room" @if(in_array('A Balcony along with the room', $facilities)) checked @endif/>A Balcony along
                      </label>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="facilities[]" value="24 Hours Water and Gass Supply" @if(in_array('24 Hours Water and Gass Supply', $facilities)) checked @endif/>24 Hours Water  & guss
                      </label>
                      <br>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="facilities[]" value="A quiet and Lovely environment" @if(in_array('A quiet and Lovely environment', $facilities)) checked @endif/>A quiet and Lovely environment
                      </label>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="facilities[]" value="Wifi"@if(in_array('Wifi', $facilities)) checked @endif />Wifi
                      </label>
                      <br>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="facilities[]" value="Security Guard"@if(in_array('Security Guard', $facilities)) checked @endif/>Security Guard</br>
                      </label>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="facilities[]" value="Tiles" @if(in_array('Tiles', $conditions)) checked @endif/>Tiles</br>
                      </label>
                      <br>
                      @if ($errors->has('facilities'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('facilities') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <hr>
              <div class="form-group">
                  <label for="conditions">{{ __('Room Conditions') }}</label>
                  <div class="form-input checkbox-inline">
                      <label class="checkbox-inline">
                          <input type="checkbox" name="conditions[]" value="Must be Nonsmoker" @if(in_array('Must be Nonsmoker', $conditions)) checked @endif/>Must be Nonsmoker
                      </label>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="conditions[]" value="Must be maintain of role of Room" @if(in_array('Must be maintain of role of Room', $conditions)) checked @endif/>Must be maintain of role of Room
                      </label>
                      <br>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="conditions[]" value="Before 11PM come back"@if(in_array('Before 11PM come back', $conditions)) checked @endif />Before 11PM come back
                      </label>
                      <label class="checkbox-inline">
                          <input type="checkbox" name="conditions[]" value="Before 11PM come back" @if(in_array('Before 11PM come back', $conditions)) checked @endif/>Before 11PM come back</br>
                      </label>
                      <br>
                      @if ($errors->has('conditions'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('conditions') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <hr>
              <div class="form-group">
                  <label for="mobile">{{ __('Phone Number') }}</label>
                  <div class="form-input">
                      <div class="row">
                        <div class="col-sm-4">
                          <select name="operator" type="operator" id="operator" class="form-control{{ $errors->has('operator') ? ' is-valid' : '' }}" value="{{ old('operator') }}">
                              <option value="018"@if ($sublets->operator == "018")selected="selected" @endif  >018</option>
                              <option value="017"@if ($sublets->operator == "017")selected="selected" @endif  >017</option>
                              <option value="019"@if ($sublets->operator == "019")selected="selected" @endif  >019</option>
                              <option value="016"@if ($sublets->operator == "016")selected="selected" @endif  >016</option>
                              <option value="015"@if ($sublets->operator == "015")selected="selected" @endif  >+015</option>
                          </select>
                      @if ($errors->has('operator'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('operator') }}</strong>
                      </span>
                      @endif
                        </div>
                        <div class="col-sm-8">
                          <input id="digit" type="number" class="form-control{{ $errors->has('digit') ? ' is-valid' : '' }}" name="digit" value="{{ $sublets->mobile }}">
                        </div>
                      </div>
                      
                      @if ($errors->has('digit'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('digit') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label for="city_id">{{ __(' City Name') }}</label>
                  <div class="form-input">
                      <select name="city_id" type="text" id="city_id" class="form-control{{ $errors->has('city_id') ? ' is-valid' : '' }}" value="{{ old('city_id') }}">
                          <option value="0" disabled="true" selected="true">===Select City===</option>
                            @foreach(App\Models\Admin\City::orderBy('name','desc')->get(); as $city)
		                        <option value="{{$city->id}}" {{ $city->id ==$sublets->city_id ? 'selected' : ''}}>{{$city->name}}</option>
	                        @endforeach
                      </select>
                      @if ($errors->has('city_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('city_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label for="thana_id">{{ __(' Thana Name') }}</label>
                  <div class="form-input">
                      <select name="thana_id" type="text" id="thana_id" class="form-control{{ $errors->has('thana_id') ? ' is-valid' : '' }}" value="{{ old('thana_id') }}">
                          <option value="0" disabled="true" selected="true">===Select Thana===</option>
                          @foreach(App\Models\Admin\Thana::orderBy('name','desc')->get(); as $thana)
		                        <option value="{{$thana->id}}" {{ $thana->id ==$sublets->thana_id ? 'selected' : ''}}>{{$thana->name}}</option>
	                        @endforeach
                      </select>
                      @if ($errors->has('thana_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('thana_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label for="ward_id">{{ __(' Ward Name') }}</label>
                  <div class="form-input">
                      <select name="ward_id" type="text" id="ward_id" class="form-control{{ $errors->has('ward_id') ? ' is-valid' : '' }}" value="{{ old('ward_id') }}">
                          <option value="0" disabled="true" selected="true">===Select Ward===</option>
                          @foreach(App\Models\Admin\Ward::orderBy('name','desc')->get(); as $ward)
		                        <option value="{{$ward->id}}" {{ $ward->id ==$sublets->ward_id ? 'selected' : ''}}>{{$ward->name}}</option>
	                       @endforeach
                      </select>
                      @if ($errors->has('ward_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('ward_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label for="address">{{ __('Address') }}</label>
                  <div class="form-input">
                      <textarea  class="form-control{{ $errors->has('address') ? ' is-valid' : '' }}" name="address" value="{{ old('address') }}" cols="40" rows="5">{!! $sublets->address !!}</textarea>
                      @if ($errors->has('address'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('address') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label for="images">{{ __('images') }}</label>
                  <div class="form-input">

                     <div style="margin-top: 5px">
                          <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                      </div>

                      <div style="margin-top: 5px">
                          <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                      </div>

                      <div style="margin-top: 5px">
                          <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                      </div>

                      <div style="margin-top: 5px">
                          <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                      </div>

                      <div style="margin-top: 5px">
                          <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                      </div>

                      <div style="margin-top: 5px">
                          <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                      </div>

                      @if ($errors->has('images'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('images') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <button class="btn btn-success" type="submit" style="text-align: center;">Create Post For Family</button>
              </div>
              
              {{ Form::close() }}
            		</div>
            		<div class="col-sm-5 p-5">
            			<div class="text-center">
		    				<div id="demo" class="carousel slide" data-ride="carousel">
		                <div class="carousel-inner">
		                  <div class="carousel-inner">
		                    @php
		                    $i=1
		                    @endphp
		                    @foreach($sublets->images as $image)
		                    <div style="text-align: center;" class="carousel-item {{$i==1 ? 'active' : ''}}">
		                      <img class="card-img-top-image" src="{{asset('images/sublet_room/'.$image->images)}}" alt=""  height="560px" ">
		                    </div>
		                    @php
		                    $i++
		                    @endphp
		                    @endforeach
		                  </div>
		                </div>
		                <!-- Left and right controls -->
		                <a class="carousel-control-prev" href="#demo" data-slide="prev">
		                  <span class="carousel-control-prev-icon"></span>
		                </a>
		                <a class="carousel-control-next" href="#demo" data-slide="next">
		                  <span class="carousel-control-next-icon"></span>
		                </a>
		              </div>
		    			</div>
            		</div>
            	</div>
              </div>
            </div>
@endsection

          
@section('scripts')


<script type="text/javascript">
         CKEDITOR.replace( 'address',
         {
          customConfig : 'config.js',
          toolbar : 'simple'
          })
</script> 


<script type="text/javascript">
$(document).ready(function(){
$('select[name="city_id"]').on('change',function(){
var city_id=$(this).val();
//alert(city_id);
if(city_id){
$.ajax({
url:'{{ url('')}}/cities/ajax/'+city_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="thana_id"]').empty();
$.each(data,function(key,value){
$('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="thana_id"]').empty();
}
});
$('select[name="thana_id"]').on('change',function(){
var thana_id=$(this).val();
//alert(thana_id);
console.log(thana_id);
if(thana_id){
$.ajax({
url:'{{ url('')}}/thanas/ajax/'+thana_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="ward_id"]').empty();
$.each(data,function(key,value){
$('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="ward_id"]').empty();
}
});
});
</script>
@endsection
