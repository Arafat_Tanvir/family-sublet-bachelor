@extends('backend.admin.layouts.master')
@section('content')
<div class="container">
    <div class="row">
       <div class="col-sm-12">
            <div class="card o-hidden border-0 shadow-lg my-5 border-left-success">
                <div class="card-header text-center shadow-lg border-bottom-success">
                    Sublet Finder
                </div>
              <div class="card-body border-bottom-success">
                  {{ Form::open([
                    'route' => 'sub-lets-admin-store',
                    'method' => 'POST',
                    'class'=>'form-horizontal form-horizontal row-fluid',
                    'enctype'=>'multipart/form-data'
                    ])
                    }}
                    <div class="form-group" style="margin-top: 20px;">
                        <label for="category_id">{{ __('Room Category') }}</label>
                        <div class="form-input">
                            <select name="category_id" type="category_id" id="category_id" class="form-control {{ $errors->has('category_id') ? ' is-valid' : '' }}" value="{{ old('category_id') }}">
                                <option value="0" disabled="true" selected="true">===Select Category===</option>
                                @foreach(App\Backend\Admin\Category::orderBy('name','desc')->where('parent_id',NULL)->get(); as $parent)
                                <option value="{{$parent->id}}">{{$parent->name}}</option>
                                    @foreach(App\Backend\Admin\Category::orderBy('name','desc')->where('parent_id',$parent->id)->get(); as $child)
                                        <option value="{{$child->id}}">---->{{$child->name}}</option>
                                    @endforeach
                                @endforeach
                            </select>
                            @if ($errors->has('category_id'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('category_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">{{ __('Title') }}</label>
                        <div class="form-input">
                            <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-valid' : '' }}" name="title" value="{{ old('title') }}">
                            @if ($errors->has('title'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="room_status">{{ __('Room Status') }}</label>
                        <div class="form-input">
                            <select name="room_status" type="room_status" id="room_status" class="form-control{{ $errors->has('room_status') ? ' is-valid' : '' }}" value="{{ old('room_status') }}">
                                <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                                <option value="Flat">Flat</option>
                                <option value="Semi-ripe house">Semi-ripe house</option>
                            </select>
                            @if ($errors->has('room_status'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('room_status') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="month">{{ __('Month') }}</label>
                        <div class="form-input">
                            <select name="month" type="month" id="month" class="form-control{{ $errors->has('month') ? ' is-valid' : '' }}" value="{{ old('month') }}">
                                <option value="0" disabled="true" selected="true">===Room Available From===</option>
                                <option value="January">January</option>
                                <option value="February">February </option>
                                <option value="March"> March </option>
                                <option value="April"> April </option>
                                <option value="May"> May </option>
                                <option value="June">June </option>
                                <option value="July">July </option>
                                <option value="August">August </option>
                                <option value="September">September </option>
                                <option value="October">October </option>
                                <option value="November">November </option>
                                <option value="December">December</option>
                                <option value="No Condition">No Condition</option>
                            </select>
                            @if ($errors->has('month'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('month') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="religion">{{ __('Religion') }}</label>
                        <div class="form-input">
                            <select name="religion" type="religion" id="religion" class="form-control{{ $errors->has('religion') ? ' is-valid' : '' }}" value="{{ old('religion') }}">
                                <option value="0" disabled="true" selected="true">===Family Religion===</option>
                                <option value="Islam" >Islam</option>
                                <option value="Hinduism" >Hinduism</option>
                                <option value="Buddhism" >Buddhism</option>
                                <option value="Christianity" >Christianity</option>
                                <option value="No Condition">No Condition</option>
                            </select>
                            @if ($errors->has('religion'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('religion') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="total_room">{{ __('Total Room') }}</label>
                        <div class="form-input">
                            <select name="total_room" type="total_room" id="total_room" class="form-control{{ $errors->has('total_room') ? ' is-valid' : '' }}" value="{{ old('total_room') }}">
                                <option value="0" disabled="true" selected="true">===Family Total Room===</option>
                                <option value="1" >1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5">5</option>
                                <option value="6" >6</option>
                                <option value="7" >7</option>
                                <option value="8" >8</option>
                            </select>
                            @if ($errors->has('total_room'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('total_room') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bed_room">{{ __('How Many Bed Room') }}</label>
                        <div class="form-input">
                            <select name="bed_room" type="bed_room" id="bed_room" class="form-control{{ $errors->has('bed_room') ? ' is-valid' : '' }}" value="{{ old('bed_room') }}">
                                <option value="0" disabled="true" selected="true">===Select Bed Room Available===</option>
                                <option value="1" >1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5">5</option>
                            </select>
                            @if ($errors->has('bed_room'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('bed_room') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bath_room">{{ __('How Many Bath Room') }}</label>
                        <div class="form-input">
                            <select name="bath_room" type="bath_room" id="bath_room" class="form-control{{ $errors->has('bath_room') ? ' is-valid' : '' }}" value="{{ old('bath_room') }}">
                                <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                                <option value="1" >1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5">5</option>
                            </select>
                            @if ($errors->has('bath_room'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('bath_room') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="dinning_room">{{ __('Dinning Room') }}</label>
                        <div class="form-input">

                            <div class="row">
                                <label class="col-sm-2 col-sm-offset-2">
                                    <input type="radio" name="dinning_room" value="Yes">Yes
                                </label>
                                <label class="col-sm-2 col-sm-offset-2">
                                     <input type="radio" name="dinning_room" value="No">No
                                </label>
                            </div>


                            @if ($errors->has('dinning_room'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('dinning_room') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="room_size">{{ __('Room Size') }}</label>
                        <div class="form-input">
                            <input id="room_size" type="number" class="form-control{{ $errors->has('room_size') ? ' is-valid' : '' }}" name="room_size" value="{{ old('room_size') }}">
                            @if ($errors->has('room_size'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('room_size') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="drawing_room">{{ __('Drawing Room') }}</label>
                        <div class="form-input">
                            <div class="row">
                                <label class="col-sm-2 col-sm-offset-2">
                                    <input type="radio" name="drawing_room" value="Yes">Yes
                                </label>
                                <label class="col-sm-2 col-sm-offset-2">
                                    <input type="radio" name="drawing_room" value="No">No
                                </label>
                            </div>
                            
                            @if ($errors->has('drawing_room'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('drawing_room') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="room_rent">{{ __('Room Rent') }}</label>
                        <div class="form-input">
                            <input id="room_rent" type="number" class="form-control{{ $errors->has('room_rent') ? ' is-valid' : '' }}" name="room_rent" value="{{ old('room_rent') }}">
                            @if ($errors->has('room_rent'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('room_rent') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">{{ __('Short Description') }}</label>
                        <div class="form-input">
                            <textarea id="editor" class="form-control{{ $errors->has('description') ? ' is-valid' : '' }}" name="description" value="{{ old('description') }}" cols="40" rows="5">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="facilities">{{ __('Room facilities') }}</label>
                        <div class="form-input">
                            <label class="checkbox-inline" >
                                <input type="checkbox" name="facilities[]" value="Fully Decorated"/>Fully Decorated
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="facilities[]" value="Attach Bathroom" />Attach Bathroom
                            </label>
                            <br>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="facilities[]" value="A Balcony along with the room" />A Balcony along
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="facilities[]" value="24 Hours Water and Gass Supply" />24 Hours Water  & guss
                            </label>
                            <br>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="facilities[]" value="A quiet and Lovely environment" />A quiet and Lovely environment
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="facilities[]" value="Wifi" />Wifi
                            </label>
                            <br>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="facilities[]" value="Security Guard"/>Security Guard</br>
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="facilities[]" value="Tiles" />Tiles</br>
                            </label>
                            <br>
                            @if ($errors->has('facilities'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('facilities') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="conditions">{{ __('Room Conditions') }}</label>
                        <div class="form-input checkbox-inline">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="conditions[]" value="Must be Nonsmoker" />Must be Nonsmoker
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="conditions[]" value="Must be maintain of role of Room" />Must be maintain of role of Room
                            </label>
                            <br>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="conditions[]" value="Before 11PM come back" />Before 11PM come back
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="conditions[]" value="Before 11PM come back" />Before 11PM come back</br>
                            </label>
                            <br>
                            @if ($errors->has('conditions'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('conditions') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="mobile">{{ __('Phone Number') }}</label>
                        <div class="form-input">
                            <div class="row">
                              <div class="col-sm-4">
                                <select name="operator" type="operator" id="operator" class="form-control{{ $errors->has('operator') ? ' is-valid' : '' }}" value="{{ old('operator') }}">
                                    <option value="0" disabled="true" selected="true">===Mobile Operator===</option>
                                    <option value="018" >018</option>
                                    <option value="017" >017</option>
                                    <option value="019" >019</option>
                                    <option value="016" >016</option>
                                    <option value="015" >015</option>
                                </select>
                            @if ($errors->has('operator'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('operator') }}</strong>
                            </span>
                            @endif
                              </div>
                              <div class="col-sm-8">
                                <input id="digit" type="number" class="form-control{{ $errors->has('digit') ? ' is-valid' : '' }}" name="digit" value="{{ old('digit') }}">
                              </div>
                            </div>
                            
                            @if ($errors->has('digit'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('digit') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city_id">{{ __(' City Name') }}</label>
                        <div class="form-input">
                            <select name="city_id" type="text" id="city_id" class="form-control{{ $errors->has('city_id') ? ' is-valid' : '' }}" value="{{ old('city_id') }}">
                                <option value="0" disabled="true" selected="true">===Select City===</option>
                                @foreach($cities as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('city_id'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('city_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="thana_id">{{ __(' Thana Name') }}</label>
                        <div class="form-input">
                            <select name="thana_id" type="text" id="thana_id" class="form-control{{ $errors->has('thana_id') ? ' is-valid' : '' }}" value="{{ old('thana_id') }}">
                                <option value="0" disabled="true" selected="true">===Select Thana===</option>
                            </select>
                            @if ($errors->has('thana_id'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('thana_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ward_id">{{ __(' Ward Name') }}</label>
                        <div class="form-input">
                            <select name="ward_id" type="text" id="ward_id" class="form-control{{ $errors->has('ward_id') ? ' is-valid' : '' }}" value="{{ old('ward_id') }}">
                                <option value="0" disabled="true" selected="true">===Select Ward===</option>
                            </select>
                            @if ($errors->has('ward_id'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('ward_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">{{ __('Address') }}</label>
                        <div class="form-input">
                            <textarea class="form-control{{ $errors->has('address') ? ' is-valid' : '' }}" name="address" value="{{ old('address') }}" cols="40" rows="5">{{ old('address') }}</textarea>
                            @if ($errors->has('address'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="images">{{ __('images') }}</label>
                        <div class="form-input">

                           <div style="margin-top: 5px">
                                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                            </div>

                            <div style="margin-top: 5px">
                                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                            </div>

                            <div style="margin-top: 5px">
                                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                            </div>

                            <div style="margin-top: 5px">
                                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                            </div>

                            <div style="margin-top: 5px">
                                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                            </div>

                            <div style="margin-top: 5px">
                                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                            </div>

                            @if ($errors->has('images'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('images') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success" type="submit" style="text-align: center;">Create Post For Family</button>
                    </div>
                    
                    {{ Form::close() }}
                </div>

                <div class="card-header text-center shadow-lg">
                    Sublet Finder
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

          
@section('scripts')


<script type="text/javascript">
         CKEDITOR.replace( 'address',
         {
          customConfig : 'config.js',
          toolbar : 'simple'
          })
</script> 


<script type="text/javascript">
$(document).ready(function(){
$('select[name="city_id"]').on('change',function(){
var city_id=$(this).val();
//alert(city_id);
if(city_id){
$.ajax({
url:'{{ url('')}}/cities/ajax/'+city_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="thana_id"]').empty();
$.each(data,function(key,value){
$('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="thana_id"]').empty();
}
});
$('select[name="thana_id"]').on('change',function(){
var thana_id=$(this).val();
//alert(thana_id);
console.log(thana_id);
if(thana_id){
$.ajax({
url:'{{ url('')}}/thanas/ajax/'+thana_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="ward_id"]').empty();
$.each(data,function(key,value){
$('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="ward_id"]').empty();
}
});
});
</script>
@endsection
