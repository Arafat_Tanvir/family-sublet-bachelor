@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Ward Edit</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('wards.index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="table-responsive mt-2">
                   <form method="POST" action="{{ route('wards.update',$wards->id) }}" >
                       @csrf
                       @method('PUT')

                       <div class="form-group">
                           <label for="latitude">City Name</label>
                           <div class="form-input">
                               <select name="thana_id" class="form-control is-valid form-control-sm input-md" value="{{old('thana_id')}}">
                                       <option value="0" disabled="true" selected="true">===Select Under City===</option>
                                       @foreach($thanas as $thana)
                                       <option value="{{$thana->id}}" {{ $thana->id == $wards->thana_id ? 'selected' : ''}}>{{$thana->name}}</option>
                                       @endforeach
                               </select>
                               <div class="valid-feedback">
                                 {{ ($errors->has('thana_id')) ? $errors->first('thana_id') : ''}}
                               </div>
                           </div>
                       </div>


                        <div class="form-group">
                            <label for="name">Thana Name</label>
                            <div class="form-input">
                                <input type="text" class="form-control is-invalid form-control-sm" name="name" id="name" value="{{ $wards->name}}">
                                <div class="invalid-feedback">
                                  {{ ($errors->has('name')) ? $errors->first('name') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bangla_name">Thana Bangla Name</label>
                            <div class="form-input">
                                <input type="text" class="form-control is-valid form-control-sm" name="bangla_name" id="bangla_name" value="{{ $wards->bangla_name? $wards->bangla_name:'N\A'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('bangla_name')) ? $errors->first('bangla_name') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="latitude">Thana Latitude</label>
                            <div class="form-input">
                                <input type="double" class="form-control is-valid form-control-sm input-md" step="0.01" name="latitude" id="latitude" value="{{ $wards->latitude? $wards->latitude:'0.00'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('latitude')) ? $errors->first('latitude') : ''}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="longitude">Thana Longitude</label>
                            <div class="form-input">
                                <input type="double" class="form-control is-valid form-control-sm input-md"step="0.01" name="longitude" id="longitude" value="{{ $wards->longitude? $wards->longitude:'0.00'}}">
                                <div class="valid-feedback">
                                  {{ ($errors->has('longitude')) ? $errors->first('longitude') : ''}}
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                  </div>
                </div>
              
@endsection
