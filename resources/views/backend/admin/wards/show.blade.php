@extends('Backend.admin.layouts.master')

@section('content')
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Ward Edit</h1>
            </div>
            <div class="card-body">
              <h1 class="text-right"><a href="{{route('wards.index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-3">
                   <p>Name</p>
                   <p>Bangla Name</p>
                   <p>Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-9">
                   <p>City: {{$wards->thana->city->name}}, Thana: {{$wards->thana->name}}, Ward: {{$wards->name}} </p>
                   <p>{{$wards->bangla_name?$wards->bangla_name:'N\A'}} </p>
                   <p>{{$wards->latitude?$wards->latitude:'N\A'}} </p>
                   <p>{{$wards->longitude?$wards->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div id="map">

          </div>
        </div>

      </div>
    </div>
  </div>

@endsection
