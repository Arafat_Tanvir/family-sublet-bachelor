@extends('backend.user.layouts.master')

@section('content')
<section class="content">
    <div class="row">
		<div class="col-sm-12">
			<div class="box">
                <div class="box-body">
					<div class="card shadow mb-4">
					    <div class="card-body">
			    			<div class="text-center" style="background-color: #36B9CC;color: white;">
						          <div class="box-body">
						            <img src="{{asset('images/categories/'.$bechelors->category->images)}}" class="rounded-circle" height="100px" width="100px" alt="">
						          <h3>{{ $bechelors->room_status}}, 1st {{ $bechelors->month}},{{$bechelors->year}},{{$bechelors->seat}} Seat Available</h3>

						            <h2 class="alert alert-pill" ">BDT : {{ $bechelors->room_rent}}</h2>
						          </div>
						      </div>
						            <div class="box box-solid">
								            <div class="box-body">
								              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
								                <ol class="carousel-indicators">
								                 
								                  @foreach($bechelors->images as $image)
								                  <li data-target="#carousel-example-generic" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
								                  @endforeach
								                </ol>
								                <div class="carousel-inner">
								                  @php
								                  $i=1
								                  @endphp

								                  @foreach($bechelors->images as $image)
								                  <div class="item {{$i==1 ? 'active' : ''}}">
								                    <img  src="{{asset('images/bechelor_room/'.$image->images)}}" style="max-height: 350px;" width="100%">
								                   
								                  </div>
								                  @php

								                  $i++
								                  @endphp
								                  @endforeach
								                  
								                </div>
								                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
								                  <span class="fa fa-angle-left"></span>
								                </a>
								                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
								                  <span class="fa fa-angle-right"></span>
								                </a>
								              </div>
								            </div>
								          </div>           
					    			<div class="text-center">
					    				<div class="box box-solid">
								            <div class="box-header with-border">
								              <h3 class="box-title">Bachelor Room Info</h3>
								            </div>
								            <!-- /.box-header -->
								            <div class="box-body">
								              <div class="box-group" id="accordion">
								                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
								                <div class="panel box box-primary">
								                  <div class="box-header with-border">
								                    <h4 class="box-title">
								                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								                        Necessary Information
								                      </a>
								                    </h4>
								                  </div>
								                  <div id="collapseOne" class="panel-collapse collapse in">
								                    <div class="box-body">

								                    	<div class="row">
								                    		<div class="col-sm-6">
								                    			<div class="table-responsive">
												                    <table class="table table-bordered text-left">
												                     	<tr>
												                     	 	<th>Category</th>
												                     	 	<td>{{ $bechelors->category->name}}</td>
												                     	</tr>
												                     	<tr>
												                     	 	<th>Roommate Type</th>
												                     	 	<td>{{ $bechelors->roommate_type}}</td>
												                     	</tr>
												                     	<tr>
												                     	 	<th>Roomate Gender</th>
												                     	 	<td>{{ $bechelors->roommate_gender}}</td>
												                     	</tr>
														                <tr>
												                     	 	<th>Roommate Religion</th>
												                     	 	<td>{{ $bechelors->roommate_religion}}</td>
												                     	</tr>
												                     	
												                    </table>
												                </div>
								                    		</div>
								                    		<div class="col-sm-6">
								                    			
								                    		
										                       <div class="text-left py-1">
												    				<h4>Facilities</h4>
												    				<ul style="list-style-type:square;">
												    				    <li>{{ $bechelors->facilities}}</li>
												    				</ul>
												    			</div>
												    			<div class="text-left py-1">
												    			    <h4>Conditions</h4>
												    				<ul style="list-style-type:square;">
												    				    <li>{{ $bechelors->conditions}}</li>
												    				</ul>
												    			</div>

												              </div>
								                    	</div>
								                    </div>
								                  </div>
								                </div>
								                <div class="panel box box-danger">
								                  <div class="box-header with-border">
								                    <h4 class="box-title">
								                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
								                        Description
								                      </a>
								                    </h4>
								                  </div>
								                  <div id="collapseTwo" class="panel-collapse collapse">
								                    <div class="box-body">
								                      <div class="text-left py-1">
										    			    
										    			    <ul>
										    			        <span>{!! $bechelors->short_description !!}</span>
										    			    </ul>
												    	</div>
								                    </div>
								                  </div>
								                </div>
								                <div class="panel box box-success">
								                  <div class="box-header with-border">
								                    <h4 class="box-title">
								                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
								                        Address & location & Map
								                      </a>
								                    </h4>
								                  </div>
								                  <div id="collapseThree" class="panel-collapse collapse">
								                    <div class="box-body">
								                    	<div class="row">
								                    		<div class="col-sm-6">
								                    			<div class="text-left py-1">
												    			    <ul>
												    			        <span>{!! $bechelors->address !!}</span>
												    			    </ul>
												    			</div>
								                    		</div>
								                    		<div class="col-sm-6">
								                    			<div class="table-responsive">
												                    <table class="table table-bordered text-left">
												                     	<tr>
												                     	 	<th>City</th>
												                     	 	<td>{{ $bechelors->city->name}}</td>
												                     	</tr>
												                     	<tr>
												                     	 	<th>Thana</th>
												                     	 	<td>{{ $bechelors->thana->name}}</td>
												                     	</tr>
												                     	<tr>
												                     	 	<th>Ward</th>
												                     	 	<td>{{ $bechelors->Ward->name}}</td>
												                     	</tr>
												                    </table>
												                </div>
								                    		</div>
								                    	</div>
								                    	
										               
								                        <div style="height: 300px;">
														    {!! Mapper::render() !!}
														</div>
														
								                    </div>
								                  </div>
								                </div>
								              </div>
								            </div>
								            <!-- /.box-body -->
								          </div>






















							               
					    			    </div>
					                
									
							    	 
						        <div class="box box-success">
						            <div class="box-header">
						              <i class="fa fa-comments-o"></i>
						              <h3 class="box-title">Chat</h3>
						              <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
						                {{ str_plural('comment', $bechelors->comments->count()) }} {{$bechelors->comments->count()}}
						              </div>
						            </div>
						            <div class="box-body chat" id="chat-box">
						            	 @foreach(App\Models\Admin\Comment::orderBy('id','asc')->where('bachelor_id',$bechelors->id)->where('parent_id',NULL)->get(); as $parent)
						              
						              <div class="item">
						                <img src="{{asset('images/users/'.$parent->user->images)}}" alt="user image" class="online">

						                <p class="message">
						                  <a href="#" class="name">
						                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
						                    {{$parent->user->name}}
						                  </a>
						                  {{$parent->description}}
						                   <span href="#comment-{{$parent->id}}" data-toggle="collapse" class="btn btn-primary btn-sm btn-flat pull-right">Reply</span>
						                </p>

						                
						                <div class="attachment">
						                	<div class="child-rows collapse" id="comment-{{$parent->id}}">
							                	@foreach(App\Models\Admin\Comment::orderBy('id','asc')->where('parent_id',$parent->id)->get(); as $child)
							                   <div class="item">
							                      <img src="{{asset('images/users/'.$child->user->images)}}" alt="user image" class="online">
							                      <p class="message">
									                  <a href="#" class="name">
									                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
									                    {{$child->user->name}}
									                  </a>
									                  {{$child->description}}
									                </p>
							                  </div>

							                  @endforeach
							                  <div class="child-rows">
								                {{ Form::open([
										                'route' => 'user-comment-store',
										                'method' => 'POST',
										                'class'=>'form-horizontal form-horizontal row-fluid',
										                'enctype'=>'multipart/form-data'
										            ])
								                }}
							                    <input type="hidden" name="parent_id" value="{{ $parent->id }}" />
								                <input type="hidden" name="bachelor_id" value="{{ $bechelors->id }}" />
									              <div class="input-group">
									               <input type="text" class="form-control" name="description" value="{{ old('description') }}" required>
									                @if ($errors->has('description'))
								                        <span class="btn-danger" role="alert">
								                            <strong>{{ $errors->first('description') }}</strong>
								                        </span>
								                    @endif

									                <div class="input-group-btn">
									                  <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
									                </div>
									              </div>
											    {{ Form::close() }}
								            </div>
							                </div>
							            </div>
						               
						              </div>
						              @endforeach
						           
						            </div>

						        {{ Form::open([
					                    'route' => 'user-comment-store',
					                    'method' => 'POST',
					                    'class'=>'form-horizontal form-horizontal row-fluid',
					                    'enctype'=>'multipart/form-data'
					                 ])
			                    }}
				                <input type="hidden" name="bachelor_id" value="{{ $bechelors->id }}" />
				            	 <div class="box-footer">
						              <div class="input-group">
				            			 <input type="text" class="form-control" name="description" value="{{ old('description') }}" required>
				                            @if ($errors->has('description'))
				                        <span class="btn-danger" role="alert">
				                            <strong>{{ $errors->first('description') }}</strong>
				                        </span>
				                            @endif
				                            <div class="input-group-btn">
						                  <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
						                </div>
				            		</div>
				            	</div>
			                    {{ Form::close() }}
						            <!-- /.chat -->
						           
						                
						              </div>
						            
						          </div>
					    	</div>
					    </div>
					</div>
				
	</div>


		
		
		</div>
</section>
@endsection
