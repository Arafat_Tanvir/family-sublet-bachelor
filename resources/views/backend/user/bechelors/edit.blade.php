@extends('backend.user.layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-sm-8">
            <div class="box">
                <div class="box-body">
                   
              <div>
            			{{ Form::open([
                  'route' => ['bechelors-user-update',$bechelors->id],
                  'method' => 'POST',
                  'class'=>'form-horizontal form-horizontal row-fluid',
                  'enctype'=>'multipart/form-data'
                  ])
                  }}

                  <h3 style="margin-top: 30px;">Wanted Roommate Information</h3>
                    <hr>

                    <div class="form-group">
                  <label class="col-sm-3 control-form" for="roommate_type">{{ __('roommate_type') }}</label>
                  <div class="col-sm-8">
                      <select name="roommate_type" type="roommate_type" id="roommate_type" class="form-control{{ $errors->has('roommate_type') ? ' is-valid' : '' }}" value="{{ old('roommate_type') }}">
                          <option value="0" disabled="true" selected="true">===Choose roommate_type===</option>
                        <option value="Student" @if ($bechelors->roommate_type == "Student")selected="selected" @endif >Student</option>
                        <option value="Job-Holder" @if ($bechelors->roommate_type == "Job-Holder")selected="selected" @endif >Job-Holder</option>
                        <option value="Anyone" @if ($bechelors->roommate_type == "Anyone")selected="selected" @endif >Anyone</option>
                       
                      </select>
                      @if ($errors->has('roommate_type'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('roommate_type') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>


                    <div class="form-group">
                  <label class="col-sm-3 control-form" for="roommate_religion">{{ __('Roommate Religion') }}</label>
                  <div class="col-sm-8">
                      <select name="roommate_religion" type="roommate_religion" id="roommate_religion" class="form-control{{ $errors->has('roommate_religion') ? ' is-valid' : '' }}" value="{{ old('roommate_religion') }}">
                          <option value="0" disabled="true" selected="true">===Choose Religion===</option>
                        <option value="Islam" @if ($bechelors->roommate_religion == "Islam")selected="selected" @endif >Islam</option>
                        <option value="Hinduism" @if ($bechelors->roommate_religion == "Hinduism")selected="selected" @endif >Hinduism</option>
                        <option value="Buddhism" @if ($bechelors->roommate_religion == "Buddhism")selected="selected" @endif >Buddhism</option>
                        <option value="Christianity" @if ($bechelors->roommate_religion == "Christianity")selected="selected" @endif >Christianity</option>
                      </select>
                      @if ($errors->has('roommate_religion'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('roommate_religion') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

                  

                  <div class="form-group">
                        <label class="col-sm-3 control-form" for="room">Roommate Gender</label>
                        <div class="col-sm-8">
                          <div class="row text-center" style="margin-top: 5px;">
                              <label class="col-sm-6">
                                  <input type="radio" class="flat-red" name="roommate_gender" value="Male" @if($bechelors->roommate_gender=='Male') checked @endif>Male
                              </label>
                              <label class="col-sm-6">
                                   <input type="radio" class="flat-red" name="roommate_gender" value="Female" @if($bechelors->roommate_gender=='Female') checked @endif>Female
                              </label>

                              @if ($errors->has('roommate_gender'))
                              <span class="btn-danger" role="alert">
                                  <strong>{{ $errors->first('roommate_gender') }}</strong>
                              </span>
                              @endif
                          </div>
                      </div>
                  </div>

                  <h3 style="margin-top: 30px;">Basic Information</h3>
                    <hr>


              <div class="form-group" style="margin-top: 20px;">
                  <label class="col-sm-3 control-form" for="category_id">{{ __('Room Category') }}</label>
                  <div class="col-sm-8">
                      <select name="category_id" type="category_id" id="category_id" class="form-control {{ $errors->has('category_id') ? ' is-valid' : '' }}" value="{{ old('category_id') }}">
	                        <option value="0" disabled="true" selected="true">===Select Category===</option>
	                       @foreach(App\Models\Admin\Category::orderBy('name','desc')->where('name','Bachelor')->get(); as $parent)
                        <option class="btn btn-success" value="{{$parent->id}}"  selected="true"  {{ $parent->id ==$bechelors->category_id ? 'selected' : ''}}>{{$parent->name}}</option>
                        @foreach(App\Models\Admin\Category::orderBy('name','desc')->where('parent_id',$parent->id)->get(); as $child)
                            <option value="{{$child->id}}" {{ $child->id ==$bechelors->category_id ? 'selected' : ''}}>==>>{{$child->name}}</option>
                        @endforeach
                        @endforeach

                      </select>
                      @if ($errors->has('category_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('category_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

               <div class="form-group">
                        <label class="col-sm-3 control-form" for="room">Select Month and Year</label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-6">
                                    <select name="month" type="month" id="month" class="form-control{{ $errors->has('month') ? ' is-valid' : '' }}" value="{{ old('month') }}">
                                        <option value="0" disabled="true" selected="true">===Select Month===</option>
                                        <option value="January"@if ($bechelors->month == "January")selected="selected" @endif >January</option>
                                        <option value="February" @if ($bechelors->month == "February")selected="selected" @endif >February </option>
                                        <option value="March" @if ($bechelors->month == "March")selected="selected" @endif > March </option>
                                        <option value="April" @if ($bechelors->month == "April")selected="selected" @endif > April </option>
                                        <option value="May" @if ($bechelors->month == "May")selected="selected" @endif > May </option>
                                        <option value="June" @if ($bechelors->month == "June")selected="selected" @endif >June </option>
                                        <option value="July" @if ($bechelors->month == "July")selected="selected" @endif >July </option>
                                        <option value="August" @if ($bechelors->month == "August")selected="selected" @endif >August </option>
                                        <option value="September" @if ($bechelors->month == "September")selected="selected" @endif >September </option>
                                        <option value="October" @if ($bechelors->month == "October")selected="selected" @endif >October </option>
                                        <option value="November" @if ($bechelors->month == "November")selected="selected" @endif >November </option>
                                        <option value="December" @if ($bechelors->month == "December")selected="selected" @endif >December </option>
                                          <option value="No Condition" @if ($bechelors->month == "No Condition")selected="selected" @endif >No Condition</option>
                                    </select>
                                    @if ($errors->has('month'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('month') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6">
                                    <div class="row text-left">
                                        <label class="col-sm-6">
                                            <input type="radio" class="flat-red" name="year" value="2019" @if($bechelors->year=='2019') checked @endif>2019
                                        </label>
                                        <label class="col-sm-6">
                                             <input type="radio" class="flat-red" name="year" value="2020" @if($bechelors->year=='2020') checked @endif>2020
                                        </label>

                                        @if ($errors->has('year'))
                                        <span class="btn-danger" role="alert">
                                            <strong>{{ $errors->first('year') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              <div class="form-group">
                            <label class="col-sm-3 control-form" for="room">Select Seat And Room</label>
                            <div class="col-sm-4">
                                    <select name="seat" type="seat" id="seat" class="form-control{{ $errors->has('seat') ? ' is-valid' : '' }}" value="{{ old('seat') }}">
                                        <option value="0" disabled="true" selected="true">===Choose Seat===</option>
                                        <option value="1"@if ($bechelors->seat == "1")selected="selected" @endif >1</option>
                                        <option value="2"@if ($bechelors->seat == "2")selected="selected" @endif >2</option>
                                        <option value="3"@if ($bechelors->seat == "3")selected="selected" @endif >3</option>
                                        <option value="4"@if ($bechelors->seat == "4")selected="selected" @endif >4</option>
                                        <option value="5"@if ($bechelors->seat == "5")selected="selected" @endif>5</option>
                                        <option value="6"@if ($bechelors->seat == "6")selected="selected" @endif >6</option>
                                        <option value="7"@if ($bechelors->seat == "7")selected="selected" @endif >7</option>
                                        <option value="8"@if ($bechelors->seat == "8")selected="selected" @endif >8</option>
                                    </select>
                                    @if ($errors->has('seat'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('seat') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                      

              <div class="form-group">
                  <label class="col-sm-3 control-form" for="room_status">{{ __('Room Type') }}</label>
                  <div class="col-sm-8">
                      <select name="room_status" type="room_status" id="room_status" class="form-control{{ $errors->has('room_status') ? ' is-valid' : '' }}" value="{{ old('room_status') }}">
                          <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                          <option value="Flat" @if ($bechelors->room_status == "Flat")selected="selected" @endif >Flat</option>
                          <option value="Semi-ripe house" @if ($bechelors->room_status == "Semi-ripe house")selected="selected" @endif >Semi-ripe house</option>
                      </select>
                      @if ($errors->has('room_status'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('room_status') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              
              <div class="form-group">
                  <label class="col-sm-3 control-form" for="room_rent">{{ __('Room Rent') }}</label>
                  <div class="col-sm-8">
                      <input id="room_rent" type="number" class="form-control{{ $errors->has('room_rent') ? ' is-valid' : '' }}" name="room_rent" value="{{ $bechelors->room_rent }}">
                      @if ($errors->has('room_rent'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('room_rent') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-form" for="short_description">{{ __('ShortDescription') }}</label>
                  <div class="col-sm-8">
                      <textarea id="editor" class="form-control{{ $errors->has('short_description') ? ' is-valid' : '' }}" name="short_description" value="{{ old('short_description')}}" cols="40" rows="5">{!! $bechelors->short_description !!}</textarea>
                      @if ($errors->has('short_description'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('short_description') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              
              <div class="form-group">
                  <label class="col-sm-3 control-form" for="facilities">{{ __('Room facilities') }}</label>
                  <div class="col-sm-8">




                    <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Fully Decorated"@if(in_array('Fully Decorated', $facilities)) checked @endif/>Fully Decorated
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Attach Bathroom"@if(in_array('Attach Bathroom', $facilities)) checked @endif />Attach Bathroom
                                    
                                         </td>
                                    </tr>
                                    <tr>
                                        
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="A Balcony along with the room" @if(in_array('A Balcony along with the room', $facilities)) checked @endif/>A Balcony along
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="24 Hours Water and Gass Supply" @if(in_array('24 Hours Water and Gass Supply', $facilities)) checked @endif/>24 Hours Water  & guss
                                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
            
                                               <input type="checkbox" class="flat-red" name="facilities[]" value="Security Guard"@if(in_array('Security Guard', $facilities)) checked @endif/>Security Guard</br>
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Tiles" @if(in_array('Tiles', $conditions)) checked @endif/>Tiles</br>
                                    
                                        </td>
                                       
                                    </tr>
                                    <tr>
                                        <td>
            
                                               <input type="checkbox" class="flat-red" name="facilities[]" value="Wifi"@if(in_array('Wifi', $facilities)) checked @endif />Wifi
                                    
                                        </td>
                                        <td>
            
                                                 <input type="checkbox" class="flat-red" name="facilities[]" value="A quiet and Lovely environment" @if(in_array('A quiet and Lovely environment', $facilities)) checked @endif/>A quiet and Lovely environment
                                    
                                        </td>
                                       
                                    </tr>
                                </thead>
                            </table>
                      @if ($errors->has('facilities'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('facilities') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              
              <div class="form-group">
                  <label class="col-sm-3 control-form" for="conditions">{{ __('Room Conditions') }}</label>
                  <div class="col-sm-8">
                    <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>
            
                                                 <input type="checkbox" class="flat-red" name="conditions[]" value="Before 11PM come back" @if(in_array('Before 11PM come back', $conditions)) checked @endif/>Before 11PM come back
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="conditions[]" value="Before 11PM come back"@if(in_array('Before 11PM come back', $conditions)) checked @endif />Before 11PM come back
                                    
                                         </td>
                                    </tr>
                                    <tr>
                                        <td>
            
                                                 <input type="checkbox" class="flat-red" name="conditions[]" value="Must be maintain of role of Room" @if(in_array('Must be maintain of role of Room', $conditions)) checked @endif/>Must be maintain of role of Room
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="conditions[]" value="Must be Nonsmoker" @if(in_array('Must be Nonsmoker', $conditions)) checked @endif/>Must be Nonsmoker
                                    
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                      
                     
                      @if ($errors->has('conditions'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('conditions') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              
              <div class="form-group">
                  <label class="col-sm-3 control-form" for="phone">{{ __('Phone Number') }}</label>
                  <div class="col-sm-8">
                          <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-valid' : '' }}" name="phone" value="{{ $bechelors->phone }}">

                      @if ($errors->has('phone'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('phone') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <label class="col-sm-3 control-form" for="short_description">{{ __('Short Description') }}</label>
                  <div class="col-sm-8">
                      <textarea id="editor" class="form-control{{ $errors->has('short_description') ? ' is-valid' : '' }}" name="short_description" value="{{ old('short_description')}}" cols="40" rows="5">{!! $bechelors->short_description !!}</textarea>
                      @if ($errors->has('short_description'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('short_description') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <h3 style="margin-top: 30px;">Location and Address </h3>


              <div class="form-group" style="margin-top: 30px;">
                  <label class="col-sm-3 control-form" for="city_id">{{ __(' City Name') }}</label>
                  <div class="col-sm-8">
                      <select name="city_id" type="text" id="city_id" class="form-control{{ $errors->has('city_id') ? ' is-valid' : '' }}" value="{{ old('city_id') }}">
                          <option value="0" disabled="true" selected="true">===Select City===</option>
                            @foreach(App\Models\Admin\City::orderBy('name','desc')->get(); as $city)
		                        <option value="{{$city->id}}" {{ $city->id ==$bechelors->city_id ? 'selected' : ''}}>{{$city->name}}</option>
	                        @endforeach
                      </select>
                      @if ($errors->has('city_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('city_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-form" for="thana_id">{{ __(' Thana Name') }}</label>
                  <div class="col-sm-8">
                      <select name="thana_id" type="text" id="thana_id" class="form-control{{ $errors->has('thana_id') ? ' is-valid' : '' }}" value="{{ old('thana_id') }}">
                          <option value="0" disabled="true" selected="true">===Select Thana===</option>
                          @foreach(App\Models\Admin\Thana::orderBy('name','desc')->get(); as $thana)
		                        <option value="{{$thana->id}}" {{ $thana->id ==$bechelors->thana_id ? 'selected' : ''}}>{{$thana->name}}</option>
	                        @endforeach
                      </select>
                      @if ($errors->has('thana_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('thana_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-form" for="ward_id">{{ __(' Ward Name') }}</label>
                  <div class="col-sm-8">
                      <select name="ward_id" type="text" id="ward_id" class="form-control{{ $errors->has('ward_id') ? ' is-valid' : '' }}" value="{{ old('ward_id') }}">
                          <option value="0" disabled="true" selected="true">===Select Ward===</option>
                          @foreach(App\Models\Admin\Ward::orderBy('name','desc')->get(); as $ward)
		                        <option value="{{$ward->id}}" {{ $ward->id ==$bechelors->ward_id ? 'selected' : ''}}>{{$ward->name}}</option>
	                       @endforeach
                      </select>
                      @if ($errors->has('ward_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('ward_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-form" for="address">{{ __('Address') }}</label>
                  <div class="col-sm-8">
                      <textarea  class="form-control{{ $errors->has('address') ? ' is-valid' : '' }}" name="address" value="{{ old('address') }}" cols="40" rows="5">{!! $bechelors->address !!}</textarea>
                      @if ($errors->has('address'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('address') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

               <div class="box-footer">
                    <button class="btn btn-success btn-flat pull-right" type="submit">Update</button>
                </div>
              
              {{ Form::close() }}
              </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
      <div class="box box-solid">
          <div class="box-body">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               
                @foreach($bechelors->images as $image)
                <li data-target="#carousel-example-generic" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                @endforeach
              </ol>
              <div class="carousel-inner">
                @php
                $i=1
                @endphp

                @foreach($bechelors->images as $image)
                <div class="item {{$i==1 ? 'active' : ''}}">
                  <img  src="{{asset('images/bechelor_room/'.$image->images)}}" style="height: 420px;max-width: 100%;">
                  <div class="carousel-caption">
                      <h3>Easy Finder</h3>
                      <p>Bachelor Roommate Wanted!</p>
                      
                  </div>
                </div>
                @php

                $i++
                @endphp
                @endforeach
                
              </div>
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="fa fa-angle-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="fa fa-angle-right"></span>
              </a>
            </div>
          </div>
        </div> 
    </div>
</div>
</section>

@endsection

          
@section('scripts')

<script>
  $(function () {
    

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
  })
</script>


<script type="text/javascript">
         CKEDITOR.replace( 'address',
         {
          customConfig : 'config.js',
          toolbar : 'simple'
          })
</script> 


<script type="text/javascript">
$(document).ready(function(){
$('select[name="city_id"]').on('change',function(){
var city_id=$(this).val();
//alert(city_id);
if(city_id){
$.ajax({
url:'{{ url('')}}/cities/ajax/'+city_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="thana_id"]').empty();
$.each(data,function(key,value){
$('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="thana_id"]').empty();
}
});
$('select[name="thana_id"]').on('change',function(){
var thana_id=$(this).val();
//alert(thana_id);
console.log(thana_id);
if(thana_id){
$.ajax({
url:'{{ url('')}}/thanas/ajax/'+thana_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="ward_id"]').empty();
$.each(data,function(key,value){
$('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="ward_id"]').empty();
}
});
});
</script>
@endsection
