@extends('backend.user.layouts.master')

@section('title')
    Bachelor-Advertisement
@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                
                <div class="box-body">
                  {{ Form::open([
                    'route' => 'bechelors-user-store',
                    'method' => 'POST',
                    'class'=>'form-horizontal',
                    'enctype'=>'multipart/form-data'
                    ])
                    }}

                     
                    <h3 style="margin-top: 30px;">Wanted Roommate Information</h3>
                    <hr>

                    <div class="form-group">
                            <label for="roommate_type" class="col-sm-3 control-label">Roommate Type</label>
                            <div class="col-sm-6">
                                <select name="roommate_type" class="form-control" id="roommate_type">
                                    <option value="0" disabled="true" selected="true">===Select Roommate Type===</option>
                                        <option value="Student">Student</option>
                                        <option value="Job-Holder">Job-Holder </option>
                                        <option value="Anyone">Anyone </option>
                                </select>
                                @if ($errors->has('roommate_type'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('roommate_type') }}</strong>
                                    </span>
                                    @endif
                            </div>
                        </div>

                    <div class="form-group">
                            <label for="roommate_religion" class="col-sm-3 control-label">Roommate Religion</label>
                            <div class="col-sm-6">
                                <select name="roommate_religion" class="form-control" id="roommate_religion">
                                    <option value="0" disabled selected>===Select roommate_religion===</option>
                                    <option value="Islam" >Islam</option>
                                    <option value="Hinduism" >Hinduism</option>
                                    <option value="Buddhism" >Buddhism</option>
                                    <option value="Christianity" >Christianity</option>
                                    <option value="No Condition">No Condition</option>
                                </select>
                                @if ($errors->has('roommate_religion'))
                                <span class="btn-danger" role="alert">
                                    <strong>{{ $errors->first('roommate_religion') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="roommate_gender" class="col-sm-3 control-label">Roomate Gender</label>
                             <div class="col-sm-6">
                                    <div class="row " style="margin-top: 5px;">
                                        <label class="col-sm-6 text-center">
                                            <input type="radio" class="flat-red" name="roommate_gender" value="Male">  Male
                                        </label>
                                        <label class="col-sm-6 text-center">
                                             <input type="radio" class="flat-red" name="roommate_gender" value="Female">  Female
                                        </label>



                                        @if ($errors->has('roommate_gender'))
                                        <span class="btn-danger" role="alert">
                                            <strong>{{ $errors->first('roommate_gender') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                        </div>


                    <h3 style="margin-top: 30px;">Basic Information</h3>
                    <hr>
                        
                      

                    <div class="form-group" style="margin-top: 20px;">
                        <label class="col-sm-3 control-label" for="category_id">{{ __('Room Category') }}</label>
                        <div class="col-sm-6">
                            <select name="category_id" type="category_id" id="category_id" class="form-control {{ $errors->has('category_id') ? ' is-valid' : '' }}" value="{{ old('category_id') }}">
                                <option value="0" disabled="true" selected="true">===Select Category===</option>
                                @foreach(App\Models\Admin\Category::orderBy('name','desc')->where('name','Bachelor')->get(); as $parent)
                                <option value="{{$parent->id}}">{!!$parent->name!!}</option>
                                    @foreach(App\Models\Admin\Category::orderBy('name','desc')->where('parent_id',$parent->id)->get(); as $child)
                                        <option  value="{{$child->id}}">{{$child->name}}</option>
                                    @endforeach
                                @endforeach
                            </select>
                            @if ($errors->has('category_id'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('category_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    
                    
                    <div class="form-group">
                            <label for="month" class="col-sm-3 control-label">Month</label>
                            <div class="col-sm-3">
                                <select name="month" class="form-control" id="month">
                                    <option value="0" disabled="true" selected="true">===Select Month===</option>
                                        <option value="January">January</option>
                                        <option value="February">February </option>
                                        <option value="March"> March </option>
                                        <option value="April"> April </option>
                                        <option value="May"> May </option>
                                        <option value="June">June </option>
                                        <option value="July">July </option>
                                        <option value="August">August </option>
                                        <option value="September">September </option>
                                        <option value="October">October </option>
                                        <option value="November">November </option>
                                        <option value="December">December</option>
                                        <option value="No Condition">No Condition</option>
                                </select>
                                @if ($errors->has('month'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('month') }}</strong>
                                    </span>
                                    @endif
                            </div>
                             <div class="col-sm-3">
                                    <div class="row" style="margin-top: 5px;">
                                        <label class="col-sm-6 text-center">
                                            <input type="radio" class="flat-red" name="year" value="2019">  2019
                                        </label>
                                        <label class="col-sm-6 text-center">
                                             <input type="radio" class="flat-red" name="year" value="2020">  2020
                                        </label>

                                        @if ($errors->has('year'))
                                        <span class="btn-danger" role="alert">
                                            <strong>{{ $errors->first('year') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            
                        </div>

                        

                        <div class="form-group">
                            <label for="seat" class="col-sm-3 control-label">Available Seat</label>
                            <div class="col-sm-6">
                                <select name="seat" type="seat" id="seat" class="form-control{{ $errors->has('seat') ? ' is-valid' : '' }}" value="{{ old('seat') }}">
                                        <option value="0" disabled="true" selected="true">===Select Seat===</option>
                                        <option value="1" >1</option>
                                        <option value="2" >2</option>
                                        <option value="3" >3</option>
                                        <option value="4" >4</option>
                                        <option value="5">5</option>
                                        <option value="6" >6</option>
                                        <option value="7" >7</option>
                                        <option value="8" >8</option>
                                    </select>
                                    @if ($errors->has('seat'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('seat') }}</strong>
                                    </span>
                                    @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="room_status" class="col-sm-3 control-label">Room Status</label>
                            <div class="col-sm-6">
                                 <select name="room_status" type="room_status" id="room_status" class="form-control{{ $errors->has('room_status') ? ' is-valid' : '' }}" value="{{ old('room_status') }}">
                                <option value="0" disabled="true" selected="true">===Select Room Status===</option>
                                <option value="Flat">Flat</option>
                                <option value="Semi-ripe house">Semi-ripe house</option>
                            </select>
                            @if ($errors->has('room_status'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('room_status') }}</strong>
                            </span>
                            @endif
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="room_rent" class="col-sm-3 control-label">Per seat Rent</label>
                            <div class="col-sm-6">
                                <input type="float" class="form-control" name="room_rent" id="room_rent">
                                @if ($errors->has('room_rent'))
                                <span class="btn-danger" role="alert">
                                    <strong>{{ $errors->first('room_rent') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    
                    
                   
                    <div class="form-group">
                        <label for="facilities" class="col-sm-3 control-label">{{ __('Room facilities') }}</label>
                        <div class="col-sm-6">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Attach Bathroom" />Attach Bathroom
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="A Balcony along with the room" />A Balcony along
                                    
                                         </td>
                                    </tr>
                                    <tr>
                                        
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="A quiet and Lovely environment" />A quiet and Lovely environment
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Wifi" />Wifi
                                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Security Guard"/>Security Guard
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Tiles" />Tiles
                                    
                                        </td>
                                       
                                    </tr>
                                    <tr>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Fully Decorated"/>Fully Decorated
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="24 Hours Water and Gass Supply" />24 Hours Water  & guss
                                    
                                        </td>
                                       
                                    </tr>
                                </thead>
                            </table>

                            @if ($errors->has('facilities'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('facilities') }}</strong>
                            </span>
                            @endif

                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label for="conditions" class="col-sm-3 control-label">{{ __('Room Conditions') }}</label>
                        <div class="col-sm-6">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="conditions[]" value="Must be Nonsmoker" />Must be Nonsmoker
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="conditions[]" value="Must be maintain of role of Room" />Must be maintain of role of Room
                                    
                                         </td>
                                    </tr>
                                    <tr>
                                        <td>
            
                                                 <input type="checkbox" class="flat-red" name="conditions[]" value="Before 11PM come back" />Before 11PM come back
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="conditions[]" value="Before 11PM come back" />Before 11PM come back</br>
                                    
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                                
                            @if ($errors->has('conditions'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('conditions') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="phone" class="col-sm-3 control-label">{{ __('Contact Number') }}</label>
                        <div class="col-sm-6">
                              
                                <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-valid' : '' }}" placeholder="Enter eleven phone" name="phone" value="{{ old('phone') }}">
                              
                              @if ($errors->has('phone'))
                                <span class="btn-danger" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                               @endif
                        </div>
                   </div>

                   <div class="form-group">
                        <label for="short_description" class="col-sm-3 control-label">{{ __('Short Description') }}</label>
                        <div class="col-sm-6 checkbox-inline">
                            <textarea id="editor" class="form-control{{ $errors->has('short_description') ? ' is-valid' : '' }}" name="short_description" value="{{ old('short_description') }}" cols="40" rows="5">{{ old('short_description') }}</textarea>
                            @if ($errors->has('short_description'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('short_description') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>


                   <div class="form-group">
                        <label for="images" class="col-sm-3 control-label">{{ __('Images') }}</label>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6">
                                     <div style="margin-top: 5px">
                                        <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                                    </div>

                                    <div style="margin-top: 5px">
                                        <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                                    </div>

                                    <div style="margin-top: 5px">
                                        <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div style="margin-top: 5px">
                                        <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                                    </div>

                                    <div style="margin-top: 5px">
                                        <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                                    </div>

                                    <div style="margin-top: 5px">
                                        <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                                    </div>
                                 </div>
                            </div>

                            @if ($errors->has('images'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('images') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                   <h3 style="margin-top: 30px;">Location and Address </h3>
                   <hr>
                    <div class="form-group">
                        <label for="city_id" class="col-sm-3 control-label">{{ __(' City Name') }}</label>
                        <div class="col-sm-6">
                            <select name="city_id" type="text" id="city_id" class="form-control{{ $errors->has('city_id') ? ' is-valid' : '' }}" value="{{ old('city_id') }}">
                                <option value="0" disabled="true" selected="true">===Select City===</option>
                                @foreach($cities as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('city_id'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('city_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="thana_id" class="col-sm-3 control-label">{{ __(' Thana Name') }}</label>
                        <div class="col-sm-6">
                            <select name="thana_id" type="text" id="thana_id" class="form-control{{ $errors->has('thana_id') ? ' is-valid' : '' }}" value="{{ old('thana_id') }}">
                                <option value="0" disabled="true" selected="true">===Select Thana===</option>
                            </select>
                            @if ($errors->has('thana_id'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('thana_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ward_id" class="col-sm-3 control-label">{{ __(' Ward Name') }}</label>
                        <div class="col-sm-6">
                            <select name="ward_id" type="text" id="ward_id" class="form-control{{ $errors->has('ward_id') ? ' is-valid' : '' }}" value="{{ old('ward_id') }}">
                                <option value="0" disabled="true" selected="true">===Select Ward===</option>
                            </select>
                            @if ($errors->has('ward_id'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('ward_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="col-sm-3 control-label">{{ __('Address') }}</label>
                        <div class="col-sm-6">
                            <textarea class="form-control{{ $errors->has('address') ? ' is-valid' : '' }}" name="address" value="{{ old('address') }}" cols="40" rows="5">{{ old('address') }}</textarea>
                            @if ($errors->has('address'))
                            <span class="btn-danger" role="alert">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    

                   <div class="box-footer">
                    <button class="btn btn-success btn-flat pull-right" type="submit">Post Bachelor</button>
                </div>
                    
                    {{ Form::close() }}
             
          </div>
  </div>
</section>
@endsection

          
@section('scripts')
<script>
  $(function () {
    

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
  })
</script>


<script type="text/javascript">
         CKEDITOR.replace( 'address',
         {
          customConfig : 'config.js',
          toolbar : 'simple'
          })
</script> 


<script type="text/javascript">
$(document).ready(function(){
$('select[name="city_id"]').on('change',function(){
var city_id=$(this).val();
//alert(city_id);
if(city_id){
$.ajax({
url:'{{ url('')}}/cities/ajax/'+city_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="thana_id"]').empty();
$.each(data,function(key,value){
$('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="thana_id"]').empty();
}
});
$('select[name="thana_id"]').on('change',function(){
var thana_id=$(this).val();
//alert(thana_id);
console.log(thana_id);
if(thana_id){
$.ajax({
url:'{{ url('')}}/thanas/ajax/'+thana_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="ward_id"]').empty();
$.each(data,function(key,value){
$('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="ward_id"]').empty();
}
});
});
</script>


@endsection
