@extends('backend.user.layouts.master')
@section('title')
    User-Dashboard
@endsection
@section('content')

<div class="container">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user" style="margin-top: 40px;">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              
            </div>
            <div class="widget-user-image">
               <img src="{{asset('images/users/'.Auth::user()->images)}}" class="img-circle" alt="User Image">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h3 class="widget-user-username">{{ Auth::user()->name}}</h3>
                    <h5 class="widget-user-desc">{{ Auth::user()->email}} </h5>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.widget-user -->
        </div>

@endsection