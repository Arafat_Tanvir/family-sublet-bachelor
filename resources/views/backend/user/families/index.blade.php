@extends('backend.user.layouts.master')
@section('content')
@foreach($families as $family)
<section class="content">
    <div class="row">
      <div class="col-sm-12">
       <div class="box">
         <div class="box-body">
        <div class="col-sm-6 ">
         <div class="box box-solid">
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                 
                  @foreach($family->images as $image)
                  <li data-target="#carousel-example-generic" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
                  @endforeach
                </ol>
                <div class="carousel-inner">
                  @php
                  $i=1
                  @endphp

                  @foreach($family->images as $image)
                  <div class="item {{$i==1 ? 'active' : ''}}">
                    <img  src="{{asset('images/family_room/'.$image->images)}}" style="width:100%;height: 320px;">
                    <div class="carousel-caption">
                        <h3>Easy Finder</h3>
                        <p>Bachelor Roommate Wanted!</p>
                        
                    </div>
                  </div>
                  @php

                  $i++
                  @endphp
                  @endforeach
                  
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
          </div>           
        </div>
        <div class="col-sm-6" onclick="location.href='{{route('families-user-show',$family->id)}}'">
          <div class="text-center" style="background-color: #36B9CC;color: white;">
              <div class="box-body">
                <img src="{{asset('images/categories/'.$family->category->images)}}" class="rounded-circle" height="50px" width="50px" alt="">
              <p>{{ $family->room_status}}, 1st {{ $family->month}},{{$family->year}}</p>
                <p class="badge badge-warning" ">{{ $family->room_rent}} /TK</p>
              </div>
          </div>
          <div class="text-center">

            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Category</th>
                  <td>{{ $family->category->name}}</td>
                </tr>
                <tr>
                  <th>family Type</th>
                  <td>{{ $family->family_gender}}</td>
                </tr>
                <tr>
                  <th>Ward</th>
                  <td>{{ $family->ward->name}}</td>
                </tr>
                <tr>
                  <th>Address</th>
                  <td>{{ $family->address}}</td>
                </tr>
              </thead>
            </table>
          </div>
          <div class="text-right">
          <table class="table">
            <thead>
              <tr>
                <th style="text-align: left;">
                  <a href="{{route('families-user-edit', $family->id)}}" ><i class="fa  fa-pencil-square-o" style="font-size:20px;color: green"></i></a>
                </th>
                <th style="text-align: right;">
                   @if($family->confirmed)
                         
                    @else
                    <i>Pending</i>
                    @endif
                </th>
                <th style="text-align: right;">
                   @if($family->is_seen)
                      <i>Seen(admin)</i>
                      @else
                      <i>Unseen</i>
                      @endif
                </th>
              </tr>
            </thead>
          </table>
          </div>   
        </div>
        </div>          
          </div>
          </div>        
    </div>
</section>
@endforeach

  @endsection