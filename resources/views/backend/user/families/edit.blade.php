@extends('backend.user.layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
              <div>
            			{{ Form::open([
                  'route' => ['families-user-update',$families->id],
                  'method' => 'POST',
                  'class'=>'form-horizontal form-horizontal row-fluid',
                  'enctype'=>'multipart/form-data'
                  ])
                  }}

              <h3 style="margin-top: 30px;margin-left: 40px; ">Family Basic Information </h3>
              <div class="form-group" style="margin-top: 20px;">
                  <label class="col-sm-3 control-label" for="category_id">{{ __('Room Category') }}</label>
                  <div class="col-sm-6">
                      <select name="category_id" type="category_id" id="category_id" class="form-control {{ $errors->has('category_id') ? ' is-valid' : '' }}" value="{{ old('category_id') }}">
	                        <option value="0" disabled="true" selected="true">===Select Category===</option>
                          @foreach(App\Models\Admin\Category::orderBy('name','desc')->where('name','family')->get(); as $parent)
                          <option value="{{$parent->id}}" {{ $parent->id ==$families->category_id ? 'selected' : ''}}>{{$parent->name}}</option>
                            @foreach(App\Models\Admin\Category::orderBy('name','desc')->where('parent_id',$parent->id)->get(); as $child)
                                <option value="{{$child->id}}" {{ $child->id ==$families->category_id ? 'selected' : ''}}>--->{{$child->name}}</option>
                            @endforeach
                          @endforeach
                      </select>
                      @if ($errors->has('category_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('category_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>


              <div class="form-group">
                        <label class="col-sm-3 control-label" for="room">Select Month and Year</label>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6" style="margin-top: 3px;">
                                    <select name="month" type="month" id="month" class="form-control{{ $errors->has('month') ? ' is-valid' : '' }}" value="{{ old('month') }}">
                                        <option value="0" disabled="true" selected="true">===Select Month===</option>
                                        <option value="January"@if ($families->month == "January")selected="selected" @endif >January</option>
                                        <option value="February" @if ($families->month == "February")selected="selected" @endif >February </option>
                                        <option value="March" @if ($families->month == "March")selected="selected" @endif > March </option>
                                        <option value="April" @if ($families->month == "April")selected="selected" @endif > April </option>
                                        <option value="May" @if ($families->month == "May")selected="selected" @endif > May </option>
                                        <option value="June" @if ($families->month == "June")selected="selected" @endif >June </option>
                                        <option value="July" @if ($families->month == "July")selected="selected" @endif >July </option>
                                        <option value="August" @if ($families->month == "August")selected="selected" @endif >August </option>
                                        <option value="September" @if ($families->month == "September")selected="selected" @endif >September </option>
                                        <option value="October" @if ($families->month == "October")selected="selected" @endif >October </option>
                                        <option value="November" @if ($families->month == "November")selected="selected" @endif >November </option>
                                        <option value="December" @if ($families->month == "December")selected="selected" @endif >December </option>
                                          <option value="No Condition" @if ($families->month == "No Condition")selected="selected" @endif >No Condition</option>
                                    </select>
                                    @if ($errors->has('month'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('month') }}</strong>
                                    </span>
                                    @endif
                                </div>
                               <div class="col-sm-6 text-center">

                                  <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <td>
                                                    <input type="radio" class="flat-red" name="year" value="2019" @if($families->year=='2019') checked @endif>2019
                                                </td>
                                                <td>
                                                    <input type="radio" class="flat-red" name="year" value="2020" @if($families->year=='2020') checked @endif>2020
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>

                                    

                                        @if ($errors->has('year'))
                                        <span class="btn-danger" role="alert">
                                            <strong>{{ $errors->first('year') }}</strong>
                                        </span>
                                        @endif

                                </div>
                            </div>
                        </div>
                    </div>

              <div class="form-group">
                  <label class="col-sm-3 control-label" for="room_status">{{ __('Room Type') }}</label>
                  <div class="col-sm-6">
                      <select name="room_status" type="room_status" id="room_status" class="form-control{{ $errors->has('room_status') ? ' is-valid' : '' }}" value="{{ old('room_status') }}">
                          <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                          <option value="Flat" @if ($families->room_status == "Flat")selected="selected" @endif >Flat</option>
                          <option value="Semi-ripe house" @if ($families->room_status == "Semi-ripe house")selected="selected" @endif >Semi-ripe house</option>
                      </select>
                      @if ($errors->has('room_status'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('room_status') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                        <label class="col-sm-3 control-label" for="room">Room Rent</label>
                        <div class="col-sm-6">
                                    <input id="room_rent" type="double" class="form-control{{ $errors->has('room_rent') ? ' is-valid' : '' }}" name="room_rent" value="{{ $families->room_rent }}">
                                    @if ($errors->has('room_rent'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('room_rent') }}</strong>
                                    </span>
                                    @endif
                                
                                
                        </div>
                    </div>

              
              
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="total_room">{{ __('Total Room') }}</label>
                  <div class="col-sm-6">
                      <select name="total_room" type="total_room" id="total_room" class="form-control{{ $errors->has('total_room') ? ' is-valid' : '' }}" value="{{ old('total_room') }}">
                          <option value="0" disabled="true" selected="true">===Family Total Room===</option>
                          <option value="1"@if ($families->total_room == "1")selected="selected" @endif >1</option>
                          <option value="2"@if ($families->total_room == "2")selected="selected" @endif >2</option>
                          <option value="3"@if ($families->total_room == "3")selected="selected" @endif >3</option>
                          <option value="4"@if ($families->total_room == "4")selected="selected" @endif >4</option>
                          <option value="5"@if ($families->total_room == "5")selected="selected" @endif>5</option>
                          <option value="6"@if ($families->total_room == "6")selected="selected" @endif >6</option>
                          <option value="7"@if ($families->total_room == "7")selected="selected" @endif >7</option>
                          <option value="6"@if ($families->total_room == "6")selected="selected" @endif >6</option>
                      </select>
                      @if ($errors->has('total_room'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('total_room') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="bed_room">{{ __('How Many Bed Room') }}</label>
                  <div class="col-sm-6">
                      <select name="bed_room" type="bed_room" id="bed_room" class="form-control{{ $errors->has('bed_room') ? ' is-valid' : '' }}" value="{{ old('bed_room') }}">
                          <option value="0" disabled="true" selected="true">===Select Bed Room Available===</option>
                           <option value="1"@if ($families->bed_room == "1")selected="selected" @endif >1</option>
                          <option value="2"@if ($families->bed_room == "2")selected="selected" @endif >2</option>
                          <option value="3"@if ($families->bed_room == "3")selected="selected" @endif >3</option>
                          <option value="4"@if ($families->bed_room == "4")selected="selected" @endif >4</option>
                          <option value="5"@if ($families->bed_room == "5")selected="selected" @endif>5</option>
                          <option value="6"@if ($families->bed_room == "6")selected="selected" @endif >6</option>
                          <option value="7"@if ($families->bed_room == "7")selected="selected" @endif >7</option>
                          <option value="6"@if ($families->bed_room == "6")selected="selected" @endif >6</option>
                      </select>
                      @if ($errors->has('bed_room'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('bed_room') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="bath_room">{{ __('How Many Bath Room') }}</label>
                  <div class="col-sm-6">
                      <select name="bath_room" type="bath_room" id="bath_room" class="form-control{{ $errors->has('bath_room') ? ' is-valid' : '' }}" value="{{ old('bath_room') }}">
                          <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                          <option value="1"@if ($families->bath_room == "1")selected="selected" @endif >1</option>
                          <option value="2"@if ($families->bath_room == "2")selected="selected" @endif >2</option>
                          <option value="3"@if ($families->bath_room == "3")selected="selected" @endif >3</option>
                          <option value="4"@if ($families->bath_room == "4")selected="selected" @endif >4</option>
                          <option value="5"@if ($families->bath_room == "5")selected="selected" @endif>5</option>
                          <option value="6"@if ($families->bath_room == "6")selected="selected" @endif >6</option>
                          <option value="7"@if ($families->bath_room == "7")selected="selected" @endif >7</option>
                          <option value="6"@if ($families->bath_room == "6")selected="selected" @endif >6</option>
                      </select>
                      @if ($errors->has('bath_room'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('bath_room') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="dinning_room">{{ __('Dinning Room') }}</label>
                  <div class="col-sm-6 text-center">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>
                                            <input type="radio" class="flat-red" name="dinning_room" value="Yes"@if($families->dinning_room=='Yes') checked @endif>Yes
                                        </td>
                                        <td>
                                            <input type="radio" class="flat-red" name="dinning_room" value="No"@if($families->dinning_room=='No') checked @endif>No
                                        </td>
                                    </tr>
                                </thead>
                            </table>

                      @if ($errors->has('dinning_room'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('dinning_room') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <label class="col-sm-3 control-label" for="drawing_room">{{ __('Drawing Room') }}</label>
                  <div class="col-sm-6 text-center">

                  <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>
                                    <input type="radio" class="flat-red" name="drawing_room" value="Yes"@if($families->drawing_room=='Yes') checked @endif>Yes
                                </td>
                                <td>
                                   <input type="radio" class="flat-red" name="drawing_room" value="No"@if($families->drawing_room=='No') checked @endif>No
                                </td>
                            </tr>
                        </thead>
                    </table>
                      
                      
                      @if ($errors->has('drawing_room'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('drawing_room') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
            
              
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="city_id">{{ __('Facilities') }}</label>
                  <div class="col-sm-6">




                    <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Fully Decorated"@if(in_array('Fully Decorated', $facilities)) checked @endif/>Fully Decorated
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Attach Bathroom"@if(in_array('Attach Bathroom', $facilities)) checked @endif />Attach Bathroom
                                    
                                         </td>
                                    </tr>
                                    <tr>
                                        
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="A Balcony along with the room" @if(in_array('A Balcony along with the room', $facilities)) checked @endif/>A Balcony along
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="24 Hours Water and Gass Supply" @if(in_array('24 Hours Water and Gass Supply', $facilities)) checked @endif/>24 Hours Water  & guss
                                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
            
                                               <input type="checkbox" class="flat-red" name="facilities[]" value="Security Guard"@if(in_array('Security Guard', $facilities)) checked @endif/>Security Guard</br>
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="facilities[]" value="Tiles" @if(in_array('Tiles', $conditions)) checked @endif/>Tiles</br>
                                    
                                        </td>
                                       
                                    </tr>
                                    <tr>
                                        <td>
            
                                               <input type="checkbox" class="flat-red" name="facilities[]" value="Wifi"@if(in_array('Wifi', $facilities)) checked @endif />Wifi
                                    
                                        </td>
                                        <td>
            
                                                 <input type="checkbox" class="flat-red" name="facilities[]" value="A quiet and Lovely environment" @if(in_array('A quiet and Lovely environment', $facilities)) checked @endif/>A quiet and Lovely environment
                                    
                                        </td>
                                       
                                    </tr>
                                </thead>
                            </table>
                      @if ($errors->has('facilities'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('facilities') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="city_id">{{ __(' Conditions') }}</label>
                  <div class="col-sm-6">
                    <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>
            
                                                 <input type="checkbox" class="flat-red" name="conditions[]" value="Before 11PM come back" @if(in_array('Before 11PM come back', $conditions)) checked @endif/>Before 11PM come back
                                    
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="conditions[]" value="Before 11PM come back"@if(in_array('Before 11PM come back', $conditions)) checked @endif />Before 11PM come back
                                    
                                         </td>
                                    </tr>
                                    <tr>
                                        <td>
            
                                                 <input type="checkbox" class="flat-red" name="conditions[]" value="Must be maintain of role of Room" @if(in_array('Must be maintain of role of Room', $conditions)) checked @endif/>Must be maintain of role of Room
                                        </td>
                                        <td>
            
                                                <input type="checkbox" class="flat-red" name="conditions[]" value="Must be Nonsmoker" @if(in_array('Must be Nonsmoker', $conditions)) checked @endif/>Must be Nonsmoker
                                    
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                      
                     
                      @if ($errors->has('conditions'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('conditions') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label" for="phone">{{ __('Phone Number') }}</label>
                <div class="col-sm-6">
                    
                      
                        <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-valid' : '' }}" name="phone" value="{{ $families->phone}}">
                     
                    
                    
                    @if ($errors->has('phone'))
                    <span class="btn-danger" role="alert">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                  <label class="col-sm-3 control-label" for="short_description">{{ __('Short Description') }}</label>
                  <div class="col-sm-6">
                      <textarea id="editor" class="form-control{{ $errors->has('short_description') ? ' is-valid' : '' }}" name="short_description" value="{{ old('short_description')}}" cols="40" rows="5">{!! $families->short_description !!}</textarea>
                      @if ($errors->has('short_description'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('short_description') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

            <h3 style="margin-top: 30px;">Location and Address </h3>

              <div class="form-group">
                  <label class="col-sm-3 control-label" for="city_id">{{ __(' City Name') }}</label>
                  <div class="col-sm-6">
                      <select name="city_id" type="text" id="city_id" class="form-control{{ $errors->has('city_id') ? ' is-valid' : '' }}" value="{{ old('city_id') }}">
                          <option value="0" disabled="true" selected="true">===Select City===</option>
                            @foreach(App\Models\Admin\City::orderBy('name','desc')->get(); as $city)
		                        <option value="{{$city->id}}" {{ $city->id ==$families->city_id ? 'selected' : ''}}>{{$city->name}}</option>
	                        @endforeach
                      </select>
                      @if ($errors->has('city_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('city_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="thana_id">{{ __(' Thana Name') }}</label>
                  <div class="col-sm-6">
                      <select name="thana_id" type="text" id="thana_id" class="form-control{{ $errors->has('thana_id') ? ' is-valid' : '' }}" value="{{ old('thana_id') }}">
                          <option value="0" disabled="true" selected="true">===Select Thana===</option>
                          @foreach(App\Models\Admin\Thana::orderBy('name','desc')->get(); as $thana)
		                        <option value="{{$thana->id}}" {{ $thana->id ==$families->thana_id ? 'selected' : ''}}>{{$thana->name}}</option>
	                        @endforeach
                      </select>
                      @if ($errors->has('thana_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('thana_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="ward_id">{{ __(' Ward Name') }}</label>
                  <div class="col-sm-6">
                      <select name="ward_id" type="text" id="ward_id" class="form-control{{ $errors->has('ward_id') ? ' is-valid' : '' }}" value="{{ old('ward_id') }}">
                          <option value="0" disabled="true" selected="true">===Select Ward===</option>
                          @foreach(App\Models\Admin\Ward::orderBy('name','desc')->get(); as $ward)
		                        <option value="{{$ward->id}}" {{ $ward->id ==$families->ward_id ? 'selected' : ''}}>{{$ward->name}}</option>
	                       @endforeach
                      </select>
                      @if ($errors->has('ward_id'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('ward_id') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label" for="address">{{ __('Address') }}</label>
                  <div class="col-sm-6">
                      <textarea  class="form-control{{ $errors->has('address') ? ' is-valid' : '' }}" name="address" value="{{ old('address') }}" cols="40" rows="5">{!! $families->address !!}</textarea>
                      @if ($errors->has('address'))
                      <span class="btn-danger" role="alert">
                          <strong>{{ $errors->first('address') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

               <div class="box-footer">
                    <button class="btn btn-success btn-flat pull-right" type="submit">Family Update</button>
                </div>
              
              {{ Form::close() }}
                 </div>

                
            </div>
        </div>
    </div>
</div>
</section>
@endsection

          
@section('scripts')


<script>
  $(function () {
    

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
  })
</script>


<script type="text/javascript">
$(document).ready(function(){
$('select[name="city_id"]').on('change',function(){
var city_id=$(this).val();
//alert(city_id);
if(city_id){
$.ajax({
url:'{{ url('')}}/cities/ajax/'+city_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="thana_id"]').empty();
$.each(data,function(key,value){
$('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="thana_id"]').empty();
}
});
$('select[name="thana_id"]').on('change',function(){
var thana_id=$(this).val();
//alert(thana_id);
console.log(thana_id);
if(thana_id){
$.ajax({
url:'{{ url('')}}/thanas/ajax/'+thana_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="ward_id"]').empty();
$.each(data,function(key,value){
$('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="ward_id"]').empty();
}
});
});
</script>
@endsection
