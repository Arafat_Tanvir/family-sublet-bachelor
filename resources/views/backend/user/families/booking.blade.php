@extends('backend.user.layouts.master')

@section('title')
    Bechelor-booking
@endsection

@section('content')
    <section class="content-header alert alert-info">
      <h1 style="margin-bottom: 12px;">
        Payment & order process
      </h1>
    </section>

 <section class="invoice content-header">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> AdminLTE, Inc.
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Admin, Inc.</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@almasaeedstudio.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>John Doe</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (555) 539-1037<br>
            Email: john.doe@example.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #007612</b><br>
          <br>
          <b>Order ID:</b> 4F3S8J<br>
          <b>Payment Due:</b> 2/22/2014<br>
          <b>Account:</b> 968-34567
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          {{ 
                		Form::open([
		                    'route' => 'user-order',
		                    'method' => 'POST',
		                    'class'=>'form-horizontal form-horizontal row-fluid',
		                    'enctype'=>'multipart/form-data'
                          ])

                    }}


						<input type="hidden" name="family_id" value="{{ $families->id }}" />

						<div class="form-group">
	                        <label class="col-sm-3 control-label" for="name">Name</label>
	                        <div class="col-sm-9">
	                            
                                    <input id="name" type="double" class="form-control" name="name" value="{{ Auth::check() ? Auth::user()->name : '' }}">
                                    @if ($errors->has('name'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="col-sm-3 control-label" for="email">email</label>
	                        <div class="col-sm-9">
	                            
                                    <input id="email" type="double" class="form-control" name="email" value="{{ Auth::check() ? Auth::user()->email : '' }}">
                                    @if ($errors->has('email'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <label class="col-sm-3 control-label" for="phone">phone</label>
	                        <div class="col-sm-9">
	                            
                                    <input id="phone" type="double" class="form-control" name="phone" value="{{ Auth::check() ? Auth::user()->phone : '' }}">
                                    @if ($errors->has('phone'))
                                    <span class="btn-danger" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <label class="col-sm-3 control-label" for="message">message</label>
	                        <div class="col-sm-9">
	                            
                                    <textarea name="message" id="message" class="form-control" cols="30" rows="5" value="{{ old('message') }}"placeholder="Enter Your message"  autofocus></textarea>
                                    @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
	                        </div>
	                    </div>


	                    <div class="form-group">
	                        <label class="col-sm-3 control-label" for="payment_id">payment_id</label>
	                        <div class="col-sm-9">
	                            
                                    <select name="payment_id" class="form-control" value="{{old('payment_id')}}" id="payments">
										<option value="0" disabled="true" selected="true" >===Select Payment Method===</option>
										@foreach(App\Models\Admin\Payment::orderBy('id','desc')->get(); as $payment))
										<option value="{{$payment->short_name}}" ng-model="payment_id">{{$payment->name}}</option>
										@endforeach
									</select>

                                    @if ($errors->has('payment_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('payment_id') }}</strong>
                                    </span>
                                    @endif
	                        </div>
	                    </div>


							@foreach(App\Models\Admin\Payment::orderBy('id','desc')->get(); as $payment)
							
							@if($payment->short_name=="Cash")
							<div id="payment_{{ $payment->short_name}}" class="hidden alert alert-success">
								<div class="cash">
										<img src="{{asset('images/payments/'.$payment->image)}}" height="160px" width="100%" alt="Rocket Image" class="">
									
									<hr>
									<h4 class="badge badge-warning"> {{$payment->name}} Payment </h4><br>
									<h5 class="alert alert-info">For Cash in there is noting necssary.Just Finish Your Booking</h5><br>
								</div>
							</div>
							@else
							<div id="payment_{{ $payment->short_name}}" class="hidden alert alert-success">
								<div class="bkash">
									
										<img src="{{asset('images/payments/'.$payment->image)}}"  height="160px" width="100%" alt="Rocket Image" class="">
									
								
									<h4 class="badge badge-warning"> {{$payment->name}} Payment </h4>
									<p>
										<strong>{{$payment->name}} No: {{ $payment->no}}</strong><br>
										<strong class="badge badge-info"> Account Type: {{ $payment->type}}</strong>
										<div class="alert alert-danger">
											<p>
											Please send the Above money to this <span badge badge-success>{{$payment->name}}</span> money and write your transaction code bellow  there
											</p>
										</div>
									</p>
								</div>
							</div>
							@endif
							
							@endforeach
							<input type="text" id="transaction_id" placeholder="Please inter Transaction ID" name="transaction_id" class="form-control hidden">
							<br>
						<button type="submit" class="btn btn-primary btn-flat pull-right">
                        Order
                        </button>
					</form>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount Due 2/22/2014</p>

          <div class="table-responsive">

            <table class="table table-bordered">
            	<thead>
            		<tr>
            			<td>Category Name: </td>
            			<td>{{$families->category->name}}</td>
            		</tr>
            		<tr>
            			<td>Avaiable From : </td>
            			<td>{{$families->month}},{{$families->year}}</td>
            		</tr>
            		<tr>
            			<td>Status :  </td>
            			<td>{{$families->room_status}}</td>
            		</tr>
            		<tr>
            			<td> Rent :  </td>
            			<td>{{$families->room_rent}}</td>
            		</tr>
            		<tr>
            			<td>Category Name: </td>
            			<td>{{$families->category->name}}</td>
            		</tr>

            	</thead>
             
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
@section('scripts')

<script type="text/javascript">
	$("#payments").change(function() {
		//alert('dsfaskf');
		$payment_method=$("#payments").val();
		if ($payment_method=="Cash") {
			$("#payment_Cash").removeClass('hidden');
			$("#payment_Rocket").addClass('hidden');
			$("#transaction_id").addClass('hidden');
			$("#payment_bKash").addClass('hidden');
		}else if($payment_method=="bKash"){
			$("#payment_bKash").removeClass('hidden');
			$("#transaction_id").removeClass('hidden');
			$("#payment_Cash").addClass('hidden');
			$("#payment_Rocket").addClass('hidden');
		}else if($payment_method=="Rocket"){
			$("#payment_Rocket").removeClass('hidden');
			$("#transaction_id").removeClass('hidden');
			$("#payment_Cash").addClass('hidden');
			$("#payment_bKash").addClass('hidden');
		}
	});
</script>
@endsection