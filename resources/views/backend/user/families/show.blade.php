@extends('backend.user.layouts.master')

@section('title')
    Bechelor-booking
@endsection

@section('content')
<section class="content">
    <div class="row">
		<div class="col-sm-12">
			<div class="box">
                <div class="box-body">
					<div class="card shadow mb-4">
					    <div class="card-body">
					    			<div class="text-center" style="background-color: #36B9CC;color: white;">
						          <div class="box-body">
						            <img src="{{asset('images/categories/'.$families->category->images)}}" class="rounded-circle" height="100px" width="100px" alt="">
						          <h3>{{ $families->room_status}}, 1st {{ $families->month}},{{$families->year}}</h3>

						            <h2 class="alert alert-pill" ">BDT : {{ $families->room_rent}}</h2>
						          </div>
						      </div>
						           
						            <div class="box box-solid">
								            <div class="box-body">
								              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
								                <ol class="carousel-indicators">
								                 
								                  @foreach($families->images as $image)
								                  <li data-target="#carousel-example-generic" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
								                  @endforeach
								                </ol>
								                <div class="carousel-inner">
								                  @php
								                  $i=1
								                  @endphp

								                  @foreach($families->images as $image)
								                  <div class="item {{$i==1 ? 'active' : ''}}">
								                    <img  src="{{asset('images/family_room/'.$image->images)}}" style="max-height: 350px;" width="100%">
								                    
								                  </div>
								                  @php

								                  $i++
								                  @endphp
								                  @endforeach
								                  
								                </div>
								                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
								                  <span class="fa fa-angle-left"></span>
								                </a>
								                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
								                  <span class="fa fa-angle-right"></span>
								                </a>
								              </div>
								            </div>
								          </div>           
					    			<div class="text-center">
					    				<div class="box box-solid">
								            <div class="box-header with-border">
								              <h3 class="box-title">Bachelor Room Info</h3>
								            </div>
								            <!-- /.box-header -->
								            <div class="box-body">
								              <div class="box-group" id="accordion">
								                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
								                <div class="panel box box-primary">
								                  <div class="box-header with-border">
								                    <h4 class="box-title">
								                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								                        Necessary Information
								                      </a>
								                    </h4>
								                  </div>
								                  <div id="collapseOne" class="panel-collapse collapse in">
								                    <div class="box-body">
								                      <div class="col-sm-6">
								                    			<div class="table-responsive">
												                    <table class="table table-bordered text-left">
												                     	<tr>
												                     	 	<th>Category</th>
												                     	 	<td>{{ $families->category->name}}</td>
												                     	</tr>
												                     	
												                     	<tr>
												                     	 	<th>Sublet Gender</th>
												                     	 	<td>{{ $families->sublet_gender}}</td>
												                     	</tr>
														                <tr>
												                     	 	<th>Sublet Religion</th>
												                     	 	<td>{{ $families->sublet_religion}}</td>
												                     	</tr>

												                     	 <tr>
												                     	 	<th>Phone</th>
												                     	 	<td>{{ $families->phone}}</td>
												                     	</tr>
												                     	
												                    </table>
												                </div>
								                    		</div>
								                    		<div class="col-sm-6">
								                    			
								                    		
										                       <div class="text-left py-1">
												    				<h4>Facilities</h4>
												    				<ul style="list-style-type:square;">
												    				    <li>{{ $families->facilities}}</li>
												    				</ul>
												    			</div>
												    			<div class="text-left py-1">
												    			    <h4>Conditions</h4>
												    				<ul style="list-style-type:square;">
												    				    <li>{{ $families->conditions}}</li>
												    				</ul>
												    			</div>

												              </div>
								                    </div>
								                  </div>
								                </div>
								                <div class="panel box box-danger">
								                  <div class="box-header with-border">
								                    <h4 class="box-title">
								                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
								                        Description
								                      </a>
								                    </h4>
								                  </div>
								                  <div id="collapseTwo" class="panel-collapse collapse">
								                    <div class="box-body">
								                      <div class="text-left py-1">
										    			    
										    			    <ul>
										    			        <span>{!! $families->short_description !!}</span>
										    			    </ul>
												    	</div>
								                    </div>
								                  </div>
								                </div>
								                <div class="panel box box-success">
								                  <div class="box-header with-border">
								                    <h4 class="box-title">
								                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
								                        Address & location & Map
								                      </a>
								                    </h4>
								                  </div>
								                  <div id="collapseThree" class="panel-collapse collapse">
								                    <div class="box-body">
								                    	<div class="row">
								                    		<div class="col-sm-6">
								                    			<div class="text-left py-1">
												    			    <ul>
												    			        <span>{!! $families->address !!}</span>
												    			    </ul>
												    			</div>
								                    		</div>
								                    		<div class="col-sm-6">
								                    			<div class="table-responsive">
												                    <table class="table table-bordered text-left">
												                     	<tr>
												                     	 	<th>City</th>
												                     	 	<td>{{ $families->city->name}}</td>
												                     	</tr>
												                     	<tr>
												                     	 	<th>Thana</th>
												                     	 	<td>{{ $families->thana->name}}</td>
												                     	</tr>
												                     	<tr>
												                     	 	<th>Ward</th>
												                     	 	<td>{{ $families->Ward->name}}</td>
												                     	</tr>
												                    </table>
												                </div>
								                    		</div>
								                    	</div>
								                    	
										               
								                        <div style="height: 300px;">
														    {!! Mapper::render() !!}
														</div>
														
								                    </div>
								                  </div>
								                </div>
								              </div>
								            </div>
								            <!-- /.box-body -->
								          </div>





















							               
					    			    </div>
									
							    	 
						        <div class="box box-success">
						            <div class="box-header">
						              <i class="fa fa-comments-o"></i>
						              <h3 class="box-title">Chat</h3>
						              <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
						                {{ str_plural('comment', $families->comments->count()) }} {{$families->comments->count()}}
						              </div>
						            </div>
						            <div class="box-body chat" id="chat-FamilyCommentbox">
						            	 @foreach(App\Models\Admin\Comment::orderBy('id','asc')->where('family_id',$families->id)->where('parent_id',NULL)->get(); as $parent)
						              <!-- chat item -->
						              <div class="item">
						              	@if(isset($parent->user_id))
						                <img src="{{asset('images/users/'.$parent->user->images)}}" alt="user image" class="online">

						                <p class="message">
						                  <a href="#" class="name">
						                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
						                    {{$parent->user->name}}
						                  </a>
						                   {{$parent->description}}
						                 </p>
						                
						                 @else
						                  <img src="{{asset('images/admins/'.$parent->admin->images)}}" alt="user image" class="online">
						                  

						                <p class="message">
						                  <a href="#" class="name" style="color: red">
						                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
						                    {{$parent->admin->name}}
						                  </a>
						                  {{$parent->description}}
						                 </p>
						                 

						                 @endif

						                  
						                   <span href="#comment-{{$parent->id}}" data-toggle="collapse" class="btn btn-primary btn-sm btn-flat pull-right">Reply</span>
						                

						                
						                <div class="attachment">
						                	<div class="child-rows collapse" id="comment-{{$parent->id}}">
							                	@foreach(App\Models\Admin\Comment::orderBy('id','asc')->where('parent_id',$parent->id)->get(); as $child)
							                   <div class="item">
							                   	@if(isset($child->user_id))
							                      <img src="{{asset('images/users/'.$child->user->images)}}" alt="user image" class="online">
							                      <p class="message">
									                  <a href="#" class="name">
									                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
									                    {{$child->user->name}}
									                  </a>
									                  {{$child->description}}
									                </p>
									                @else
									                  
									                 <img src="{{asset('images/admins/'.$child->admin->images)}}" alt="user image" class="online">

									                <p class="message">
									                  <a href="#" class="name" style="color: red">
									                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
									                     {{$child->admin->name}}
									                  </a>
									                  {{$child->description}}
									                 </p>
									                 
									                 @endif
							                  </div>

							                  @endforeach
							                  <div class="child-rows">
								                {{ Form::open([
										                'route' => 'user-comment-store',
										                'method' => 'POST',
										                'class'=>'form-horizontal form-horizontal row-fluid',
										                'enctype'=>'multipart/form-data'
										            ])
								                }}
							                    <input type="hidden" name="parent_id" value="{{ $parent->id }}" />
								                <input type="hidden" name="family_id" value="{{ $families->id }}" />
									              <div class="input-group">
									               <input type="text" class="form-control" name="description" value="{{ old('description') }}" required>
									                @if ($errors->has('description'))
								                        <span class="btn-danger" role="alert">
								                            <strong>{{ $errors->first('description') }}</strong>
								                        </span>
								                    @endif

									                <div class="input-group-btn">
									                  <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
									                </div>
									              </div>
											    {{ Form::close() }}
								            </div>
							                </div>
							            </div>
						               
						              </div>
						              @endforeach
						           
						            </div>

						        {{ Form::open([
					                    'route' => 'user-comment-store',
					                    'method' => 'POST',
					                    'class'=>'form-horizontal form-horizontal row-fluid',
					                    'enctype'=>'multipart/form-data'
					                 ])
			                    }}
				                <input type="hidden" name="family_id" value="{{ $families->id }}" />
				            	 <div class="box-footer">
						              <div class="input-group">
				            			 <input type="text" class="form-control" name="description" value="{{ old('description') }}" required>
				                            @if ($errors->has('description'))
				                        <span class="btn-danger" role="alert">
				                            <strong>{{ $errors->first('description') }}</strong>
				                        </span>
				                            @endif
				                            <div class="input-group-btn">
						                  <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
						                </div>
				            		</div>
				            	</div>
			                    {{ Form::close() }}
						            <!-- /.chat -->
						           
						                
						              </div>
						            
						          </div>
					    	</div>
					    </div>
					</div>
				
	</div>


		
		
		</div>
</section>
@endsection
