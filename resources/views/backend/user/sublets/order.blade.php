@extends('backend.user.layouts.master')

@section('title')
    Bechelor-booking
@endsection

@section('content')
  <section class="content">
    <div class="row ">
      <div class="col-sm-12">
      	@if($sublet_orders->count() > 0)
      	<div class="box">
      	

      		<div class="box-header">
      			<h4 class="lead" style="color: red"><b>Room Order Information</b></h4>
      		</div>
      		<div class="box-body">
      			
      		

       <table class="table table-bordered">
				<thead>
					<tr>
						<th rowspan="2">SL</th>
						<th rowspan="2">Category</th>
						<th rowspan="2">Image</th>
						<th rowspan="2">Seat Quantity</th>
						<th rowspan="2">Per Seat Price</th>
						<th rowspan="2">Sub Total Price</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<div style="display: none;">{{$a=1}}</div>

						@foreach($sublet_orders as $sublet_order)
						<td>{{ $a++ }}</td>
						<td>{{ $sublet_order->id}}</td>
						<td>image</td>
	            	
						<td>
							
						</td>
						<td>rent</td>
						<td>Total Cost</td>
					</tr>
					@endforeach
					<tr>
						<th style="margin-left: 120px" colspan="5">Total Amount</th>
						<th colspan="2">Taka</th>
					</tr>
				</tbody>
			</table>
			</div>
      	</div>
      	@else
			<h4 class="text-center" style="margin-top: 220px;">
				No Order 
			</h4>
		    @endif
      </div>
  </section>
@endsection