<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{asset('images/users/'.Auth::user()->images)}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{ Auth::user()->first_name .' '.Auth::user()->last_name }}</p>
        </div>

    </div>

    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Dashboard</li>
        <li class="active">
            <a href="{{ route('user-dashboard')}}">
                <i class="fa fa-dashboard" style="font-size:20px;color:white"></i><span style="margin-left: 5px;">Dashboard</span>
                <span class="pull-right-container">
            </span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-bed" style="font-size:20px;color:white"></i>
                <span style="margin-left: 5px;">Bachelor Room</span>
                <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('bechelors-user-create') }}"><i class="fa fa-bullhorn" style="font-size:20px;color:white"></i> Advertisement</a></li>
                <li><a href="{{ route('bechelors-user-index') }}"><i class="fa fa-check-circle-o" style="font-size:20px;color:white"></i> Pending  Advertisement</a></li>
                <li><a href="{{ route('bechelors-user-confirmed') }}"><i class="fa fa-thumbs-o-up" style="font-size:20px;color:white"></i> Confirm Bechelor Post</a></li>
                <li><a href="{{ route('user-cards-index') }}"><i class="fa fa-arrows-alt" style="font-size:20px;color:white"></i>Cart</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
               <i class="fa fa-area-chart" style="font-size:20px;color:white"></i>
                <span style="margin-left: 5px;">Sub-let Room</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('sub-lets-user-create') }}"><i class="fa fa-circle-o"></i> Sub-let Advertisement(Post)</a></li>
                <li><a href="{{ route('sub-lets-user-index') }}"><i class="fa fa-circle-o"></i> My Bechelor Post</a></li>
                <li><a href="{{ route('sub-lets-user-confirmed') }}"><i class="fa fa-circle-o"></i>Confirm Sub-let Post</a></li>
                <li><a href="{{ route('sublets-user-order-index') }}"><i class="fa fa-circle-o"></i> Sub-let Order</a></li>
                

            </ul>
        </li>
        <li class="treeview">
            <a href="#">
               <i class="fa fa-object-ungroup" style="font-size:20px;color:white"></i>
                <span style="margin-left: 5px;">Family Room</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('families-user-create') }}"><i class="fa fa-circle-o"></i> Family Advertisement(Post)</a></li>
                <li><a href="{{ route('families-user-index') }}"><i class="fa fa-circle-o"></i> My Family Post</a></li>
                <li><a href="{{ route('families-user-confirmed') }}"><i class="fa fa-circle-o"></i>Confirm Family Post</a></li>

                <li><a href="{{ route('families-user-order-index') }}"><i class="fa fa-circle-o"></i>Family Order</a></li>

            </ul>
        </li>
    </ul>
</section>