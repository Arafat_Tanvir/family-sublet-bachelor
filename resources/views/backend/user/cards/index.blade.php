@extends('backend.user.layouts.master')
@section('title')
  Bachelors-Card-Booking
@endsection
@section('content')
<section class="content">
 <div class="row">
		<div class="col-sm-12">
			<div class="box">
                <div class="box-body">
			@if(App\Models\Admin\Bachelor_Card::orderBy('id','asc')->where('user_id',Auth::id())->where('order_id',NULL)->count() > 0)
			<table class="table table-bordered">
				<thead>
					<tr>
						<th rowspan="2">SL</th>
						<th rowspan="2">Category</th>
						<th rowspan="2">Image</th>
						<th rowspan="2">Seat Quantity</th>
						<th rowspan="2">Per Seat Price</th>
						<th rowspan="2">Sub Total Price</th>
						<th rowspan="2">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<div style="display: none;">{{$a=1}}</div>

						@foreach($bechelor_cards as $card)
						<td>{{ $a++ }}</td>
						<td>{{ $card->bachelor->category->name }}</td>
						<td>

							<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				                <ol class="carousel-indicators">
				                 
				                  @foreach($card->bachelor->images as $image)
				                  <li data-target="#carousel-example-generic" data-slide-to="{{$image->id}}" class="{{$image->id==$image->id ? 'active' : ''}}"></li>
				                  @endforeach
				                </ol>
				                <div class="carousel-inner">
				                  @php
				                  $i=1
				                  @endphp

				                  @foreach($card->bachelor->images as $image)
				                  <div class="item {{$i==1 ? 'active' : ''}}">
				                    <img  src="{{asset('images/bechelor_room/'.$image->images)}}" style="width:100%;height: 100px;">
				                  </div>
				                  @php

				                  $i++
				                  @endphp
				                  @endforeach
				                  
				                </div>
				                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
				                  <span class="fa fa-angle-left"></span>
				                </a>
				                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
				                  <span class="fa fa-angle-right"></span>
				                </a>
				              </div>


						</td>
	            	
						<td>
							<form style="margin-left: 40px" class="form-inline" action="{{ route('user-cards-update', $card->id)}}" method="POST">
								{{csrf_field()}}
									<input type="number" value="{{$card->seat_quantity}}" name="seat_quantity" class="col-sm-6 form-control">
							        <button type="submit" class="btn btn-primary btn-sm">Update</button>
							</form>
						</td>
						<td>{{ $card->bachelor->room_rent}}</td>
						@php

						$total_taka=$card->bachelor->room_rent * $card->seat_quantity;

						@endphp

						<td>{{ $total_taka  }}</td>
						
						<td>
							<a href="#DeleteModal{{ $card->id}}" data-toggle="modal" class="btn btn-outline-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$card->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('user-cards-destroy', $card->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="btn btn-outline-primary btn-lg">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-outline-success btn-lg" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								
							</a>
						</td>
					</tr>
					<div class="pull-right">
						<a href="{{ route('card-user-booking',$card->id)}}" class="btn btn-outline-warning btn-lg">Checkout</a>
					</div>
					@endforeach
					<tr>
						<th style="margin-left: 120px" colspan="5">Total Amount</th>
						<th colspan="2">{{ $total_taka}}</th>
					</tr>
				</tbody>
			</table>
			
			@else
			<button class="badge badge-pill">
				No Iteam in your Card
			</button>
		    @endif
		</div>
	</div>

</div>
</div>
</section>
@endsection