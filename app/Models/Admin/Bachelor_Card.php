<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Bachelor_Card extends Model
{
	protected $fillable = ['seat_quantity','bachelor_id','user_id','order_id'];

	public function bachelor(){

	    return $this->belongsTo(Bechelor::class);

	}
    
}
