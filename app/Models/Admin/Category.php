<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = ['name','description','images','parent_id'];
	
	public function parent()
    {
    	return $this->belongsTo(Category::class,'parent_id');
    }

    public function families()
	{
	   return $this->hasMany(Family::class);
	}

	public function sublets()
	{
	   return $this->hasMany(Sublet::class);
	}

	public function bechelors()
	{
	   return $this->hasMany(Bechelor::class);
	}


    public static function ParentOrNotCategory($parent_id,$child_id)
	{
		$categories=Category::where('id',$child_id)
		->where('parent_id',$parent_id)
		->get();
		if(!is_null($categories))
		{
			return true;
		}else
		{
			return false;
		}
	}


}
