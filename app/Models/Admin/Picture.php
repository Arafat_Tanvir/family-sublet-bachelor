<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = ['images','family_id','sublet_id','bechelor_id'];
}
