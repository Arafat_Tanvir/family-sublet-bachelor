<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

	protected $fillable = ['transaction_id','is_paid','is_complete','is_seen_by_admin','user_id','bachelor_id','sublet_id','family_id','payment_id'];

	public function cart(){

	    return $this->hasMany(Bachelor_Card::class,'order_id');

	}

	public function payment(){

	    return $this->belongsTo(Payment::class);

	}

}
