<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Bechelor extends Model
{
    protected $fillable = ['month','year','religon','seat','room','room_status','operator','digit','room_rent','confirmed','description','ip_address','facilities','conditons','address','category_id','city_id','thana_id','ward_id','user_id'];

	public function images(){

	   	return $this->hasMany('App\Models\Admin\Picture','bachelor_id');
	}

	public function comments()
	{
	    return $this->hasMany('App\Models\Admin\Comment','bachelor_id');
	}

	public function user(){

    	return $this->belongsTo('App\Models\User\User');
    }

    public function category(){

	    return $this->belongsTo(Category::class);

	}
	public function city(){

	    return $this->belongsTo(City::class);

	}
	public function thana(){

	    return $this->belongsTo(Thana::class);
	}
	public function ward(){

	    return $this->belongsTo(Ward::class);
	}
}
