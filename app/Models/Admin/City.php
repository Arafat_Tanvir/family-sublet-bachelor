<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name','bangla_name','latitude','longitude'];
}
