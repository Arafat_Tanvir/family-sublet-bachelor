<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    public function user(){

    	return $this->belongsTo('App\Models\User\User','user_id');
    }

    public function admin(){

    	return $this->belongsTo('App\Models\Admin\Admin','admin_id');
    }
}
