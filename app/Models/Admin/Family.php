<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $fillable = ['room_status','total_room','room_rent','bed_room','bath_room','dinning_room','drawing_room','room_size','facilities','conditions','mobile','confirmed','description','ip_address','address','category_id','city_id','thana_id','ward_id','user_id'];

	public function images(){

	   	return $this->hasMany('App\Models\Admin\Picture','family_id');
	}

	public function comments()
	{
	    return $this->hasMany('App\Models\Admin\Comment','family_id');
	}

	public function user(){

    	return $this->belongsTo('App\Models\User\User');
    }

    


    public function category(){

	    return $this->belongsTo(Category::class);

	}
	public function city(){

	    return $this->belongsTo(City::class);

	}
	public function thana(){

	    return $this->belongsTo(Thana::class);
	}
	public function ward(){

	    return $this->belongsTo(Ward::class);
	}
}
