<?php

namespace App\Models\User;

use App\Models\Admin\Division;
use App\Models\Admin\District;
use App\Models\Admin\Upazila;
use App\Models\Admin\Union;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','gender','phone','email', 'password','status','images','remember_token'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
