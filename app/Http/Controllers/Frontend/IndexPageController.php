<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Admin\Family;
use App\Models\Admin\Sublet;
use App\Models\Admin\Bechelor;
use App\Models\User\User;
use App\Models\Admin\Category;
use App\Http\Controllers\Controller;
use Mail;

class IndexPageController extends Controller
{
    public function index()
    {
        $family_count=Family::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->get();
        $sublet_count=Sublet::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->get();
        $bachelor_count=Bechelor::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->get();

        $bachelors=Bechelor::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->paginate(3);
        $families=Family::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->paginate(3);
        $sublets=Sublet::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->paginate(3);
        
        $users=User::where('status',1)->orderBy('id','asc')->paginate(10);
        //dd($users);
        return view('frontend.home',
            [
                'bachelors' =>$bachelors,
                'families'  =>$families,
                'sublets'   =>$sublets,
                'users'     =>$users,
                'family_count'=>$family_count,
                'sublet_count'=>$sublet_count,
                'bachelor_count'=>$bachelor_count
            ]);
    }

    public function aboutUs()
    {
        return view('frontend.about-us');
    }

    public function services()
    {
        return view('frontend.services');
    }

    public function contact()
    {
        return view('frontend.contact');
    }

    public function contactMailSent(Request $request){
        $input = [
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'subject'=>$request->input('subject'),
            'message'=>$request->input('message')
        ];
    }
}
