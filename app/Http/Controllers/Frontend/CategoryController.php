<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Admin\Category;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function categories_show($id)
    {
        $categories=Category::find($id);
        //dd($categories);
        if(!is_null($categories))
        {
            return view('frontend.categories.index',compact('categories'));
        }else{
            session()->flash('errors','Sorry !! There is no product by this URL');
            return redirect('products');
        }
    }
}
