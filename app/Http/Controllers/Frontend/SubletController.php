<?php

namespace App\Http\Controllers\Frontend;

use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Sublet;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Sublet_Order;
use App\Models\Admin\Payment;

class SubletController extends Controller
{
    
    public function index()
    {
        $sublets=Sublet::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->paginate(3);
        return view('frontend.sublets.index',
            [
                'sublets' =>$sublets
            ]);
    }


    public function show($id)
    {
        //dd("ksd");
        $sublets = Sublet::findOrFail($id);

        $sublet_other=Sublet::orderBy('id','desc')
        ->where('category_id',$sublets->category_id)
        ->Orwhere('city_id',$sublets->city_id)
        ->Orwhere('thana_id',$sublets->thana_id)
        ->Orwhere('ward_id',$sublets->ward_id)
        ->paginate(3);





        $city_latitude=$sublets->city->latitude;
        $city_longitude=$sublets->city->longitude;

        $thana_latitude=$sublets->thana->latitude;
        $thana_longitude=$sublets->thana->longitude;

        $ward_latitude=$sublets->ward->latitude;
        $ward_longitude=$sublets->ward->longitude;

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);

        return view('frontend.sublets.show',[
            'sublets'=>$sublets,
            'sublet_other'=>$sublet_other
        ]);
    }
}
