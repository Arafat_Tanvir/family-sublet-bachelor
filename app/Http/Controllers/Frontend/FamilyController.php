<?php

namespace App\Http\Controllers\Frontend;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Family;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Family_Order;
use App\Models\Admin\Payment;

class FamilyController extends Controller
{
        public function index()
	    {
	        $families=Family::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->paginate(3);
	        return view('frontend.families.index',
	            [
	                'families' =>$families
                    
	            ]);
	    }


	public function show($id)
    {
        $families = Family::findOrFail($id);

        $family_other=Family::orderBy('id','desc')
        ->where('category_id',$families->category_id)
        ->Orwhere('city_id',$families->city_id)
        ->Orwhere('thana_id',$families->thana_id)
        ->Orwhere('ward_id',$families->ward_id)
        ->paginate(12);

        $city_latitude=$families->city->latitude;
        $city_longitude=$families->city->longitude;

        $thana_latitude=$families->thana->latitude;
        $thana_longitude=$families->thana->longitude;

        $ward_latitude=$families->ward->latitude;
        $ward_longitude=$families->ward->longitude;

        //dd("fdfs");

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);
        return view('frontend.families.show',[
            'families'=>$families,
            'family_other'=>$family_other
        ]);
    }


    
}
