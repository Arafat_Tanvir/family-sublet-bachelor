<?php


namespace App\Http\Controllers\Frontend;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Bechelor;

class BachelorController extends Controller
{
    public function index()
    {
        $bechelors=Bechelor::where('confirmed',1)->where('is_done',0)->orderBy('id','desc')->paginate(18);
        $today = today();
        
        $month_start=substr($today,5);

        return view('frontend.bachelors.index',
            [
                'bechelors' =>$bechelors
            ]);
    }


    public function show($id)
    {
        $bechelors = Bechelor::findOrFail($id);
        $bechelor_other=Bechelor::orderBy('id','desc')
        ->where('category_id',$bechelors->category_id)
        ->Orwhere('city_id',$bechelors->city_id)
        ->Orwhere('thana_id',$bechelors->thana_id)
        ->Orwhere('ward_id',$bechelors->ward_id)
        ->paginate(3);
        
        $city_latitude=$bechelors->city->latitude;
        $city_longitude=$bechelors->city->longitude;

        $thana_latitude=$bechelors->thana->latitude;
        $thana_longitude=$bechelors->thana->longitude;

        $ward_latitude=$bechelors->ward->latitude;
        $ward_longitude=$bechelors->ward->longitude;

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);

        return view('frontend.bachelors.show',[
            'bechelors'=>$bechelors,
            'bechelor_other'=>$bechelor_other
        ]);
    }
}
