<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Order;
use App\Models\Admin\Family;
use App\Models\Admin\Bechelor;
use App\Models\Admin\Sublet;
use App\Models\Admin\Bachelor_Card;
use App\Models\Admin\Payment;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
	  public function family_index()
    {
    	$family_orders=Order::orderBy('id','desc')->whereNotNull('family_id')->get();
        return view('backend/admin/family-order/family-order',compact('family_orders'));
    }

    public function sublet_index()
    {
    	$sublet_orders=Order::orderBy('id','desc')->whereNotNull('sublet_id')->get();
        return view('backend/admin/sublet-order/sublet-order',compact('sublet_orders'));
    }

    public function bachelor_index()
    {
    	  $bachelor_orders=Order::orderBy('id','desc')->whereNotNull('bechelor_id')->get();
        return view('backend/admin/bachelor-order/bachelor-order',compact('bachelor_orders'));
    }

    public function family_pending()
    {
      $family_orders=Order::orderBy('id','desc')
          ->whereNotNull('family_id')
          ->where('is_complete',0)
          ->get();

        return view('backend/admin/family-order/family-pending',compact('family_orders'));
    }

    public function sublet_pending()
    {
      $sublet_orders=Order::orderBy('id','desc')
        ->whereNotNull('sublet_id')
        ->where('is_complete',0)
        ->get();

      return view('backend/admin/sublet-order/sublet-pending',compact('sublet_orders'));
    }

    public function bachelor_pending()
    {
        $bachelor_orders=Order::orderBy('id','desc')
          ->whereNotNull('bechelor_id')
          ->where('is_complete',0)
          ->get();
        
        return view('backend/admin/bachelor-order/bachelor-pending',compact('bachelor_orders'));
    }

     public function family_complete()
    {
      $family_orders=Order::orderBy('id','desc')
          ->whereNotNull('family_id')
          ->where('is_paid',1)
          ->where('is_complete',1)
          ->get();

        return view('backend/admin/family-order/family-complete',compact('family_orders'));
    }

    public function sublet_complete()
    {
      $sublet_orders=Order::orderBy('id','desc')
        ->whereNotNull('sublet_id')
        ->where('is_paid',1)
        ->where('is_complete',1)
        ->get();
      return view('backend/admin/sublet-order/sublet-complete',compact('sublet_orders'));
    }

    public function bachelor_complete()
    {
        $bachelor_orders=Order::orderBy('id','desc')
          ->whereNotNull('bechelor_id')
          ->where('is_paid',1)
          ->where('is_complete',1)
          ->get();
        return view('backend/admin/bachelor-order/bachelor-complete',compact('bachelor_orders'));
    }



    public function bachelor_show($id)
    {
      
      $bachelor_order=Order::findOrFail($id);
      $bachelor_order->is_seen_by_admin=1;
      $bachelor_order->save();
      return view('backend/admin/bachelor-order/bachelor-show',compact('bachelor_order'));
    }

    public function family_show($id)
    {
      $family_order=Order::findOrFail($id);
      $family=Family::findOrFail($family_order->family_id);
      $family_order->is_seen_by_admin=1;
      $family_order->save();
      return view('backend/admin/family-order/family-show',compact('family_order','family'));
    }


    public function sublet_show($id)
    {
      $sublet_order=Order::findOrFail($id);
      $sublet=Sublet::findOrFail($sublet_order->sublet_id);
      $sublet_order->is_seen_by_admin=1;
      $sublet_order->save();
      return view('backend/admin/sublet-order/sublet-show',compact('sublet_order','sublet'));
    }


    public function order_paid($id)
    {
      $orders=Order::findOrFail($id);
      //dd($orders);
      if($orders->is_paid){
        $orders->is_paid=0;
      }else{
        $orders->is_paid=1;
      }
      $orders->save();
      return back();

    }

    public function order_complete($id)
    {
      $orders=Order::findOrFail($id);

      if(!is_null($orders->bechelor_id)){

        $bachelor=Bechelor::findOrFail($orders->bechelor_id);
        $available_seat = $bachelor->seat;
        foreach($orders->cart as $card){
          $cart_id=$card->id;
          $booking_seat=$card->seat_quantity;
        }

        if($orders->is_complete)
        {
          $update_seat=$available_seat+$booking_seat;
          $bachelor->seat=$update_seat;
          $bachelor->is_done=0;
          $bachelor->update();
          $orders->is_complete=0;
          $orders->save();
          return back();
        }else
        {
          $update_seat=$available_seat-$booking_seat;
          $bachelor->seat=$update_seat;
          if($update_seat<1){
               $bachelor->is_done=1;
               $bachelor->update();
               $orders->is_complete=1;
               $orders->save();
               return back();
          }
          $bachelor->update();
          $orders->is_complete=1;
          $orders->save();
          return back();
          
         
        }

       }
       if(!is_null($orders->family_id)){
         $family=Family::findOrFail($orders->family_id);
          if($orders->is_complete){
             $orders->is_complete=0;
             $family->is_done=0;
             $family->update();
          }else{
            $orders->is_complete=1;
            $family->is_done=1;
            $family->update();
          }
        $orders->save();
        return back();

       }

       if(!is_null($orders->sublet_id)){
        $sublet=Sublet::findOrFail($orders->sublet_id);
          if($orders->is_complete){
             $orders->is_complete=0;
             $sublet->is_done=0;
             $sublet->update();
          }else{
            $orders->is_complete=1;
            $sublet->is_done=1;
            $sublet->update();
          }
        $orders->save();
        return back();

       }

      

    }



}
