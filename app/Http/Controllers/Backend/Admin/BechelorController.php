<?php

namespace App\Http\Controllers\Backend\admin;

use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\BechelorComment;
use App\Models\Admin\BechelorImage;
use App\Models\Admin\Bechelor;
use App\Models\Admin\Category;
use App\Models\Admin\City;
use App\Models\Admin\Ward;
use App\Models\Admin\Thana;
use App\Models\Admin\Admin;
use Illuminate\Support\Facades\Auth;
use File;
use Image;
use DB;

class BechelorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function pending()
    {
        $bechelors=Bechelor::orderBy('id','desc')->where('confirmed',0)->get();
        return view('backend.admin.bechelors.pending',compact('bechelors'));
    }

    public function confirm()
    {
        $bechelors=Bechelor::orderBy('id','desc')->where('confirmed',1)->where('is_done',0)->get();
        return view('backend.admin.bechelors.confirm',compact('bechelors'));
    }

    public function complete()
    {
        $bechelors=Bechelor::orderBy('id','desc')->where('is_done',1)->get();
        return view('backend.admin.bechelors.complete',compact('bechelors'));
    }



    public function create(){
        $cities=City::orderBy('id','desc')->get();
        $categories=Category::orderBy('id','desc')->get();
        return view('backend.admin.bechelors.create',compact('cities','categories'));
    }



    //Bechelor data validate

    protected function BechelorInfoDataValidate($request){
        $this->validate($request,[
            'month'=>'required',
            'year'=>'required',
            'religion'=>'required',
            'seat'=>'required',
            'room'=>'required',
            'room_status'=>'required',
            'room_rent'=>'required',
            'operator'=>'required',
            'digit'=>'required',
            'description'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'address'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'images'=>'required'

        ]);
    }
    //image Upload for Bechelor
    protected function BechelorImageUpload($request,$id){
        $i=1;
        if(count($request->images)>0){
            foreach ($request->images as $images) {
                $img=time().$i++.'.'.$images->getClientOriginalExtension();
                $location=public_path('images/bechelor_room/'.$img);
                Image::make($images)->resize(300, 200)->save($location);

                $bechelor_images=new BechelorImage();
                $bechelor_images->images=$img;
                $bechelor_images->bechelor_id=$id;
                $bechelor_images->save();
            }
        }
    }

    protected function BechelorBasicSave($request){
        $bechelor=new Bechelor;
        $bechelor->month=$request->month;
        $bechelor->year=$request->year;
        $bechelor->religion=$request->religion;
        $bechelor->seat=$request->seat;
        $bechelor->room=$request->room;
        $bechelor->room_status=$request->room_status;
        $bechelor->room_rent=$request->room_rent;
        $bechelor->operator=$request->operator;
        $bechelor->digit=$request->digit;
        $bechelor->description=$request->description;
        $bechelor->ip_address=$request->ip();
        $bechelor->facilities= implode(',',$request['facilities']);
        $bechelor->conditions= implode(',',$request['conditions']);
        $bechelor->address=$request->address;
        $bechelor->category_id=$request->category_id;
        $bechelor->city_id=$request->city_id;
        $bechelor->thana_id=$request->thana_id;
        $bechelor->ward_id=$request->ward_id;
        

        if (Auth::check()) {
            $bechelor->user_id=Auth::id();
        }
        // dd($bechelor);
        $bechelor->save();

        //for image store fmaily id into Bechelorimage id as foreign key
        $id=$bechelor->id;
        $this->BechelorImageUpload($request,$id);

    }

    //store Bechelor data to store method
    public function store(Request $request){
        
        $this->BechelorInfoDataValidate($request);
        //dd("sdfks");
        $this->BechelorBasicSave($request);
        return redirect('admin/bechelors');

    }

    public function show($id)
    {

         

        $bechelors = Bechelor::findOrFail($id);
        $bechelors->is_seen=1;
        $bechelors->save();

        $city_latitude=$bechelors->city->latitude;
        $city_longitude=$bechelors->city->longitude;

        $thana_latitude=$bechelors->thana->latitude;
        $thana_longitude=$bechelors->thana->longitude;

        $ward_latitude=$bechelors->ward->latitude;
        $ward_longitude=$bechelors->ward->longitude;

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 8]]);


        return view('backend.admin.bechelors.show',[
            'bechelors'=>$bechelors,
        ]);
    }

    public function edit($id)
    {

        $bechelors = Bechelor::findOrFail($id);
        $facilities = explode(",", $bechelors->facilities);
        $conditions = explode(",", $bechelors->conditions);
        
        return view('backend.admin.bechelors.edit',[
            'bechelors'=>$bechelors,
            'facilities'=>$facilities,
            'conditions'=>$conditions

        ]);
    }

   protected function BechelorInfoDataValidateUpdate($request){
        $this->validate($request,[
            'month'=>'required',
            'year'=>'required',
            'religion'=>'required',
            'seat'=>'required',
            'room'=>'required',
            'room_status'=>'required',
            'room_rent'=>'required',
            'operator'=>'required',
            'digit'=>'required',
            'description'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'address'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required'

        ]);
    }

    protected function FamilyBasicUpdate($request,$id){
        $bechelor = Bechelor::findOrFail($id);
       $bechelor->month=$request->month;
        $bechelor->year=$request->year;
        $bechelor->religion=$request->religion;
        $bechelor->seat=$request->seat;
        $bechelor->room=$request->room;
        $bechelor->room_status=$request->room_status;
        $bechelor->room_rent=$request->room_rent;
        $bechelor->operator=$request->operator;
        $bechelor->digit=$request->digit;
        $bechelor->description=$request->description;
        $bechelor->ip_address=$request->ip();
        $bechelor->facilities= implode(',',$request['facilities']);
        $bechelor->conditions= implode(',',$request['conditions']);
        $bechelor->address=$request->address;
        $bechelor->category_id=$request->category_id;
        $bechelor->city_id=$request->city_id;
        $bechelor->thana_id=$request->thana_id;
        $bechelor->ward_id=$request->ward_id;
        if (Auth::check()) {
            $bechelor->user_id=Auth::id();
        }
        $bechelor->update();

    }

    public function update(Request $request, $id)
    {
        //dd("fhj");
        $this->BechelorInfoDataValidateUpdate($request);
        //dd("dsfs");
        $this->FamilyBasicUpdate($request,$id);
        return redirect('admin/bechelors');
    }


    public function is_Confirmed($id)
    {

      $bechelors=Bechelor::findOrFail($id);
      if($bechelors->confirmed){
        $bechelors->confirmed=0;
      }else{
        $bechelors->confirmed=1;
      }
      $bechelors->save();
      session()->flash('success','bechelors status is change!');
      return back();

    }

}
