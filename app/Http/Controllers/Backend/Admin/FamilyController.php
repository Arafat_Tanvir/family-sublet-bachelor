<?php

namespace App\Http\Controllers\Backend\admin;

use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\FamilyComment;
use App\Models\Admin\FamilyImage;
use App\Models\Admin\Family;
use App\Models\Admin\Category;
use App\Models\Admin\City;
use App\Models\Admin\Ward;
use App\Models\Admin\Thana;
use App\Models\Admin\Admin;
use Illuminate\Support\Facades\Auth;
use File;
use Image;
use DB;

class FamilyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    
    
    public function pending()
    {
        $families=Family::orderBy('id','desc')->where('confirmed',0)->get();
        return view('backend.admin.families.pending',compact('families'));
    }

    public function confirm()
    {
        $families=Family::orderBy('id','desc')->where('confirmed',1)->where('is_done',0)->get();
        return view('backend.admin.families.confirm',compact('families'));
    }

    public function complete()
    {
        $families=Family::orderBy('id','desc')->where('is_done',1)->get();
        return view('backend.admin.families.complete',compact('families'));
    }



    public function create(){
        $cities=City::orderBy('id','desc')->get();
        $categories=Category::orderBy('id','desc')->get();
        return view('backend.admin.families.create',compact('cities','categories'));
    }



    //family data validate

    protected function FamilyInfoDataValidate($request){
        $this->validate($request,[
            'room_status'=>'required',
            'total_room'=>'required',
            'room_rent'=>'required',
            'other_cost'=>'required',
            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'drawing_room'=>'required',
            'room_size'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'operator'=>'required',
            'digit'=>'required',
            'description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required',
            'images'=>'required'

        ]);
    }
    //image Upload for Family
    protected function FamilyImageUpload($request,$id){
        $i=1;
        if(count($request->file('images'))>0){
            foreach ($request->images as $images) {
                $img=time().$i++.'.'.$images->getClientOriginalExtension();
                $location=public_path('images/family_room/'.$img);
                Image::make($images)->resize(300, 200)->save($location);

                $family_images=new familyImage();
                $family_images->images=$img;
                $family_images->family_id=$id;
                $family_images->save();
            }
        }
    }

    protected function fmailyBasicSave($request){
        $family=new Family;
        $family->room_status=$request->room_status;
        $family->total_room=$request->total_room;
        $family->room_rent=$request->room_rent;
        $family->other_cost=$request->other_cost;
        $family->bed_room=$request->bed_room;
        $family->bath_room=$request->bath_room;
        $family->dinning_room=$request->dinning_room;
        $family->drawing_room=$request->drawing_room;
        $family->room_size=$request->room_size;
        $family->operator=$request->operator;
        $family->digit=$request->digit;
        $family->ip_address=$request->ip();
        $family->description=$request->description;
        $family->facilities= implode(',', $request['facilities']);
        $family->conditions= implode(',', $request['conditions']);
        $family->category_id=$request->category_id;
        $family->city_id=$request->city_id;
        $family->thana_id=$request->thana_id;
        $family->ward_id=$request->ward_id;
        $family->address=$request->address;

        if (Auth::check()) {
            $family->user_id=Auth::id();
        }

        $family->save();

        //for image store fmaily id into familyimage id as foreign key
        $id=$family->id;
        $this->FamilyImageUpload($request,$id);

    }

    //store family data to store method
    public function store(Request $request){
        $this->FamilyInfoDataValidate($request);
        $this->fmailyBasicSave($request);
        return redirect('admin/families');

    }

    public function show($id)
    {
        $families = Family::findOrFail($id);
        $families->is_seen=1;
        $families->save();
        $city_latitude=$families->city->latitude;
        $city_longitude=$families->city->longitude;

        $thana_latitude=$families->thana->latitude;
        $thana_longitude=$families->thana->longitude;

        $ward_latitude=$families->ward->latitude;
        $ward_longitude=$families->ward->longitude;

        //dd("fdfs");

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);
        return view('backend.admin.families.show',[
            'families'=>$families,
        ]);
    }

    public function edit($id)
    {

        $families = Family::findOrFail($id);
        $facilities = explode(",", $families->facilities);
        $conditions = explode(",", $families->conditions);
        
        return view('backend.admin.families.edit',[
            'families'=>$families,
            'facilities'=>$facilities,
            'conditions'=>$conditions

        ]);
    }

    protected function FamilyInfoDataValidateUpdate($request){
        $this->validate($request,[
            'room_status'=>'required',
            'total_room'=>'required',
            'room_rent'=>'required',
            'other_cost'=>'required',
            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'drawing_room'=>'required',
            'room_size'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'operator'=>'required',
            'digit'=>'required',
            'description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required'

        ]);
    }

    protected function familyBasicUpdate($request,$id){
        $family = Family::findOrFail($id);
        $family->room_status=$request->room_status;
        $family->total_room=$request->total_room;
        $family->room_rent=$request->room_rent;
        $family->other_cost=$request->other_cost;
        $family->bed_room=$request->bed_room;
        $family->bath_room=$request->bath_room;
        $family->dinning_room=$request->dinning_room;
        $family->drawing_room=$request->drawing_room;
        $family->room_size=$request->room_size;
        $family->operator=$request->operator;
        $family->digit=$request->digit;
        $family->ip_address=$request->ip();
        $family->description=$request->description;
        $family->facilities= implode(',', $request['facilities']);
        $family->conditions= implode(',', $request['conditions']);
        $family->category_id=$request->category_id;
        $family->city_id=$request->city_id;
        $family->thana_id=$request->thana_id;
        $family->ward_id=$request->ward_id;
        $family->address=$request->address;

        if (Auth::check()) {
            $family->user_id=Auth::id();
        }

        $family->update();
    }

    public function update(Request $request, $id)
    {
        //dd("fhj");
        $this->FamilyInfoDataValidateUpdate($request);
        $this->familyBasicUpdate($request,$id);
        return redirect('admin/families');
    }

    public function is_Confirmed($id)
    {

      $families=Family::findOrFail($id);
      if($families->confirmed){
        $families->confirmed=0;
      }else{
        $families->confirmed=1;
      }
      $families->save();
      session()->flash('success','families status is change!');
      return back();

    }
}
