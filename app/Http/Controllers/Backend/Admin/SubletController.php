<?php

namespace App\Http\Controllers\Backend\Admin;

use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\BechelorComment;
use App\Models\Admin\SubletImage;
use App\Models\Admin\Sublet;
use App\Models\Admin\Category;
use App\Models\Admin\City;
use App\Models\Admin\Ward;
use App\Models\Admin\Thana;
use App\Models\Admin\Admin;
use Illuminate\Support\Facades\Auth;
use File;
use Image;
use DB;

class SubletController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function pending()
    {
        $sublets=Sublet::orderBy('id','desc')->where('confirmed',0)->get();
        return view('backend.admin.sublets.pending',compact('sublets'));
    }

    public function confirm()
    {
        $sublets=Sublet::orderBy('id','desc')->where('confirmed',1)->where('is_done',0)->get();
        return view('backend.admin.sublets.confirm',compact('sublets'));
    }

    public function complete()
    {
        $sublets=Sublet::orderBy('id','desc')->where('is_done',1)->get();
        return view('backend.admin.sublets.complete',compact('sublets'));
    }

    public function create(){
        $cities=City::orderBy('id','desc')->get();
        return view('backend.admin.sublets.create',compact('cities','categories'));
    }



    //Bechelor data validate

    protected function SubletInfoDataValidate($request){
        $this->validate($request,[
            'month'=>'required',
            'year'=>'required',
            'religion'=>'required',
            'room_status'=>'required',
            'total_room'=>'required',
            'room_rent'=>'required',
            'other_cost'=>'required',
            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'drawing_room'=>'required',
            'room_size'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'operator'=>'required',
            'digit'=>'required',
            'description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required',
            'images'=>'required'

        ]);
    }
    //image Upload for Family
    protected function SubletImageUpload($request,$id){
        $i=1;
        if(count($request->file('images'))>0){
            foreach ($request->images as $images) {
                $img=time().$i++.'.'.$images->getClientOriginalExtension();
                $location=public_path('images/sublet_room/'.$img);
                Image::make($images)->resize(300, 200)->save($location);

                $sublet_images=new Subletimage();
                $sublet_images->images=$img;
                $sublet_images->sublet_id=$id;
                $sublet_images->save();
            }
        }
    }

    protected function SubletBasicSave($request){
        $sublet=new Sublet;
        $sublet->month=$request->month;
        $sublet->year=$request->year;
        $sublet->religion=$request->religion;
        $sublet->room_status=$request->room_status;
        $sublet->total_room=$request->total_room;
        $sublet->room_rent=$request->room_rent;
        $sublet->other_cost=$request->other_cost;
        $sublet->bed_room=$request->bed_room;
        $sublet->bath_room=$request->bath_room;
        $sublet->dinning_room=$request->dinning_room;
        $sublet->drawing_room=$request->drawing_room;
        $sublet->room_size=$request->room_size;
        $sublet->operator=$request->operator;
        $sublet->digit=$request->digit;
        $sublet->ip_address=$request->ip();
        $sublet->description=$request->description;
        $sublet->facilities= implode(',', $request['facilities']);
        $sublet->conditions= implode(',', $request['conditions']);
        $sublet->category_id=$request->category_id;
        $sublet->city_id=$request->city_id;
        $sublet->thana_id=$request->thana_id;
        $sublet->ward_id=$request->ward_id;
        $sublet->address=$request->address;

        if (Auth::check()) {
            $sublet->user_id=Auth::id();
        }

        $sublet->save();

        //for image store fmaily id into subletimage id as foreign key
        $id=$sublet->id;
        $this->SubletImageUpload($request,$id);

    }

    //store sublet data to store method
    public function store(Request $request){
        //dd("fkdsf");
        $this->SubletInfoDataValidate($request);
        $this->SubletBasicSave($request);
        return redirect('admin/sub-lets');

    }

    public function show($id)
    {
        //dd("ksd");
        $sublets = Sublet::findOrFail($id);
        $sublets->is_seen=1;
        $sublets->save();

        $city_latitude=$sublets->city->latitude;
        $city_longitude=$sublets->city->longitude;

        $thana_latitude=$sublets->thana->latitude;
        $thana_longitude=$sublets->thana->longitude;

        $ward_latitude=$sublets->ward->latitude;
        $ward_longitude=$sublets->ward->longitude;

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);

        return view('backend.admin.sublets.show',[
            'sublets'=>$sublets,
        ]);
    }

    public function edit($id)
    {

        $sublets = Sublet::findOrFail($id);
        $facilities = explode(",", $sublets->facilities);
        $conditions = explode(",", $sublets->conditions);
        
        return view('backend.admin.sublets.edit',[
            'sublets'=>$sublets,
            'facilities'=>$facilities,
            'conditions'=>$conditions

        ]);
    }

     protected function SubletInfoDataValidateUpdate($request){
        $this->validate($request,[
            'month'=>'required',
            'year'=>'required',
            'religion'=>'required',
            'room_status'=>'required',
            'total_room'=>'required',
            'room_rent'=>'required',
            'other_cost'=>'required',
            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'drawing_room'=>'required',
            'room_size'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'operator'=>'required',
            'digit'=>'required',
            'description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required'

        ]);
    }

    protected function FamilyBasicUpdate($request,$id){
        $sublet = Sublet::findOrFail($id);
        $sublet->month=$request->month;
        $sublet->year=$request->year;
        $sublet->religion=$request->religion;
        $sublet->room_status=$request->room_status;
        $sublet->total_room=$request->total_room;
        $sublet->room_rent=$request->room_rent;
        $sublet->other_cost=$request->other_cost;
        $sublet->bed_room=$request->bed_room;
        $sublet->bath_room=$request->bath_room;
        $sublet->dinning_room=$request->dinning_room;
        $sublet->drawing_room=$request->drawing_room;
        $sublet->room_size=$request->room_size;
        $sublet->operator=$request->operator;
        $sublet->digit=$request->digit;
        $sublet->ip_address=$request->ip();
        $sublet->description=$request->description;
        $sublet->facilities= implode(',', $request['facilities']);
        $sublet->conditions= implode(',', $request['conditions']);
        $sublet->category_id=$request->category_id;
        $sublet->city_id=$request->city_id;
        $sublet->thana_id=$request->thana_id;
        $sublet->ward_id=$request->ward_id;
        $sublet->address=$request->address;

        if (Auth::check()) {
            $sublet->user_id=Auth::id();
        }

        $sublet->update();
        //for image store fmaily id into Bechelorimage id as foreign key
        // $this->BechelorImageUpdate($request,$id);

    }

    public function update(Request $request, $id)
    {
        //dd("fhj");
        $this->SubletInfoDataValidateUpdate($request);
        $this->FamilyBasicUpdate($request,$id);
        return redirect('admin/sub-lets');
    }

     public function is_Confirmed($id)
    {

      $sublets=Sublet::findOrFail($id);
      if($sublets->confirmed){
        $sublets->confirmed=0;
      }else{
        $sublets->confirmed=1;
      }
      $sublets->save();
      session()->flash('success','sublets status is change!');
      return back();

    }
}
