<?php

namespace App\Http\Controllers\Backend\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Admin;
use App\Models\Admin\Bechelor;
use App\Models\Admin\Family;
use App\Models\Admin\Sublet;
use App\Models\Admin\Order;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    


    //for user details and got data for active user and edit user data
    public function dashboard(){
    	$family_orders=Order::orderBy('id','desc')->whereNotNull('family_id')->get();
    	$sublet_orders=Order::orderBy('id','desc')->whereNotNull('sublet_id')->get();
    	$bachelor_orders=Order::orderBy('id','desc')->whereNotNull('bechelor_id')->get();

        $family_pending=Order::orderBy('id','desc')->whereNotNull('family_id')
            ->where('is_complete',0)
            ->get();
        $sublet_pending=Order::orderBy('id','desc')->whereNotNull('sublet_id')
            ->where('is_complete',0)
            ->get();
        $bachelor_pending=Order::orderBy('id','desc')->whereNotNull('bechelor_id')
            ->where('is_complete',0)
            ->get();

        $family_complete=Order::orderBy('id','desc')->whereNotNull('family_id')
            ->where('is_paid',1)
            ->where('is_complete',1)
            ->get();
        $sublet_complete=Order::orderBy('id','desc')->whereNotNull('sublet_id')
            ->where('is_paid',1)
            ->where('is_complete',1)
            ->get();
        $bachelor_complete=Order::orderBy('id','desc')->whereNotNull('bechelor_id')
            ->where('is_paid',1)
            ->where('is_complete',1)
            ->get();

        $bechelors_confirm=Bechelor::orderBy('id','desc')->where('confirmed',1)->where('is_done',0)->get();
        $families_confirm=Family::orderBy('id','desc')->where('confirmed',1)->where('is_done',0)->get();
        $sublets_confirm=Sublet::orderBy('id','desc')->where('confirmed',1)->where('is_done',0)->get();

    	$bechelors_pending=Bechelor::orderBy('id','desc')->where('confirmed',0)->get();
    	$families_pending=Family::orderBy('id','desc')->where('confirmed',0)->get();
    	$sublets_pending=Sublet::orderBy('id','desc')->where('confirmed',0)->get();

        $bechelors_complete=Bechelor::orderBy('id','desc')->where('is_done',1)->get();
        $families_complete=Family::orderBy('id','desc')->where('is_done',1)->get();
        $sublets_complete=Sublet::orderBy('id','desc')->where('is_done',1)->get();


    	return view("backend/admin/dashboard",[
    		'bechelors_confirm'=>$bechelors_confirm,
    		'families_confirm'=>$families_confirm,
    		'sublets_confirm'=>$sublets_confirm,

            'bechelors_complete'=>$bechelors_complete,
            'sublets_complete'=>$sublets_complete,
            'families_complete'=>$families_complete,

            'bechelors_pending'=>$bechelors_pending,
            'sublets_pending'=>$sublets_pending,
            'families_pending'=>$families_pending,

    		'family_orders'=>$family_orders,
    		'sublet_orders'=>$sublet_orders,
    		'bachelor_orders'=>$bachelor_orders,

            'bachelor_complete'=>$bachelor_complete,
            'sublet_complete'=>$sublet_complete,
            'family_complete'=>$family_complete,

            'bachelor_pending'=>$bachelor_pending,
            'sublet_pending'=>$sublet_pending,
            'family_pending'=>$family_pending
            
    	]);
    }
}
