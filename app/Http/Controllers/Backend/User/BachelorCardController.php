<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Bachelor_Card;
use App\Models\Admin\Bechelor;
use Illuminate\Support\Facades\Auth;

class BachelorCardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function booking($id)
    {

        $cards = Bachelor_Card::findOrFail($id);
        return view('backend.user.cards.booking',[
            'cards'=>$cards

        ]);
    }

    
    public function index()
    {
    	$bechelor_cards=Bachelor_Card::orderBy('id','desc')
        ->where('user_id',Auth::id())
        ->where('order_id',NULL)
        ->get();
        return view('backend/user/cards.index',compact('bechelor_cards'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'bachelor_id'=>'required',
        ]);

         if(Auth::check()){
            $cards=Bachelor_Card::where('user_id',Auth::id())
            ->where('order_id',NULL)
            ->first();
        }
        if (!is_null($cards))
        {
             return redirect()->route('user-cards-index');
        }
        else
        {
        	$cards=new Bachelor_Card();

	        if(Auth::check()){
	            $cards->user_id=Auth::id();
	        }
	        $cards->bachelor_id=$request->bachelor_id;
	        $cards->save();
        	return redirect()->route('user-cards-index');
        }
        
    }

    public function update(Request $request, $id)
    {
        $bachelor_card=Bachelor_Card::findOrFail($id);
        $avaiable_seat=Bechelor::where('id',$bachelor_card->bachelor_id)->first()->seat;

        if($avaiable_seat<$request->seat_quantity){
            return back();
        }

        if(!is_null($bachelor_card)){
            $bachelor_card->seat_quantity=$request->seat_quantity;
            $bachelor_card->update();
        }else{
            return redirect()->route('user-cards-index');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bachelor_card=Bachelor_Card::findOrFail($id);
        if(!is_null($bachelor_card))
        {
            $bachelor_card->delete();
            
        }else{
            return redirect()->route('user-cards-index');
        }
        return back();
    }



}
