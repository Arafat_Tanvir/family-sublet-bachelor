<?php

namespace App\Http\Controllers\Backend\user;

use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\FamilyComment;
use App\Models\Admin\Family;
use App\Models\Admin\Category;
use App\Models\Admin\City;
use App\Models\Admin\Ward;
use App\Models\Admin\Thana;
use App\Models\Admin\Family_Order;
use App\Models\Admin\Payment;
use App\Models\Admin\Picture;
use App\Models\Admin\Admin;
use Illuminate\Support\Facades\Auth;
use File;
use Image;
use DB;

class FamilyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        
    	$families=Family::orderBy('id','desc')->where('user_id',Auth::id())->where('confirmed','0')->get();
        return view('backend.user.families.index',compact('families'));
    }

    public function confirmed()
    {
        
        $families=Family::orderBy('id','desc')->where('user_id',Auth::id())->where('confirmed','1')->get();
        return view('backend.user.families.index',compact('families'));
    }


    public function create(){
    	$cities=City::orderBy('id','desc')->get();
        $categories=Category::orderBy('id','desc')->get();
    	return view('backend.user.families.create',compact('cities','categories'));
    }



    //family data validate

    protected function FamilyInfoDataValidate($request){
        $this->validate($request,[
            'month'=>'required',
            'year'=>'required',
            'room_status'=>'required',
            'total_room'=>'required',
            'room_rent'=>'required',
            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'drawing_room'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'phone'=>'required',
            'short_description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required',
            'images'=>'required'

        ]);
    }
    //image Upload for Family
    protected function FamilyImageUpload($request,$id){
        $i=1;
        if(count($request->file('images'))>0){
            foreach ($request->images as $images) {
                $img=time().$i++.'.'.$images->getClientOriginalExtension();
                $location=public_path('images/family_room/'.$img);
                Image::make($images)->save($location);

                $picture=new Picture();
                $picture->images=$img;
                $picture->family_id=$id;
                $picture->save();
            }
        }
    }

    protected function fmailyBasicSave($request){
        $family=new Family;
        $family->month=$request->month;
        $family->year=$request->year;
        $family->room_status=$request->room_status;
        $family->total_room=$request->total_room;
        $family->room_rent=$request->room_rent;
        $family->bed_room=$request->bed_room;
        $family->bath_room=$request->bath_room;
        $family->dinning_room=$request->dinning_room;
        $family->drawing_room=$request->drawing_room;
        $family->phone=$request->phone;
        $family->short_description=$request->short_description;
        $family->facilities= implode(',', $request['facilities']);
        $family->conditions= implode(',', $request['conditions']);
        $family->category_id=$request->category_id;
        $family->city_id=$request->city_id;
        $family->thana_id=$request->thana_id;
        $family->ward_id=$request->ward_id;
        $family->address=$request->address;

        if (Auth::check()) {
            $family->user_id=Auth::id();
        }

        $family->save();

        //for image store fmaily id into familyimage id as foreign key
        $id=$family->id;
        $this->FamilyImageUpload($request,$id);

    }

    //store family data to store method
    public function store(Request $request){
        $this->FamilyInfoDataValidate($request);
        $this->fmailyBasicSave($request);
        return redirect()->route('families-user-index');

    }

    public function show($id)
    {
        $families = Family::findOrFail($id);

        $city_latitude=$families->city->latitude;
        $city_longitude=$families->city->longitude;

        $thana_latitude=$families->thana->latitude;
        $thana_longitude=$families->thana->longitude;

        $ward_latitude=$families->ward->latitude;
        $ward_longitude=$families->ward->longitude;

        //dd("fdfs");

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);
        return view('backend.user.families.show',[
            'families'=>$families
        ]);
    }

    public function edit($id)
    {

        $families = Family::findOrFail($id);
        $facilities = explode(",", $families->facilities);
        $conditions = explode(",", $families->conditions);
        
        return view('backend.user.families.edit',[
            'families'=>$families,
            'facilities'=>$facilities,
            'conditions'=>$conditions

        ]);
    }

    protected function FamilyInfoDataValidateUpdate($request){

        $this->validate($request,[
            'month'=>'required',
            'year'=>'required',
            'room_status'=>'required',
            'total_room'=>'required',
            'room_rent'=>'required',


            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'drawing_room'=>'required',
            'phone'=>'required',


            'short_description'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',


            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required'

        ]);
        
    }

    protected function familyBasicUpdate($request,$id){
        $family = Family::findOrFail($id);
        $family->month=$request->month;
        $family->year=$request->year;
        $family->room_status=$request->room_status;
        $family->total_room=$request->total_room;
        $family->room_rent=$request->room_rent;
        $family->bed_room=$request->bed_room;
        $family->bath_room=$request->bath_room;
        $family->dinning_room=$request->dinning_room;
        $family->drawing_room=$request->drawing_room;
        $family->phone=$request->phone;
        $family->short_description=$request->short_description;
        $family->facilities= implode(',', $request['facilities']);
        $family->conditions= implode(',', $request['conditions']);
        $family->category_id=$request->category_id;
        $family->city_id=$request->city_id;
        $family->thana_id=$request->thana_id;
        $family->ward_id=$request->ward_id;
        $family->address=$request->address;


        if (Auth::check()) {
            $family->user_id=Auth::id();
        }

        $family->update();
    }

    public function update(Request $request, $id)
    {
        
        //dd($request);
        $this->FamilyInfoDataValidateUpdate($request);

        $this->familyBasicUpdate($request,$id);
        return redirect()->route('families-user-index');
    }


    public function booking($id)
    {

        $families = Family::findOrFail($id);
        
        return view('backend.user.families.booking',[
            'families'=>$families

        ]);
    }
}
