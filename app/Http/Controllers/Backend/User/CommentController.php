<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Comment;

class CommentController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }


     public function store(Request $request){

    	if(!is_null($request->bachelor_id)){
	    	$bechelorcomment =new Comment();
	    	$bechelorcomment->description=$request->description;
	    	$bechelorcomment->parent_id=$request->parent_id;
	    	$bechelorcomment->bachelor_id=$request->bachelor_id;
	    	if (Auth::check()) {
	            $bechelorcomment->user_id=Auth::id();
	        }
	    	//dd($bechelorcomment);
	    	$bechelorcomment->save();
	    	return back();
        }
        if(!is_null($request->family_id)){
        	$familyComment =new Comment();
	    	$familyComment->description=$request->description;
	    	$familyComment->parent_id=$request->parent_id;
	    	$familyComment->family_id=$request->family_id;
	    	if (Auth::check()) {
	            $familyComment->user_id=Auth::id();
	        }
	    	//dd($bechelorcomment);
	    	$familyComment->save();
	    	return back();
        }
         if(!is_null($request->sublet_id)){
         	$subletComment =new Comment();
	    	$subletComment->description=$request->description;
	    	$subletComment->parent_id=$request->parent_id;
	    	$subletComment->sublet_id=$request->sublet_id;
	    	if (Auth::check()) {
	            $subletComment->user_id=Auth::id();
	        }
	    	//dd($bechelorcomment);
	    	$subletComment->save();
	    	return back();
         }

    }

    public function update(Request $request, $id)
    {
          $comment=Comment::findOrFail($id);
          $comment->description=$request->description;
          //dd($comment);
          $comment->update();
          return back();
    }

}
