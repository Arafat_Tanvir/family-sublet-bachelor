<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User\User;

class UserController extends Controller
{
    // protection of user
	public function __construct()
    {
        $this->middleware('auth');
    }


    //for user details and got data for active user and edit user data
    public function dashboard()
    {
    	$user=Auth::user();
    	return view("backend/user/dashboard",compact('user'));
    }

}
