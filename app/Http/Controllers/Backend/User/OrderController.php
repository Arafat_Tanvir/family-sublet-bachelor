<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Order;
use App\Models\Admin\Bachelor_Card;
use App\Models\Admin\Payment;

class OrderController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function family_index()
    {
    	$family_orders=Order::orderBy('id','desc')
    	->where('user_id',Auth::id())
    	->where('is_complete',0)
    	->whereNotNull('family_id')
    	->get();
    	//dd($family_orders);
        return view('backend/user/families/order',compact('family_orders'));
    }

    public function sublet_index()
    {
    	$sublet_orders=Order::orderBy('id','desc')
    	->where('user_id',Auth::id())
    	->where('is_complete',0)
    	->whereNotNull('sublet_id')
    	->get();
    	//dd($sublet_orders);
        return view('backend/user/sublets/order',compact('sublet_orders'));
    }

    public function bachelor_index()
    {
    	$bachelor_orders=Order::orderBy('id','desc')
        ->where('user_id',Auth::id())
        ->where('is_complete',0)
        ->whereNotNull('bechelor_id')
        ->get();
        //dd($bachelor_orders);
        
        return view('backend/user/bechelors/order',compact('bachelor_orders'));
    }

    
    public function store(Request $request)
    {

    	if(!is_null($request->bachelor_id)){

    		//dd($request->bachelor_id);

    		$order=new Order();
    		$order->name=$request->name;
    		$order->email=$request->email;
    		$order->phone=$request->phone;
    		$order->message=$request->message;
	        $order->transaction_id=$request->transaction_id;
	        $order->bechelor_id=$request->bachelor_id;
	        if (Auth::check()) {
	            $order->user_id=Auth::id();
	        }
	        if($request->payment_id!='Cash'){
	            if($request->transaction_id==NULL || empty($request->transaction_id)){
	                session()->flash('sticky_error','Please input transaction id for your payment');
	                return back();
	            }
	        }
	        $order->payment_id=Payment::where('short_name',$request->payment_id)->first()->id;
            $order->save();

            $bachelor_cards=Bachelor_Card::where('bachelor_id',$request->bachelor_id)
	        ->where('user_id',Auth::id())->where('order_id',NUll)
	        ->first();

	        if(!is_null($bachelor_cards)){

		        $bachelor_cards->order_id=$order->id;
		        $bachelor_cards->update();
			    }
		       return redirect(route('user-cards-index'));

	    }
	    if(!is_null($request->sublet_id)){
	    	//dd($request->sublet_id);
		    $order=new Order();
		    $order->name=$request->name;
    		$order->email=$request->email;
    		$order->phone=$request->phone;
    		$order->message=$request->message;
	        $order->sublet_id=$request->sublet_id;

	        if($request->payment_id!='Cash'){
	            if($request->transaction_id==NULL || empty($request->transaction_id)){
	                session()->flash('sticky_error','Please input transaction id for your payment');
	                return back();
	            }
	        }

	        $order->payment_id=Payment::where('short_name',$request->payment_id)->first()->id;
	        if (Auth::check()) {
	            $order->user_id=Auth::id();
	            $order->save();
	            return redirect(route('sublets-user-order-index'));
	        }else
	        {
	            return back();
	        }
	    }
	    if(!is_null($request->family_id)){

		    $order=new Order();
		    $order->name=$request->name;
    		$order->email=$request->email;
    		$order->phone=$request->phone;
    		$order->message=$request->message;
	        $order->family_id=$request->family_id;

	        if($request->payment_id!='Cash'){
	            if($request->transaction_id==NULL || empty($request->transaction_id)){
	                return back();
	            }
	        }


	        $order->payment_id=Payment::where('short_name',$request->payment_id)->first()->id;

	        if (Auth::check()) {
	            $order->user_id=Auth::id();
	            $order->save();
	            return redirect(route('families-user-order-index'));
	        }else
	        {
	            return back();
	        }
	    }

	    return back();



    }
}
