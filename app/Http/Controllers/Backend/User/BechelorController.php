<?php

namespace App\Http\Controllers\Backend\user;

use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\BechelorComment;
use App\Models\Admin\BechelorImage;
use App\Models\Admin\Bechelor;
use App\Models\Admin\Category;
use App\Models\Admin\City;
use App\Models\Admin\Ward;
use App\Models\Admin\Thana;
use App\Models\Admin\Admin;
use App\Models\Admin\Picture;
use Illuminate\Support\Facades\Auth;
use File;
use Image;
use DB;

class BechelorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    //panding post 
    public function index()
    {
        
    	$bechelors=Bechelor::orderBy('id','desc')->where('user_id',Auth::id())->where('confirmed','0')->get();
        return view('backend.user.bechelors.index',compact('bechelors'));
    }

    //confirmed Post
    public function confirmed()
    {
        
        $bechelors=Bechelor::orderBy('id','desc')->where('user_id',Auth::id())->where('confirmed','1')->get();
        return view('backend.user.bechelors.index',compact('bechelors'));
    }


    public function create(){
    	$cities=City::orderBy('id','desc')->get();
        $categories=Category::orderBy('id','desc')->get();
    	return view('backend.user.bechelors.create',compact('cities','categories'));
    }



    //Bechelor data validate

    protected function BechelorInfoDataValidate($request){
        $this->validate($request,[
            'roommate_type'=>'required',
            'roommate_gender'=>'required',
            'roommate_religion'=>'required',

            'month'=>'required',
            'year'=>'required',
            'seat'=>'required',
            'room_status'=>'required',
            'room_rent'=>'required',

            'phone'=>'required',
            'short_description'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'images'=>'required',


            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required'

        ]);
    }
    //image Upload for Bechelor
    protected function BechelorImageUpload($request,$id){
        $i=1;
        if(count($request->images)>0){
            foreach ($request->images as $images) {
                $img=time().$i++.'.'.$images->getClientOriginalExtension();
                $location=public_path('images/bechelor_room/'.$img);
                Image::make($images)->save($location);

                $picture=new Picture();
                $picture->images=$img;
                $picture->bachelor_id=$id;
                $picture->save();
            }
        }
    }

    protected function BechelorBasicSave($request){
        $bechelor=new Bechelor;
        $bechelor->roommate_type=$request->roommate_type;
        $bechelor->roommate_gender=$request->roommate_gender;
        $bechelor->roommate_religion=$request->roommate_religion;

        $bechelor->month=$request->month;
        $bechelor->year=$request->year;
        $bechelor->seat=$request->seat;
        $bechelor->room_status=$request->room_status;
        $bechelor->room_rent=$request->room_rent;


        $bechelor->phone=$request->phone;
        $bechelor->short_description=$request->short_description;
        $bechelor->facilities= implode(',',$request['facilities']);
        $bechelor->conditions= implode(',',$request['conditions']);
        $bechelor->address=$request->address;


        $bechelor->category_id=$request->category_id;
        $bechelor->city_id=$request->city_id;
        $bechelor->thana_id=$request->thana_id;
        $bechelor->ward_id=$request->ward_id;
        if (Auth::check()) {
            $bechelor->user_id=Auth::id();
        }


        // dd($bechelor);
        $bechelor->save();

        //for image store fmaily id into Bechelorimage id as foreign key
        $id=$bechelor->id;
        $this->BechelorImageUpload($request,$id);

    }

    //store Bechelor data to store method
    public function store(Request $request){
        
        $this->BechelorInfoDataValidate($request);
        //dd("sdfks");
        $this->BechelorBasicSave($request);
        return redirect('user/bechelors');

    }

    public function show($id)
    {
        $bechelors = Bechelor::findOrFail($id);
        
        $city_latitude=$bechelors->city->latitude;
        $city_longitude=$bechelors->city->longitude;

        $thana_latitude=$bechelors->thana->latitude;
        $thana_longitude=$bechelors->thana->longitude;

        $ward_latitude=$bechelors->ward->latitude;
        $ward_longitude=$bechelors->ward->longitude;

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);

        return view('backend.user.bechelors.show',[
            'bechelors'=>$bechelors
        ]);
    }

    public function edit($id)
    {

        $bechelors = Bechelor::findOrFail($id);
        $facilities = explode(",", $bechelors->facilities);
        $conditions = explode(",", $bechelors->conditions);
        
        return view('backend.user.bechelors.edit',[
            'bechelors'=>$bechelors,
            'facilities'=>$facilities,
            'conditions'=>$conditions

        ]);
    }


   protected function BechelorInfoDataValidateUpdate($request){
        $this->validate($request,[
            'roommate_type'=>'required',
            'roommate_gender'=>'required',
            'roommate_religion'=>'required',

            'month'=>'required',
            'year'=>'required',
            'seat'=>'required',
            'room_status'=>'required',
            'room_rent'=>'required',

            'phone'=>'required',
            'short_description'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',


            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required'

        ]);
    }

    protected function FamilyBasicUpdate($request,$id){

        $bechelor = Bechelor::findOrFail($id);
        $bechelor->roommate_type=$request->roommate_type;
        $bechelor->roommate_gender=$request->roommate_gender;
        $bechelor->roommate_religion=$request->roommate_religion;

        $bechelor->month=$request->month;
        $bechelor->year=$request->year;
        $bechelor->seat=$request->seat;
        $bechelor->room_status=$request->room_status;
        $bechelor->room_rent=$request->room_rent;


        $bechelor->phone=$request->phone;
        $bechelor->short_description=$request->short_description;
        $bechelor->facilities= implode(',',$request['facilities']);
        $bechelor->conditions= implode(',',$request['conditions']);
        $bechelor->address=$request->address;


        $bechelor->category_id=$request->category_id;
        $bechelor->city_id=$request->city_id;
        $bechelor->thana_id=$request->thana_id;
        $bechelor->ward_id=$request->ward_id;

        if (Auth::check()) {
            $bechelor->user_id=Auth::id();
        }
        $bechelor->update();

    }

    public function update(Request $request, $id)
    {
        //dd("fhj");
        $this->BechelorInfoDataValidateUpdate($request);
        //dd("dsfs");
        $this->FamilyBasicUpdate($request,$id);
        return redirect()->route('bechelors-user-index');
    }
}
