<?php

namespace App\Http\Controllers\Backend\user;

use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\BechelorComment;
use App\Models\Admin\Sublet;
use App\Models\Admin\Category;
use App\Models\Admin\City;
use App\Models\Admin\Ward;
use App\Models\Admin\Thana;
use App\Models\Admin\Picture;
use App\Models\Admin\Admin;
use Illuminate\Support\Facades\Auth;
use File;
use Image;
use DB;

class SubletController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$sublets=Sublet::orderBy('id','desc')->where('user_id',Auth::id())->where('confirmed','0')->get();
        //dd($sublets);
        return view('backend.user.sublets.index',compact('sublets'));
    }

    public function confirmed()
    {
        
        $sublets=Sublet::orderBy('id','desc')->where('user_id',Auth::id())->where('confirmed','1')->get();
        return view('backend.user.sublets.index',compact('sublets'));
    }

    public function create(){
    	$cities=City::orderBy('id','desc')->get();
    	return view('backend.user.sublets.create',compact('cities','categories'));
    }



    //Bechelor data validate

    protected function SubletInfoDataValidate($request){
        $this->validate($request,[

            'sublet_religion'=>'required',
            'sublet_gender'=>'required',

            'month'=>'required',
            'year'=>'required',
            'room_status'=>'required',
            'total_room'=>'required',
            'room_rent'=>'required',


            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'drawing_room'=>'required',
            'facilities'=>'required',


            'conditions'=>'required',
            'phone'=>'required',
            'short_description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',


            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required',
            'images'=>'required'

        ]);
    }
    //image Upload for Family
    protected function SubletImageUpload($request,$id){
        $i=1;
        if(count($request->file('images'))>0){
            foreach ($request->images as $images) {
                $img=time().$i++.'.'.$images->getClientOriginalExtension();
                $location=public_path('images/sublet_room/'.$img);
                Image::make($images)->resize(300, 200)->save($location);

                $picture=new Picture();
                $picture->images=$img;
                $picture->sublet_id=$id;
                $picture->save();
            }
        }
    }

    protected function SubletBasicSave($request){
        $sublet=new Sublet;
        $sublet->sublet_religion=$request->sublet_religion;
        $sublet->sublet_gender=$request->sublet_gender;

        $sublet->month=$request->month;
        $sublet->year=$request->year;
        $sublet->room_status=$request->room_status;
        $sublet->total_room=$request->total_room;
        $sublet->room_rent=$request->room_rent;

       
        $sublet->bed_room=$request->bed_room;
        $sublet->bath_room=$request->bath_room;
        $sublet->dinning_room=$request->dinning_room;
        $sublet->drawing_room=$request->drawing_room;

        $sublet->phone=$request->phone;
        $sublet->short_description=$request->short_description;
        $sublet->facilities= implode(',', $request['facilities']);
        $sublet->conditions= implode(',', $request['conditions']);
        $sublet->category_id=$request->category_id;

        $sublet->city_id=$request->city_id;
        $sublet->thana_id=$request->thana_id;
        $sublet->ward_id=$request->ward_id;
        $sublet->address=$request->address;

        if (Auth::check()) {
            $sublet->user_id=Auth::id();
        }

        $sublet->save();

        //for image store fmaily id into subletimage id as foreign key
        $id=$sublet->id;
        $this->SubletImageUpload($request,$id);

    }

    //store sublet data to store method
    public function store(Request $request){
        //dd("fkdsf");
        $this->SubletInfoDataValidate($request);
        $this->SubletBasicSave($request);
        return redirect()->route('sub-lets-user-index');

    }

    public function show($id)
    {
        //dd("ksd");
        $sublets = Sublet::findOrFail($id);

        $city_latitude=$sublets->city->latitude;
        $city_longitude=$sublets->city->longitude;

        $thana_latitude=$sublets->thana->latitude;
        $thana_longitude=$sublets->thana->longitude;

        $ward_latitude=$sublets->ward->latitude;
        $ward_longitude=$sublets->ward->longitude;

        Mapper::map($thana_latitude,$thana_longitude, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);

        return view('backend.user.sublets.show',[
            'sublets'=>$sublets
        ]);
    }

    public function edit($id)
    {

        $sublets = Sublet::findOrFail($id);
        $facilities = explode(",", $sublets->facilities);
        $conditions = explode(",", $sublets->conditions);
        
        return view('backend.user.sublets.edit',[
            'sublets'=>$sublets,
            'facilities'=>$facilities,
            'conditions'=>$conditions

        ]);
    }

     protected function SubletInfoDataValidateUpdate($request){
        $this->validate($request,[

            'sublet_religion'=>'required',
            'sublet_gender'=>'required',

            'month'=>'required',
            'year'=>'required',
            'room_status'=>'required',
            'total_room'=>'required',
            'room_rent'=>'required',


            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'drawing_room'=>'required',
            'facilities'=>'required',


            'conditions'=>'required',
            'phone'=>'required',
            'short_description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',


            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required'
        ]);
    }

    protected function SubletBasicUpdate($request,$id){
        $sublet = Sublet::findOrFail($id);
        $sublet->sublet_religion=$request->sublet_religion;
        $sublet->sublet_gender=$request->sublet_gender;

        $sublet->month=$request->month;
        $sublet->year=$request->year;
        $sublet->room_status=$request->room_status;
        $sublet->total_room=$request->total_room;
        $sublet->room_rent=$request->room_rent;

       
        $sublet->bed_room=$request->bed_room;
        $sublet->bath_room=$request->bath_room;
        $sublet->dinning_room=$request->dinning_room;
        $sublet->drawing_room=$request->drawing_room;

        $sublet->phone=$request->phone;
        $sublet->short_description=$request->short_description;
        $sublet->facilities= implode(',', $request['facilities']);
        $sublet->conditions= implode(',', $request['conditions']);
        $sublet->category_id=$request->category_id;
        
        $sublet->city_id=$request->city_id;
        $sublet->thana_id=$request->thana_id;
        $sublet->ward_id=$request->ward_id;
        $sublet->address=$request->address;

        if (Auth::check()) {
            $sublet->user_id=Auth::id();
        }

        $sublet->update();
        //for image store fmaily id into Bechelorimage id as foreign key
        // $this->BechelorImageUpdate($request,$id);

    }

    public function update(Request $request, $id)
    {
        //dd("fhj");
        $this->SubletInfoDataValidateUpdate($request);
        $this->SubletBasicUpdate($request,$id);
        return redirect()->route('sub-lets-user-index');
    }


    public function booking($id)
    {

        $sublets = Sublet::findOrFail($id);
        
        return view('backend.user.sublets.booking',[
            'sublets'=>$sublets

        ]);
    }
}
