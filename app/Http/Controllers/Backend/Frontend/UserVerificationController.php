<?php

namespace App\Http\Controllers\Backend\Frontend;

use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class UserVerificationController extends Controller
{
    public function verify($token)
    {

    	$user=User::where('remember_token',$token)->first();
    	if (!is_null($user)) {
    		$user->status=1;
    	    $user->remember_token=NULL;
    	    $user->save();
    	    session()->flash('success','you are registered successfully::login now');
    	    return redirect()->route('login');
    	}else{
    		session()->flash('errors','Sorry! Your token is not match!!');
    		return redirect('/');
    	}
    	
    }

}
