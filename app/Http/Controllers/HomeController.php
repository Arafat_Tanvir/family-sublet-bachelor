<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Division;
use App\Models\Admin\District;
use App\Models\Admin\Upazila;
use App\Models\Admin\Union;
use Illuminate\Support\Facades\Auth;
use App\Models\User\User;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Support\Facades\Route;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        Mapper::map(53.381128999999990000, -1.470085000000040000, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);
        
        return view('backend/user/dashboard',compact('user'));
    }

    public function cities_ajax($id)
    {
        $thanas = DB::table('thanas')->where('city_id', $id)->pluck('name','id');
        return json_encode($thanas);
    }

    public function thanas_ajax($id)
    {
        //dd($id);
        $wards=DB::table("wards")
        ->where("thana_id",$id)
        ->pluck("name","id");
        return json_encode($wards);
    }

}
