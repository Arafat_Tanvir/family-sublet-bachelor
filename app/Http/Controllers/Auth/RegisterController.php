<?php

namespace App\Http\Controllers\Auth;
use Image;
use App\Models\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\UserVerifyRegistration;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'gender'=>'required',
            'phone'=>'required',
            'images'=>'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
   protected function register(Request $request)
    {
        //dd($request);
        if($request->file('images'))
        {
            $images=$request->file('images');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('images/users/'.$img);
            Image::make($images)->save($location);
        }

        //dd($img);
        //dd($request->gender);

        $user= User::create([

            
            'name' => $request->name,
            'email' => $request->email,
            'gender'=>$request->gender,
            'phone'=>$request->phone,
            'password' => Hash::make($request['password']),
            'images'=> $img,
            'remember_token'=>str_random(50),
            
        ]);
        //dd($user);

        $user->notify(new UserVerifyRegistration($user));
        //dd($user);
        session()->flash('success','A confirmation email has sent to you,please check your email and click to registration confirmed');
        return redirect()->route('/');
    }
}
